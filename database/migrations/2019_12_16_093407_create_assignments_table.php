<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('assignment_type')->nullable();
            $table->string('repo_type')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->year('year')->nullable();
            $table->string('vin')->nullable();
            $table->string('license_plate')->nullable();
            $table->string('vehicle_address')->nullable();
            $table->string('vehicle_apt_no')->nullable();
            $table->string('vehicle_city')->nullable();
            $table->string('vehicle_state')->nullable();
            $table->string('vehicle_zip')->nullable();
            $table->date('legel_sale_date')->nullable();
            $table->tinyInteger('keys')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_account_no')->nullable();
            $table->string('contract_date')->nullable();
            $table->string('original_loan_amount')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('ssn')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_apt_no')->nullable();
            $table->string('customer_city')->nullable();
            $table->string('customer_state')->nullable();
            $table->string('customer_zip')->nullable();
            $table->string('cust_poe_company_name')->nullable();
            $table->string('cust_poe_company_address')->nullable();
            $table->string('cust_poe_suite_no')->nullable();
            $table->string('cust_poe_company_city')->nullable();
            $table->string('cust_poe_company_state')->nullable();
            $table->string('cust_poe_company_zip')->nullable();
            $table->text('assignment_notes')->nullable();
            $table->string('vehicle_notes_type')->nullable();
            $table->text('vehicle_notes')->nullable();
            $table->date('date_initial_assignment_received')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('last_modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
