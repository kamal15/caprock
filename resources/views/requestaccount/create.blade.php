@extends('layouts.master')
@section('content')

<div class="card card-default" style="box-shadow:none;">
    <div class="row">
        <div class="col-lg-2 ">&nbsp;</div>
        <div class="col-lg-10 ">
            <section class="content">
                <div class="container-fluid">
                    <div class="card card-warning">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div class="panel panel-default">
                                        <div class="panel-heading clearfix">
                                            <div class="panel-heading-title pull-left">
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-1">&nbsp;</div>
                                                <div class="col-lg-9" style="font-size: 25px;">Request an Account</div>
                                                <div class="col-lg-2">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-10" style="width: 73em;text-align: center;">
                                                    @if (session()->has('message'))
                                                    <div style="background-color: aqua;" role="alert">
                                                        <div class="alert-text">
                                                            {{ session('message') }}
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if ($errors->any())
                                                    <div style="background-color: aqua;">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">&nbsp;</div>
                                            </div><br />
                                            @endif
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <form method="post" action="{{url('/account/create')}}">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                              <div class="row">
                                                                    <div class="col-md-1">&nbsp;</div>
                                                                    <div class="col-md-3 ">
                                                                        <label for="title">Name</label></div>
                                                                    <div class="col-md-7">
                                                                        <input type="text" class="form-control" name="name" value="{{old('name', @$requestaccount->name)}}" />
                                                                    </div>
                                                                    <div class="col-md-1">&nbsp;</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">&nbsp;</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label for="title">Email Address</label></div>
                                                                    <div class="col-md-7">
                                                                        <input type="text" class="form-control" name="email" value="{{old('email', @$requestaccount->email)}}" />
                                                                    </div>
                                                                    <div class="col-md-1">&nbsp;</div>
                                                                </div>
                                                                <div class="col-lg-4">&nbsp;</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <div class="row">
                                                                    <div class="col-md-1">&nbsp;</div>
                                                                        <div class="col-md-3">
                                                                            <label for="title">Phone</label></div>
                                                                        <div class="col-md-7">
                                                                            <input type="text" class="form-control" name="phone" value="{{old('phone', @$requestaccount->phone)}}" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4">&nbsp;</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-8">
                                                                        <div class="row">
                                                                            <div class="col-lg-4">
                                                                                <button type="submit" class="btn btn-primary">Submit</button></div>
                                                                            <div class="col-lg-4">&nbsp;</div>
                                                                            <div class="col-lg-4">&nbsp;</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4">&nbsp;</div>
                                                                </div>
                                                            </div>
                                                    </form>
                                                    <div class="col-lg-4">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    @endsection