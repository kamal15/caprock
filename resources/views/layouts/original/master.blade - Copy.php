<!DOCTYPE html>
<html>
@include('layouts/header')
<body> 
<!-- banner -->
<div class="" id="home"> 
    @include('layouts/navigation') 
<!-- //banner -->
<!-- bootstrap-pop-up -->
	 @include('layouts/popup') 
<!-- //bootstrap-pop-up --> 
    <!--//service-section-->
    <div class="services" id="service" style="padding:0px;">
    <!-- ----------- Content page here---------- --> 
		@yield('content') 
    <!-- ----------- Content page here---------- --> 
    </div>
    <!--//service-section-->
</div>	 
@include('layouts/footer') 
<!-- js -->
@include('layouts/footer_js')

<style type="text/css">
		.error{color:red;}
		
		input:-webkit-autofill,
		input:-webkit-autofill:hover, 
		input:-webkit-autofill:focus,
		textarea:-webkit-autofill,
		textarea:-webkit-autofill:hover,
		textarea:-webkit-autofill:focus,
		select:-webkit-autofill,
		select:-webkit-autofill:hover,
		select:-webkit-autofill:focus {
		border: 1px solid green;
		-webkit-text-fill-color: #fff !important;
		-webkit-box-shadow: 0 0 0px 1000px #000 inset;
		transition: background-color 5000s ease-in-out 0s;
		}
	</style>
</body>
</html>