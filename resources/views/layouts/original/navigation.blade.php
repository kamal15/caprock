<div class="container" style="width:100%">
<div class="header-nav">
			<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a  href="index.html">
                            <img src="{{ url('resources/assets/images/caprock_logo.png') }}" style="width: 253px;padding-top:21px;"></a></h1>
					</div>
					<!-- navbar-header -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="index.html" class="hvr-underline-from-center active">Home</a></li>
							<li><a href="#about" class="hvr-underline-from-center scroll">About Us</a></li>
							<li><a href="#service" class="hvr-underline-from-center scroll">Deals</a></li>
							<li><a href="#team" class="hvr-underline-from-center scroll">Team</a></li>
							<li><a href="#gallery" class="hvr-underline-from-center scroll">Latest Cars</a></li>
							<li><a href="#contact" class="hvr-underline-from-center scroll">Contact Us</a></li>

							<li><a href="{{url('login')}}" class="hvr-underline-from-center  login_css">Login</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>	
				</nav>

		</div>
		 
	</div>
</div>
 