<head>
    <meta charset="UTF-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="alternate" hreflang="en" href="{{ Config::get('constants.SITE_ALT_URL') }}" />
    <title>{{ Config::get('constants.SITE_TITLE') }}</title>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="{{ url('/assets/images/Caprock-site-icon-01.png') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ url('/assets/images/Caprock-site-icon-01.png') }}" />
    <link rel='stylesheet' id='wp-block-library-css' href="{{ url('/assets/css/style.min.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome-css' href="{{url('/assets/css/font-awesome.min.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href="{{ url('/assets/css/style.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='contact-form-7-css' href="{{ url('/assets/css/custom-style.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href="{{url('/assets/css/settings.css')}}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='style-css' href="{{url('/assets/css/style.css')}}" type='text/css' media='all' />
    {{-- <link rel='stylesheet' id='mfn-base-css'
        href='http://caprock.theinnovatecompanies.com/wp-content/themes/betheme/css/base.css?ver=21.3.8' type='text/css'
        media='all' /> --}}

    <link rel='stylesheet' id='mfn-layout-css' href="{{url('/assets/css/layout.css')}}" type='text/css' media='all' />
    {{-- <link rel='stylesheet' id='mfn-shortcodes-css'
        href='http://caprock.theinnovatecompanies.com/wp-content/themes/betheme/css/shortcodes.css' type='text/css'
        media='all' /> --}}
    <link rel='stylesheet' href="{{url('/assets/css/jquery.ui.all.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='mfn-responsive-css' href="{{url('/assets/css/responsive.css')}}" type='text/css'
        media='all' /> 
    <link rel='stylesheet' href="{{url('/assets/css/style.min.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{url('/assets/css/custom_css.css')}}" type='text/css' media='all' />


    <script type='text/javascript' href="{{url('/assets/js/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{ url('/assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/assets/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
    .menu-children {
        display: none !important;
    }

    .assignment:hover .menu-children {
        display: block !important;
    }
</style>
<?php 
$clientLogoExists = '';
$session_data       = get_session_data();
//echo '<pre>';print_r($session_data );
$fk_company_id='';
if(!empty($session_data['fk_company_id'])){
    $fk_company_id= $session_data['fk_company_id'];
}
$com_logo_name = '';
$com_logo_name = company_logo($fk_company_id);  
?>
<div id=" Wrapper">
    <div id="Header_wrapper" style="margin-top: -69px;">
        <header id="Header" style=" margin-bottom:200px; ">
            <!-- .header_placeholder 4sticky  -->
            <div class="header_placeholder"></div>
            <div id="Top_bar" class="loading">
                <div class="container" style="max-width: 1345px">
                    <div class="column one">
                        <div class="top_bar_left clearfix" style="width:inherit;">
                            <!-- Logo -->
                            <?php
                                $session_data = get_session_data();
                                $logoExists = 0;
                                $caprock_Cl_logo = '';
                                $caprock_logo = '/assets/images/logo_main.png';
                                if(!empty($com_logo_name) && file_exists('./documents/company_logo/'.$com_logo_name)){
                                    $logoExists = 1; 
                                }  
                            ?>
                            <div class="logo"><a id="logo" href="{{url('/home')}}" title="Caprock Auto Remarketing"
                                    data-height="60" data-padding="15"> 
                                    <img class="logo-main scale-with-grid" style="max-height: 200%;width: 300px;margin-top: -2px;"
                                        src="{{ url($caprock_logo) }}"
                                        data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                        data-no-retina />
                                    <img class="logo-sticky scale-with-grid"
                                        src="{{ url($caprock_logo) }}"
                                        data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                        data-no-retina />
                                    <img class="logo-mobile scale-with-grid"
                                        src="{{ url($caprock_logo) }}"
                                        data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                        data-no-retina />
                                    <img class="logo-mobile-sticky scale-with-grid"
                                        src="{{ url($caprock_logo) }}"
                                        data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                        data-no-retina /></a>
                            </div>
                            <div class="logo"> 
                                       
                                        @if(@$session_data['role_name']!='Caprock Administrator' &&  $logoExists==1)
                                           <?php  $caprock_Cl_logo = '/documents/company_logo/'.$com_logo_name;?>

                                            <a id="logo" href="{{url('/home')}}" title="Caprock Auto Remarketing"
                                            data-height="60" data-padding="15"> 
                                            <img class="logo-main scale-with-grid" style="max-height: 200%;width: 300px;margin-top: -2px;"
                                                src="{{ url($caprock_Cl_logo) }}"
                                                data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                                data-no-retina />
                                            <img class="logo-sticky scale-with-grid"
                                                src="{{ url($caprock_Cl_logo) }}"
                                                data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                                data-no-retina />
                                            <img class="logo-mobile scale-with-grid"
                                                src="{{ url($caprock_Cl_logo) }}"
                                                data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                                data-no-retina />
                                            <img class="logo-mobile-sticky scale-with-grid"
                                                src="{{ url($caprock_Cl_logo) }}"
                                                data-retina="" data-height="258" alt="Caprock Auto Remarketing"
                                                data-no-retina /></a>
                                        @endif

                            </div>
                            <div class="menu_wrapper">
                                <nav id="menu">
                                    <ul id="menu-main-menu" class="menu menu-main" style="margin-bottom:0px;">
                                        @guest
                                        <li id="menu-item-317"
                                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                                class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                        <li id="menu-item-317"
                                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                                class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                        @else

                                        <li id="menu-item-429"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-315">
                                            <a href="{{ url('dashboard')}}"><span>Dashboard</span></a>
                                        </li>


                                        <li id="menu-item-429"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-315 {{ (Request::segment(1) == 'users' && (Request::segment(2) == '' || Request::segment(2) == 'create' ||  Request::segment(3) == 'edit' )) ? 'current-menu-item' : '' }} ">
                                            <a href="{{ route('users.index')}}"><span>Users</span></a>
                                        </li>

                                        @if(@$session_data['role_name']=='Caprock Administrator' )
                                        <li id="menu-item-317"
                                            class="menu-item menu-item-type-post_type menu-item-object-page {{ Request::segment(1) == 'companies' ? 'current-menu-item' : '' }} ">
                                            <a href="{{ route('companies.index')}}"><span>Company
                                                </span></a>
                                        </li>
                                        @endif
                                        <li id="menu-item-317"
                                            class="menu-item menu-item-type-post_type menu-item-object-page {{ Request::segment(1) == 'all-documents' ? 'current-menu-item' : '' }}">
                                            <a href="{{ route('documents.index')}}"><span>Documents
                                                </span></a></li>

                                        <li id="menu-item-331"
                                            class="menu-item menu-item-type-post_type menu-item-object-page assignment {{ Request::segment(2) == 'create' && Request::segment(1) == 'files-upload' ? 'current-menu-item' : '' }}">
                                            <a href="#"><span>Assignments</span></a>
                                            <ul class="menu-children">
                                                <li id="menu-item-331-1"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                                        class="dropdown-item"
                                                        href="{{ route('assignments.create')}}">{{ __('Create Assignment') }}</a>
                                                </li>
                                                <li id="menu-item-331-2"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                                        class="dropdown-item"
                                                        href="{{ route('files.upload') }}">{{ __('Upload Assignment') }}</a>
                                                </li>
                                            </ul><span class="holder"></span>
                                        </li>

                                        <li id="menu-item-331"
                                            class="menu-item menu-item-type-post_type menu-item-object-page {{ Request::segment(1) == 'portfolio' ? 'current-menu-item' : '' }}">
                                            <a class="nav-link" href="{{ route('portfolio') }}"><span>Portfolio</span>
                                            </a>
                                        </li>

                                        <li>
                                            <div class="dropdown" style="padding-top: 24px;">
                                                <button
                                                    class="menu-item menu-item-type-post_type menu-item-object-page btn btn-secondary"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    {{ Auth::user()->name }}<span class="dropdown-toggle"
                                                        style="padding-left: 3px;"></span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item"
                                                        href="{{ route('users.edit', auth()->user()->id)}}">My
                                                        Profile</a>
                                                    <a class="dropdown-item"
                                                        href="{{ route('logout') }}">{{ __('Logout') }}</a>

                                                </div>
                                            </div>
                                        </li>
                                        @endguest
                                        <!------------------ Auth Login ---- -->
                                    </ul>
                                </nav>
                            </div>

                            <div class="secondary_menu_wrapper">
                                <!-- #secondary-menu -->
                            </div>

                            <div class="banner_wrapper">
                            </div>

                            <div class="search_wrapper">
                                <!-- #searchform -->


                                <form method="get" id="searchform" action="">


                                    <i class="icon_search icon-search-fine"></i>
                                    <a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>

                                    <input type="text" class="field" name="s" placeholder="Enter your search" />
                                    <input type="submit" class="display-none" value="" />

                                </form>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </header>

    </div>
</div>