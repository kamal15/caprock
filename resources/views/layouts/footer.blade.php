<div id="Wrapper">
    <div id="Content">
        <div class="content_wrapper clearfix">
            <div class="sections_group">
                <div class="entry_content">
                    <div class="section mcb-section mcb-section-a5eb983ca  full-width hide-tablet hide-mobile"
                        style="padding-top:0px;padding-bottom:0px;background-color:">
                        <div class="section_wrapper mcb-section-inner">
                            <div class="wrap mcb-wrap mcb-wrap-22202043f one  valign-top clearfix" style="">
                                <div class="mcb-wrap-inner">
                                    <div class="column mcb-column mcb-item-57688fd24 one column_image">
                                        <div class="image_frame image_item no_link scale-with-grid no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid"
                                                    src="{{asset('assets/images')}}/Caprock-Collateral-Management-12-12-min.png"
                                                    alt="Caprock Collateral Management"
                                                    title="Caprock Collateral Management" width="7083" height="788">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section mcb-section mcb-section-cmjd33hda  full-width hide-desktop hide-tablet bg-cover"
                        style="padding-top:0px;padding-bottom:0px;background-color:">
                        <div class="section_wrapper mcb-section-inner">
                            <div class="wrap mcb-wrap mcb-wrap-6cbtdmpcu one  valign-top bg-cover clearfix" style="">
                                <div class="mcb-wrap-inner">
                                    <div class="column mcb-column mcb-item-sb7k190is one column_image">
                                        <div class="image_frame image_item no_link scale-with-grid no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid"
                                                    src="{{asset('/assets/images')}}/Caprock-Website-12-mob-12-min.png"
                                                    alt="" title="" width="3822" height="1840"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer id="Footer" class="clearfix">
    <div class="widgets_wrapper">
        <div class="container" style="max-width: 1345px;">
            {{-- <div class="column one-fourth">
                    <aside id="text-8" class="widget widget_text">
                        <h4>About our Blog</h4>
                        <div class="textwidget">
                            <p>Learn about the latest asset remarketing trends from the experts in the industry.</p>
                            <p><a href="http://caprock.theinnovatecompanies.com/blog">Visit blog</a></p>
                        </div>
                    </aside>
                </div> --}}
            <div class="column one-third">
                <aside id="nav_menu-9" class="widget widget_nav_menu">
                    <h4>Menu</h4>
                    <div class="d-flex">
                        <div class="menu-main-menu-container">
                            <ul id="menu-main-menu-1" class="menu">
                                <li id="menu-item-429"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-315 current_page_item menu-item-429">
                                    <a href="http://caprock.theinnovatecompanies.com/" aria-current="page">Home</a></li>
                                <li id="menu-item-317"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-317"><a
                                        href="http://caprock.theinnovatecompanies.com/companyprofile/">Company
                                        Profile</a></li>
                                <li id="menu-item-331"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-331"><a
                                        href="http://caprock.theinnovatecompanies.com/services/">Services</a></li>
                            </ul>
                        </div>
                        <div class="menu-main-menu-container">
                            <ul id="menu-main-menu-1" class="menu">
                                <li id="menu-item-356"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356"><a
                                        href="http://caprock.theinnovatecompanies.com/events/">Events</a></li>
                                <li id="menu-item-432"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432"><a
                                        href="http://caprock.theinnovatecompanies.com/blog/">Blog</a></li>
                                <li id="menu-item-321"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-321"><a
                                        href="http://caprock.theinnovatecompanies.com/contact/">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="column one-third">
                <aside id="wpcw_contact-6" class="widget wpcw-widgets wpcw-widget-contact">
                    <h4>Business Inquiry?</h4>
                    <ul>
                        <li class="has-label"><strong>Email</strong><br>
                            <div><a
                                    href="mailto:i&#110;&#113;&#117;&#105;ri&#101;&#115;&#64;capre&#109;&#46;c&#111;m">i&#110;&#113;&#117;&#105;ri&#101;&#115;&#64;capre&#109;&#46;c&#111;m</a>
                            </div>
                        </li>
                        <li class="has-label"><strong>Phone</strong><br>
                            <div>877-559-1767</div>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column one-third">
                <aside id="media_image-9" class="widget widget_media_image">
                    <h4>Other Services</h4><a href="https://www.innovateauto.com/"><img
                            src="{{ url('/assets/images/innovate_auto_finance.png') }}"
                            class="image wp-image-504  attachment-medium size-medium" alt=""
                            style="width: auto; max-height: 8vh;"
                            srcset="{{ url('/assets/images/innovate_auto_finance.png') }}" /></a>
                </aside>
                <aside id="media_image-11" class="widget widget_media_image"><a href="https://myinnovateloan.com/"><img
                            src="{{ url('/assets/images/innovate_loan_servicing.png') }}"
                            class="image wp-image-574  attachment-medium size-medium" alt=""
                            style="width: auto; max-height: 9vh;"
                            srcset="{{ url('/assets/images/innovate_loan_servicing.png') }} " /></a>
                </aside>
            </div>
        </div>
    </div>

    <div class="footer_copy">
        <div class="container" style="max-width: 1345px;">
            <div class="column one">
                <div class="copyright">© <?php echo date('Y');?> Caprock Auto Remarketing. All Rights Reserved. </div>
            </div>
        </div>
    </div>



</footer>

</div>

<script type='text/javascript' src="{{ url('/assets/js/scripts.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/core.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/widget.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/mouse.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/sortable.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/tabs.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/accordion.min.js') }}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mfn = {
        "mobileInit": "1240",
        "parallax": "translate3d",
        "responsive": "1",
        "lightbox": {
            "disable": false,
            "disableMobile": false,
            "title": false
        },
        "slider": {
            "blog": 0,
            "clients": 0,
            "offer": 0,
            "portfolio": 0,
            "shop": 0,
            "slider": 0,
            "testimonials": 0
        },
        "ajax": "http:\/\/caprock.theinnovatecompanies.com\/wp-admin\/admin-ajax.php"
    };
    /* ]]> */
</script>
<script type='text/javascript' src="{{ url('/assets/js/plugins.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/menu.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/animations.min.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/jplayer.min.js') }}"> </script>
<script type='text/javascript' src="{{ url('/assets/js/translate3d.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/scripts2.js') }}"></script>
<script type='text/javascript' src="{{ url('/assets/js/wp-embed.min.js') }}"></script>
<style>
    .table td,
    .table th {
        text-align: left !important;
    }
</style>
</body>

</html>