@if (count($errors) > 0)
<div class="alert alert-danger">
  <strong>Whoops!</strong> There were some problems with your input.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

@if (session()->has('message'))
<div class="alert alert-success" role="alert">
  <div class="alert-text">
    {{ session('message') }}
  </div>
</div>
@endif

@if (session()->has('message_err'))
<div class="alert alert-danger" role="alert">
  <div class="alert-text">
    {{ session('message_err') }}
  </div>
</div>
@endif