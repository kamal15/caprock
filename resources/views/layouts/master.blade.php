<!DOCTYPE html>
<html>
@include('layouts/header')
<body
    class="home page-template-default page page-id-315  color-custom style-default button-default layout-full-width no-content-padding header-fixed minimalist-header-no sticky-tb-color ab-hide subheader-both-center menu-line-below-80 menuo-right mobile-tb-hide mobile-mini-mr-ll be-reg-2138">
<!-- banner  mobile-side-slide -->
 
  
     
    <!-- ----------- Content page here---------- --> 
	@yield('content')	 
    <!-- ----------- Content page here---------- --> 
    
 

@include('layouts/footer') 
<!-- js -->
 

<style type="text/css">
		.error{color:red;}
		
		input:-webkit-autofill,
		input:-webkit-autofill:hover, 
		input:-webkit-autofill:focus,
		textarea:-webkit-autofill,
		textarea:-webkit-autofill:hover,
		textarea:-webkit-autofill:focus,
		select:-webkit-autofill,
		select:-webkit-autofill:hover,
		select:-webkit-autofill:focus {
		border: 1px solid green;
		-webkit-text-fill-color: #fff !important;
		-webkit-box-shadow: 0 0 0px 1000px #000 inset;
		transition: background-color 5000s ease-in-out 0s;
		}
	</style>
</body>
</html>