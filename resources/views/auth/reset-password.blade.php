@extends('layouts/login_master')
@section('content')
    <form method="POST" autocomplete="off" action="{{ url('reset-password') }}" class="login100-form validate-form">
        @csrf
        <input type="hidden" id="token" name="token" value="{{@$token}}">
        <span class="login100-form-title p-b-26">
        {{ __('Reset Password') }}
    </span>
        <span class="login100-form-title p-b-48">
        <i class="zmdi zmdi-font"></i>
    </span>

        <div class="wrap-input100 validate-input">
            <input autocomplete="false" id="email" type="email" class="input100 @error('email') is-invalid @enderror"
                   name="email" value="{{ old('email', request('email')) }}" required autofocus>
            @error('email')
            <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
            <span class="focus-input100" data-placeholder="Email"></span>
        </div>

        <div class="wrap-input100 validate-input">
            <input autocomplete="false" id="password" autocomplete="off" type="password"
                   class="input100 @error('password') is-invalid @enderror" name="password"
                   value="{{ old('password') }}" required autofocus>
            @error('password')
            <span class="invalid-feedback" style="display:block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
            @enderror
            <span class="focus-input100" data-placeholder="New Password"></span>
        </div>

        <div class="wrap-input100 validate-input">
            <input autocomplete="false" id="password_confirmation" autocomplete="off" type="password"
                   class="input100 @error('password_confirmation') is-invalid @enderror"
                   name="password_confirmation"
                   value="{{ old('password_confirmation') }}" required autofocus>
            @error('password_confirmation')
            <span class="invalid-feedback" style="display:block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
            @enderror
            <span class="focus-input100" data-placeholder="Confirm Password"></span>
        </div>

        <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button type="submit" class="login100-form-btn">
                    {{ __('Reset Password') }}
                </button>
            </div>
        </div>

        <div class="form-group row mb-0" style="margin-top: 40px;">
            <div class="col-md-12 text-center">
                <a class="btn btn-link" href="{{ route('login') }}">
                    {{ __('Continue to login?') }}
                </a>
            </div>
        </div>
    </form>
    <style>
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus
    </style>
@endsection