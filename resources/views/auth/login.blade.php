@extends('layouts/login_master')
@section('content')
 
<form method="POST" autocomplete="off" action="{{ route('login') }}" class="login100-form validate-form">
    @csrf
    <span class="login100-form-title p-b-26">
        Welcome
    </span>
    <span class="login100-form-title p-b-48">
        <i class="zmdi zmdi-font"></i>
    </span>

    <div class="wrap-input100 validate-input">
        <input autocomplete="false" id="email" type="email" class="input100 @error('email') is-invalid @enderror"
            name="email" value="{{ old('email') }}" required autofocus>
        @error('email')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <span class="focus-input100" data-placeholder="Email"></span>
    </div>

    <div class="wrap-input100 validate-input">
        <span class="btn-show-pass">
            <i class="zmdi zmdi-eye"></i>
        </span>

        <input autocomplete="false" id="password" type="password"
            class="input100  @error('password') is-invalid @enderror" name="password" required autofocus>

        @error('password')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <span class="focus-input100" data-placeholder="Password"></span>



    </div>

    <div class="container-login100-form-btn">
        <div class="wrap-login100-form-btn">
            <div class="login100-form-bgbtn"></div>
            <button type="submit" class="login100-form-btn">
                {{ __('Login') }}
            </button>

        </div>
    </div>






    {{-- <div class="form-check">
        <input class="form-check-input" type="checkbox" name="remember" id="remember"
            {{ old('remember') ? 'checked' : '' }}>

    <label class="form-check-label" for="remember">
        {{ __('Remember Me') }}
    </label>
    </div> --}}
    <div class="text-left p-t-115">
        <span class="txt1">
            Don’t have an account?
        </span>
        <a class="btn-link" href="{{url('register')}}">
            Request Account
        </a>
    </div>

     <div class="form-group row mb-0">
        <div class="col-md-12 text-center">
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </div>
    </div>
</form>
<style>
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
</style>
@endsection