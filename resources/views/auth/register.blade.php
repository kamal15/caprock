@extends('layouts/login_master')
@section('content')
<form method="POST" action="{{url('/account/create')}}" class="login100-form validate-form">
    @csrf
    <span class="login100-form-title p-b-26">
        Request New Account
    </span>
    <span class="login100-form-title p-b-48">
        <i class="zmdi zmdi-font"></i>
    </span>

    <div class="wrap-input100 validate-input">
        <div class="col-md-6">
            <input id="name" type="text" class="input100 @error('name') is-invalid @enderror" name="name"
                value="{{ old('name') }}" autocomplete="off" autofocus>

            @error('name')
            <span class="invalid-feedback" style="display:block" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <span class="focus-input100" data-placeholder="Name"></span>
        </div>
    </div>

    <div class="wrap-input100 validate-input">
        <input id="email" type="text" class="input100 @error('email') is-invalid @enderror" name="email"
            value="{{ old('email') }}" autocomplete="off" autofocus>


        @error('email')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        <span class="focus-input100" data-placeholder="Email Address"></span>
    </div>

    <div class="wrap-input100 validate-input">
        <input id="phone" type="password" class="input100 @error('phone') is-invalid @enderror" name="phone"
            value="{{ old('phone') }}" autocomplete="off" autofocus>


        @error('phone')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <span class="focus-input100" data-placeholder="Phone"></span>
    </div>

    <div class="container-login100-form-btn">
        <div class="wrap-login100-form-btn">
            <div class="login100-form-bgbtn"></div>
            <button type="submit" class="login100-form-btn">
                {{ __('Submit') }}
            </button>

        </div>
    </div>


</form>
<div class="text-center p-t-115" style="padding-top:20px;">
    <span class="txt1">
        Already have an account?
    </span>

    <a class="btn-link" href="{{url('login')}}">
        Login
    </a>
</div>


@endsection