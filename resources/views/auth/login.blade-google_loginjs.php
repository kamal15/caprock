@extends('layouts/login_master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<meta name="google-signin-client_id" content="980497521894-vko37dkmito72j92a3suuchh2ucn0kol.apps.googleusercontent.com.apps.googleusercontent.com">
 
    <script>
    //------------------------Google signup Start---------------------------------------//
     function logout()
     {
         gapi.auth.signOut();
         location.reload();
     }
     function login() 
     {
       var myParams = {
    
        'clientid' : '980497521894-vko37dkmito72j92a3suuchh2ucn0kol.apps.googleusercontent.com',
    
        'cookiepolicy' : 'single_host_origin',
    
        'callback' : 'loginCallback',
    
        'approvalprompt':'force',
    
        'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'//APP Use Passthebook
    
      };
       gapi.auth.signIn(myParams);
     }
    
    
     
    function loginCallback(result)
     {
        if(result['status']['signed_in'])
        {
            var request = gapi.client.plus.people.get(
            {
                'userId': 'me'
            });
            request.execute(function (resp)
            {
                var email = '';
                if(resp['emails'])
                {
                    for(i = 0; i < resp['emails'].length; i++)
                    {
                        if(resp['emails'][i]['type'] == 'account')
                        {
                            email = resp['emails'][i]['value'];
                        }
                    }
                }
    
                var str_name 	= resp['displayName'];
                var gender 	= resp['gender'];
                var id 	= resp['id'];
                var str_email	= email; 
                
                 
                     $.ajax({
                        type: "POST",
                        url: 'http://test.innovate.com/login',
                        data: {google_id : id, email: str_email,name:str_name,modeoflogin:'1', gender:gender},
                        beforeSend: function() {
                        $("#waiting-ajax").show();
                        $("#fbLink").hide();
                             },
                        dataType : "json",
                        success: function(msg) { 
                        $("#waiting-ajax").hide();
                            if(parseInt(msg)==0){
                                $("#fbLink").show();
                                alert('Login With Gmail Fail!');return false;
                            }
                                 if(window.location=='http://test.innovate.com/login'){
                        window.location.href='http://test.innovate.com/login';
                    }else{
                        window.location.href='http://test.innovate.com/login';
                    }
                         } 
                    });
                 
                   //str += "Image:" + resp['image']['url'] + "<br>";
                 //str += "<img src='" + resp['image']['url'] + "' /><br>";
                 //str += "URL:" + resp['url'] + "<br>";
                // document.getElementById("profile").innerHTML = str; 
             });
         }
     }
    
    function onLoadCallback()
     {
         gapi.client.setApiKey('AIzaSyCauTEI7OVM8g2_j9E6c1behlI5maTRfTM');
         gapi.client.load('plus', 'v2',function(){});
     }
          </script>
     <script type="text/javascript">
           (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
     //------------------------Google signup End---------------------------------------//
    </script>

<img id="waiting-ajax" src="https://travelladda.com/assets/images/loading.gif" width="40px" style="display:none;" />
 
<a href="javascript:void(0)" onClick="login()" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
</div>





============================================================================
<form method="POST" autocomplete="off" action="{{ route('login') }}" class="login100-form validate-form">
    @csrf
    <span class="login100-form-title p-b-26">
        Welcome
    </span>
    <span class="login100-form-title p-b-48">
        <i class="zmdi zmdi-font"></i>
    </span>

    <div class="wrap-input100 validate-input">
        <input autocomplete="false" id="email" type="email" class="input100 @error('email') is-invalid @enderror"
            name="email" value="{{ old('email') }}" required autofocus>
        @error('email')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <span class="focus-input100" data-placeholder="Email"></span>
    </div>

    <div class="wrap-input100 validate-input">
        <span class="btn-show-pass">
            <i class="zmdi zmdi-eye"></i>
        </span>

        <input autocomplete="false" id="password" type="password"
            class="input100  @error('password') is-invalid @enderror" name="password" required autofocus>

        @error('password')
        <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <span class="focus-input100" data-placeholder="Password"></span>



    </div>

    <div class="container-login100-form-btn">
        <div class="wrap-login100-form-btn">
            <div class="login100-form-bgbtn"></div>
            <button type="submit" class="login100-form-btn">
                {{ __('Login') }}
            </button>

        </div>
    </div>






    {{-- <div class="form-check">
        <input class="form-check-input" type="checkbox" name="remember" id="remember"
            {{ old('remember') ? 'checked' : '' }}>

    <label class="form-check-label" for="remember">
        {{ __('Remember Me') }}
    </label>
    </div> --}}
    <div class="text-left p-t-115">
        <span class="txt1">
            Don’t have an account?
        </span>

        <a class="txt2" href="{{url('register')}}">
            Request Account
        </a>
    </div>

    {{-- <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">

            @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
    {{ __('Forgot Your Password?') }}
    </a>
    @endif
    </div>
    </div> --}}
</form>
<style>
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
</style>
@endsection