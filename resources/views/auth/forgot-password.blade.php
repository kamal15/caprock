@extends('layouts/login_master')
@section('content')
    @if(session()->has('status'))
        <div class="alert alert-dismissible fade show" role="alert" style="background-color: #70af3d; color: #ffffff;">
            {{session('status')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form method="POST" autocomplete="off" action="{{ url('forgot-password') }}" class="login100-form validate-form">
        @csrf
        <span class="login100-form-title p-b-26">
        {{ __('Forgot Password') }}
    </span>
        <span class="login100-form-title p-b-48">
        <i class="zmdi zmdi-font"></i>
    </span>

        <div class="wrap-input100 validate-input">
            <input autocomplete="false" id="email" type="email" class="input100 @error('email') is-invalid @enderror"
                   name="email" value="{{ old('email') }}" required autofocus>
            @error('email')
            <span class="invalid-feedback" style="display:block" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
            <span class="focus-input100" data-placeholder="Email"></span>
        </div>

        <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button type="submit" class="login100-form-btn">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </div>

        <div class="text-left p-t-115">
            <span class="txt1">
                Already have an account?
            </span>

            <a class="btn-link" href="{{url('login')}}">
                Login
            </a>
        </div>
    </form>
    <style>
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus
    </style>
@endsection