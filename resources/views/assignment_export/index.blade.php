@extends('layouts.master')
@section('content')
@php
$status_arr = array('Inactive', 'Active', 'Deleted');
@endphp
<?php  // echo '<pre>';print_r($Assignment_Client->toarray());exit;
$Assignment_ClientArray = array();
 if($Assignment_Client){
    $Assignment_ClientA = $Assignment_Client->toArray(); 
    $Assignment_ClientArray = $Assignment_ClientA['data'];
    // echo '<pre>';print_r($Assignment_ClientArray);exit;
  }
    ?>
<div class="section_wrapper mcb-section-inner">
  <div class="row">
    <div class="col-lg-12 margin-tb alert alert-danger bg-caprock-col" >
      <div class="col-md-3 pull-left" style="top:8px;">
        <h2>Export Assignment</h2>
      </div>
       
      <div class="col-md-6 pull-left" style="top:8px;">
        <form name="frm_srch" id="frm_srch" action="{{ route('assignment-export') }}" method="POST">
          @csrf
          <div class="form-group row">
            <div class="col-md-3">
              <input style="background: #fff;color: #797575;border-radius: 5px;" type="date" placeholder="Date from" autocomplete="off" class="form-control" name="date_from" id="date_from"> 
            </div>
            <div class="input-group col-md-3">
              <input style="background: #fff;color: #797575;border-radius: 5px;" type="date" placeholder="Date from" autocomplete="off" class="form-control" name="date_to" id="date_to"> 
            </div> 

            <div class="input-group col-md-2">
              <input type="submit" class="btn btn-info" name="submit" id="submit" value="Submit"> 
            </div>
          </div>
        </form>

      </div>

      <div class="col-md-3 pull-left" style="top:8px;">
        <form name="frm_srch" id="frm_srch" action="{{ route('export-data') }}" method="POST">
          @csrf
          <input style="background: #fff;color: #797575;display:none;" type="date" placeholder="Date from" autocomplete="off" class="form-control" name="date_from" id="date_from"> 

          <input style="background: #fff;color: #797575;display:none;" type="date" placeholder="Date from" autocomplete="off" class="form-control" name="date_to" id="date_to"> 
          <input type="submit" class="btn btn-info" name="submit" id="submit" value="Export">  
        </form> 
      </div> 
    </div>
  </div>
  @include('layouts.error')
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th class="text-justify">Assignment type</th>
      <th class="text-justify">Repo type</th>
      <th width="280px">Repo status</th>
      <th width="280px">Make</th>
      <th width="280px">Model</th>
      <th width="280px">Vin</th>
      <th width="280px">Ssn</th>
      <th width="280px">Scheduled sale date</th>
    </tr>
    @php $i = 0; @endphp 
    @foreach ($Assignment_ClientArray as $ass)
    <?php //echo '<pre>';print_r($ass);exit;
    @$scheduled_sale_date = date('m-d-Y', '13745376' );//general_date_format($ass['legal_sale_date'],'yes');
    ?>
    <tr> 
      <td class="text-justify">{{ @$ass['assignment_type'] }}</td>
      <td class="text-justify">{{ @$ass['repo_type'] }}</td>
      <td class="text-justify">{{ @$ass['repo_status'] }}</td>
      <td class="text-justify">{{ @$ass['make'] }}</td>
      <td class="text-justify">{{ @$ass['model'] }}</td>
      <td class="text-justify">{{ @$ass['year'] }}</td>
      <td class="text-justify">{{ @$ass['vin'] }}</td>
      <td class="text-justify">{{ @$ass['ssn'] }}</td> 
      <td class="text-justify">{{ @$scheduled_sale_date }}</td>    
    </tr>
    @endforeach
  </table> 
  {{ $Assignment_Client->appends(Request::except('page'))->links() }} 
</div>
<script>
  function submit_form(){
    document.getElementById('frm_srch').submit();
  } 
</script>
@endsection