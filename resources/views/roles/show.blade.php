@extends('layouts/master')
@section('content')
<div class="section_wrapper mcb-section-inner">
 


<div class="row">
    <div class="col-lg-12 margin-tb">
    <div class="col-md-4">&nbsp;</div>
        
        <div class="col-md-3 pull-left">
    @if(session('impersonated_by') )
     <a class="alert alert-info pull-left" href="<?php echo url('impersonate_leave') ;?>">Back to my  account</a> 
    @else
    <!-- <li> <a class="btn btn-info" href="{{ route('logout') }}">Logout</a></li>  -->
    @endif</div>
        <div class="pull-right">
            @can('role-create')
            <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
            @endcan
        </div>
        
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<?php //echo '<pre>';print_r($roles);exit;?>

<div class="row">
      <div class="col-md-3"> &nbsp;</div>
      <div class="col-md-6">
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">Show Roles Detail</h3>
          </div>
          <div class="card-body">
            
             <strong>Name:</strong>
            {{ $role->name }}            
          

          <div class="card-body">
           
              <strong>Permissions:</strong>
                 @if(!empty($rolePermissions))
                 <ul>
                @foreach($rolePermissions as $v)
                    <li class="label label-success">{{ $v->name }},</li>
                @endforeach
                </ul>
            @endif   
          </div>

           


        </div>
      </div>
    </div>

</div></div>
   <style>.card-body{padding:0.5rem;}</style>
    @endsection