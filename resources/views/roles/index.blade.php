@extends('layouts/master')
@section('content')
<div class="section_wrapper mcb-section-inner">



    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="col-md-4">&nbsp;</div>
            <div class="col-md-4 pull-left">
                <h2>Role Management</h2>
            </div>
            <div class="col-md-3 pull-left">
                @if(session('impersonated_by') )
                <a class="alert alert-info pull-left" href="<?php echo url('impersonate_leave') ;?>">Back to my
                    account</a>
                @else
                <!-- <li> <a class="btn btn-info" href="{{ route('logout') }}">Logout</a></li>  -->
                @endif</div>
            <div class="pull-right">
                @can('role-create')
                <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>
                @endcan
            </div>

        </div>
    </div>


    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <?php //echo '<pre>';print_r($roles);exit;?>
    <div class="row col-md-12">

        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <th width="20%">No</th>
                    <th width="40%">Name</th>
                    <th>Action</th>
                </tr>
                @foreach ($roles as $key => $role)
                <?php //echo '<pre>';print_r($role);exit;?>
                @if($role->id!=1)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name }}</td>
                    <td style="text-align:left">
                        <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                        @can('role-edit')
                        <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                        @endcan
                        @can('role-delete')
                        {!! Form::open(['method' => 'DELETE','route' =>
                        ['roles.destroy',$role->id],'style'=>'display:inline']) !!}
                        {!! Form::submit('Delete', ['style'=>'padding: 7px 19px;margin-bottom: 10px;']) !!}
                        {!! Form::close() !!}
                        @endcan
                        {{-- @if(!empty($role->model_id))
                        <a class="btn btn-success" href="{{ url('impersonate/'.$role->model_id) }}"
                        data-toggle='tooltip' data-placement='top' title='Impersonate'>login as
                        {{ $role->name }}</a>
                        @endif --}}
                    </td>
                </tr>
                @endif
                @endforeach
            </table>

        </div>


        {!! $roles->render() !!}

    </div>
</div>

@endsection