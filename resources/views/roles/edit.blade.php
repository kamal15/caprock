@extends('layouts.master') 
@section('content') 
 <!---- Form Section ---->
 <section class="content"> 
<div class="container-fluid">
    
  <div class="card card-default" style="box-shadow:none;">
 

 <div class="row">
 <div class="col-md-3">&nbsp;</div> 
 <div class="col-md-5">&nbsp;</div>
 <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a></div> 
 </div>
 
  
 <div class="row">
 <div class="col-md-3">&nbsp;</div> 
 <div class="col-md-6">   
 @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif
 </div>
 </div>
{!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
    <div class="row">
      <div class="col-md-3"> &nbsp;</div>
      <div class="col-md-6">
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">Update Role</h3>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label>Name:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</i></span>
                </div>
               
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
               
              </div>
            </div>
            <div class="form-group">
            <label>Permission:</label>
              <div class="">
                  <ul >
                 @foreach($permission as $value)
                <li>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                {{ $value->name }}</li>
                 @endforeach
</ul>
            <br/>
              </div>
            </div>
            
             
             
             <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}
  </div>
</section>
<!---- Form Section ---->
 
 
@endsection


















 