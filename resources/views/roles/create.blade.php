@extends('layouts.master') 
@section('content') 
 <!---- Form Section ---->
 <section class="content"> 
<div class="container-fluid">
    
  <div class="card card-default" style="box-shadow:none;">
 

 <div class="row">
 <div class="col-md-3">&nbsp;</div> 
 <div class="col-md-5"><h2>Create New Role</h2></div>
 <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a></div> 
 </div>
 
  
 <div class="row">
 <div class="col-md-3">&nbsp;</div> 
 <div class="col-md-6">   
 @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif
 </div>
 </div>
  {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
    <div class="row">
      <div class="col-md-3"> &nbsp;</div>
      <div class="col-md-6">
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">Create Role</h3>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label>Name:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</i></span>
                </div>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
               
              </div>
            </div>
            <div class="form-group">
            <label>Permission:</label>
              <div class="input-group">
               <strong>Permission:</strong>
            <br/>
            <ul>
            @foreach($permission as $value)
               <li> <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                {{ $value->name }}</label></li>
            <br/>
            @endforeach
            </ul>
              </div>
            </div>
             
             
            <div class="card-footer">
             
              <button type="submit" class="btn btn-info">Submit</button>
              <button type="submit" class="btn btn-default float-right">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</section>
<!---- Form Section ---->
 
 
@endsection