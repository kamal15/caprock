@extends('layouts.master')
@section('content')
<style>
  a:hover {
    color: #d2d6da;
  }
</style>
<div class="section_wrapper mcb-section-inner">
  <!---- Form Section ---->
  <section class="content">
    <div class="container-fluid">
      <div class="card card-default" style="box-shadow:none;">
        <div class="row">
          <div class="col-md-3">&nbsp;</div>
          <div class="col-md-5">
            <h2>Upload Assignment</h2>
          </div>
          <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">
          <div class="col-md-3"> &nbsp;</div>
          <div class="col-md-6">
            @include('layouts.error')
            <div class="card card-warning">
              <div class="card-body">
                <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group row">
                    <a href="{{ asset('sample/File_Mapping_and_Field_Requirements_10.27.20.xlsx')}}"
                      class="alert bg-caprock-col" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>
                      Download a Sample File</a>
                  </div>
                  <div class="form-group row">
                    <label class="col-2 col-form-label">CSV File</label>
                    <div class="col-8">
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="inputGroupFile01" multiple
                            name="files_input[]" aria-describedby="inputGroupFileAddon01">
                          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="input-group">
                        <div class="input-group-prepend"> </div>
                        <button type="submit" id="submit" name="submit" class="btn btn-primary btn-wide"> Save
                        </button>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!---- Form Section ---->
</div>
<script>
  $('#inputGroupFile01').on('change',function(){
      //get the file name
      var fileName = $(this).val(); 
      fileName =fileName.replace("C:\\fakepath\\", "");
      //replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
  })
</script>
@endsection