@extends('layouts.master') 
@section('content')
<link rel="stylesheet" href="{{url('/assets/js/bar_chart/bar.chart.min.css')}}" />
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script src='https://d3js.org/d3.v4.min.js'></script>
<script src="{{url('/assets/js/bar_chart/jquery.bar.chart.min.js')}}"></script>
<?php $dataRows = create_graph_data_pie($graph_data['title_status']);
//$dataAssignTitle_status = create_graph_data_pie($graph_data['title_status']);
 $final_data_supplyArr = explode(',',$dataRows['final_data_supply']);
  $year_arrArr = explode(',',$dataRows['year_arr']);
//echo '<pre>';print_r(explode(',',$dataRows['final_data_supply']));exit;
 //echo '<pre>';print_r($year_arrArr);exit;
 for($i=0;$i<@count($final_data_supplyArr);$i++){
   $dataPoints2[$i]['label']=$year_arrArr[$i];
   $dataPoints2[$i]['y']=$final_data_supplyArr[$i];

  // array("label"=>$dataRows['year_arr'][$i], "y"=>$dataRows['final_data_supply'][$i]),
 }
  $dataPoints = $dataPoints2;
    //echo '<pre>';print_r($dataPoints);exit;
  ?> 
  
 
<div class="container"> 
    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-bottom: 100px;">
            <div class="card">
                <div class="card-header">Dashboard</div> 
                <div class="card-body" style="padding-bottom:100px;"> 
                   <h1>Average days to reposses</h1> 
                    <div id="chart_container"> 
                        <canvas id="k_chart_sales_statistics" height="300"></canvas>
                     </div> 
                </div>

                <div class="card-body" style="padding-bottom:100px;"> 
                   <h1>Average days to sell</h1> 
                    <div id="chart_container2"> 
                        <canvas id="k_chart_sales_statistics2" height="300"></canvas>
                     </div> 
                </div>

                 <div class="card-body" style="padding-bottom:100px;"> 
                   <h1>Title Status</h1> 
                    <div id="chart_container4"> 
                      
                       <div <?php if(!empty(@$dataPoints[0]['label'])){?>id="chartContainer"<?php }?> style="height: 370px; width: 100%;"></div>
                       
                        
                     </div> 
                </div>

                <div class="card-body" style="padding-bottom:100px;"> 
                   <h1>Average Sales Price</h1> 
                    <div id="chart_container3"> 
                        <canvas id="k_chart_sales_statistics3" height="300"></canvas>
                     </div> 
                </div>

                 <div class="card-body" style="padding-bottom:100px;"> 
                   <h1>Average Vehicle Grade</h1> 
                    <div id="chart_container6"> 
                        <canvas id="k_chart_sales_statistics4" height="300"></canvas>
                     </div> 
                </div>

               
            </div>
        </div>
    </div>
</div> 
<!----------------------- Pie chart start --------------------------- -->
 
 
<script>
window.onload = function() { 
var chart = new CanvasJS.Chart("chartContainer", {
   animationEnabled: true,
   title: {
      text: "Title Status"
   },
   subtitles: [{
      text: ""
   }],
   data: [{
      type: "pie",
      yValueFormatString: "#,##\"\"",
      indexLabel: "{label} ({y})",
      dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
   }]
});
chart.render();
 
}
</script>
 

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!----------------------- Pie chart End --------------------------- -->
 
<?php 
  //  echo '<pre>';print_r($graph_data);exit;
$dataAssignRep          = create_graph_data($graph_data['average_days_to_repo']);
$dataAssignSell         = create_graph_data($graph_data['average_days_to_sell']);
$dataAssignSalesPrice   = create_graph_data($graph_data['avg_sales_price']); 
$dataVehicleGrade       = create_graph_data($graph_data['VehicleGrade']);
 //echo '<pre>';print_r($dataAssignSell );exit;
?>
<script>
var KAppOptions = {
"colors": {
   "state": {
      "brand": "#5d78ff",
      "metal": "#c4c5d6",
      "light": "#ffffff",
      "accent": "#00c5dc",
      "primary": "#5867dd",
      "success": "#34bfa3",
      "info": "#36a3f7",
      "warning": "#ffb822",
      "danger": "#fd3995",
      "focus": "#9816f4"
   },
   "base": {
      "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
      "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
   }
}
};
</script> 
<script src="{{url('assets/graphs/jquery.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/graphs/Chart.bundle.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/graphs/scripts.bundle.js')}}" type="text/javascript"></script> 
 <script>
    var KDashboard = function() { 
    var widgetSalesStatisticsChart = function() {
      //================================ Repossess graphs ====================================//
       <?php if(!empty($dataAssignRep['maxValue']) && !empty($dataAssignRep['year_arr'])){?> 
        if (!document.getElementById('k_chart_sales_statistics')) {
           // return;
        }

        var MONTHS = ['1 Aug', '2 Aug', '3 Aug', '4 Aug', '5 Aug', '6 Aug', '9 Aug'];

        var color = Chart.helpers.color;
        var barChartData = {
          labels: [<?php echo $dataAssignRep['year_arr'];?>],
          datasets:[ 
            {
               label: 'Count',
               backgroundColor: color(KApp.getStateColor('warning', 1)).alpha(1).rgbString(),
               borderWidth: 0,
               data:[<?php echo $dataAssignRep['final_data_supply'];?>]
            } 
            ] 
        }; 
        var ctx = document.getElementById('k_chart_sales_statistics').getContext('2d');
        var myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 11,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: false,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: <?php echo $dataAssignRep['maxValue'];?>,                            
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 12,
                            padding: 30
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 15,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });

        <?php }?>
//================================ Repossess graphs ====================================//
 //================================ average sell graphs ====================================//
 <?php if(!empty($dataAssignSell['maxValue']) && !empty($dataAssignSell['year_arr'])){?> 
        var barChartData2 = {
          labels: [<?php echo $dataAssignSell['year_arr'];?>],
          datasets:[ 
            {
               label: 'Count',
               backgroundColor: color(KApp.getStateColor('danger', 1)).alpha(1).rgbString(),
               borderWidth: 0,
               data:[<?php echo $dataAssignSell['final_data_supply'];?>]
            } 
            ] 
        };
        var ctx = document.getElementById('k_chart_sales_statistics2').getContext('2d');
         var myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData2,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 11,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: false,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: <?php echo $dataAssignSell['maxValue'];?>,                            
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 12,
                            padding: 30
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 15,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
   
                     left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
         <?php }?>
//================================ average sell graphs ====================================//

//================================ average sales Price graphs ====================================//
<?php if(!empty($dataAssignSalesPrice['maxValue']) && !empty($dataAssignSalesPrice['year_arr'])){?>
        var barChartData3 = {
          labels: [<?php echo $dataAssignSalesPrice['year_arr'];?>],
          datasets:[ 
            {
               label: 'Count',
               backgroundColor: color(KApp.getStateColor('info', 1)).alpha(1).rgbString(),
               borderWidth: 0,
               data:[<?php echo $dataAssignSalesPrice['final_data_supply'];?>]
            } 
            ] 
        };
        var ctx = document.getElementById('k_chart_sales_statistics3').getContext('2d');
         var myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData3,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 11,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: false,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: <?php echo $dataAssignSalesPrice['maxValue'];?>,                            
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 12,
                            padding: 30
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 15,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
      <?php }?>
//================================ average sales price graphs ====================================//
   
//================================ average vehicle grade graphs ====================================//
   <?php if(!empty($dataVehicleGrade['maxValue']) && !empty($dataVehicleGrade['year_arr'])){?>
        var barChartData4= {
          labels: [<?php echo $dataVehicleGrade['year_arr'];?>],
          datasets:[ 
            {
               label: 'Count',
               backgroundColor: color(KApp.getStateColor('success', 1)).alpha(1).rgbString(),
               borderWidth: 0,
               data:[<?php echo $dataVehicleGrade['final_data_supply'];?>]
            } 
            ] 
        };
        var ctx = document.getElementById('k_chart_sales_statistics4').getContext('2d');
         var myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData4,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 11,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: false,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: <?php echo $dataVehicleGrade['maxValue'];?>,                            
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KApp.getBaseColor('shape', 3),
                            fontSize: 12,
                            padding: 30
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 15,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
         <?php }?>
//================================ average vehicle grade graphs ====================================//

    } 
    return {
        init: function() { 
            widgetSalesStatisticsChart(); 
        }
    };
}();

// Class initialization
jQuery(document).ready(function() {
    KDashboard.init();
});
 </script>
<script src="{{url('assets/graphs/app.bundle.js')}}" type="text/javascript"></script> 
<style type="text/css">.canvasjs-chart-credit{display: none;}</style>
@endsection
