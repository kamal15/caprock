@extends('layouts.master')


@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-bottom: 100px;">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body" style="padding-bottom:100px;"> 
                    <div id="chart_container"> 
                     <!-- --------------- chart1 Start ---------------- -->
                      <div id="column-chart" class="chart-div"></div>
                     <!-- --------------- chart1 End ------------------ -->

                     <!-- --------------- chart2 Start ---------------- -->
                     <div id="column-chart2" class="chart-div"></div>
                     <!-- --------------- chart2 End ------------------ -->

                     <!-- --------------- chart3 Start ---------------- -->
                     <div id="column-chart3" class="chart-div"></div>
                     <!-- --------------- chart3 End ------------------ -->

                      <!-- --------------- chart4 Start ---------------- -->
                      <div id="pie-chart" class="chart-div"></div> 
                     <!-- --------------- chart4 End ------------------ -->

                     <!-- --------------- chart5 Start ---------------- -->
                     <div id="column-chart4" class="chart-div"></div>
                     <!-- --------------- chart5 End ------------------ -->

                     <!-- --------------- chart6 Start ---------------- -->
                     <div id="column-chart5" class="chart-div"></div>
                     <!-- --------------- chart6 End ------------------ --> 
                     </div> 
                </div>
            </div>
        </div>
    </div>
</div> 
<?php 
$color = ['#8c0C04','#fec62a','#9ee2d9','#9f9ee2','#e29eba','#8c0C04','#fec62a','#9ee2d9','#9f9ee2','#e29eba'];
//$color = ['#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04','#8c0C04',];
$country = array();
$people = array();

//echo '<pre>';print_r($assignmentsByState);exit; 
$resultCount= @count($assignmentsByState);
foreach ($assignmentsByState as $peopleData) {
    $country[] = $peopleData['Client'];
    $people[] = $peopleData['Identifier'];
}
$blank= '';
?>
 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
      
    google.charts.setOnLoadCallback(drawPieChart); 
    function drawPieChart() {

      var data = new google.visualization.arrayToDataTable([
        ["Country","People"],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$country[$i]."', ".$people[$i] ?>],
        <?php } 
        ?>
        ]);

      var options = {
          title: "Title Status",
          width: '10%',
          height: '50px',
          colors: [
            <?php
            for($i=0;$i<$resultCount;$i++) {
              echo "'".$color[$i]."',";
            } 
            ?>
          ]
        };
      var chart = new google.visualization.PieChart(document.getElementById('pie-chart'));
      chart.draw(data, options);
    }  

  google.charts.load("current", {packages:['corechart']});
  google.charts.setOnLoadCallback(drawColumnChart);
      
    function drawColumnChart() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Population', { role: 'style' }, { role: 'annotation' }],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$blank."', ".$people[$i].", '".$color[$i]."' , "."'".$people[$i]."'" ?>],
        <?php } 
        ?>
        ]);


      var options = {
        title: "Average Days to Repossess",
        chartArea: {width: '35%'},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("column-chart"));
      chart.draw(data, options);
  }
google.charts.setOnLoadCallback(drawColumnChart2);
  function drawColumnChart2() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Population', { role: 'style' }, { role: 'annotation' }],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$country[$i]."', ".$people[$i].", '".$color[$i]."' , "."'".$people[$i]."'" ?>],
        <?php } 
        ?>
        ]);


      var options = {
        title: "Average Days to Sell",
        chartArea: {width: '35%'},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("column-chart2"));
      chart.draw(data, options);
  }
google.charts.setOnLoadCallback(drawColumnChart3);
  function drawColumnChart3() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Population', { role: 'style' }, { role: 'annotation' }],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$blank."', ".$people[$i].", '".$color[$i]."' , "."'".$people[$i]."'" ?>],
        <?php } 
        ?>
        ]);


      var options = {
        title: "Average Sale % of MMR",
        chartArea: {width: '35%'},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("column-chart3"));
      chart.draw(data, options);
  } 


  google.charts.setOnLoadCallback(drawColumnChart4);
      
    function drawColumnChart4() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Population', { role: 'style' }, { role: 'annotation' }],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$blank."', ".$people[$i].", '".$color[$i]."' , "."'".$people[$i]."'" ?>],
        <?php } 
        ?>
        ]);


      var options = {
        title: "Average Sales Price",
        chartArea: {width: '35%'},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("column-chart4"));
      chart.draw(data, options);
  }

google.charts.setOnLoadCallback(drawColumnChart5);
      
    function drawColumnChart5() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Population', { role: 'style' }, { role: 'annotation' }],
        <?php
        for($i=0;$i<$resultCount;$i++){
          ?>[<?php echo "'".$country[$i]."', ".$people[$i].", '".$color[$i]."' , "."'".$people[$i]."'" ?>],
        <?php } 
        ?>
        ]);


      var options = {
        title: "Average Vehicle Grade",
        chartArea: {width: '35%'},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("column-chart5"));
      chart.draw(data, options);
  }

  </script> 
  <style> 
    #chart_container{
        position: relatives;
        padding-bottom: 0px;
        height: 0 ;width: 100%
    } 
    .chart-div{
        margin-bottom: 10px;float: left;width: 270px;
    }
</style> 
@endsection
