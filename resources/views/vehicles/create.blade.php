@extends('layouts.master')
@section('content')
<style type="text/css">
  .hidden {
    display: none;
  }
</style>
@php
$id = '';
$company_name = '';
$description = '';
$status = '';
$status_arr = array('Inactive', 'Active', 'Deleted');
if( !empty($company) ){
$id = $company->id;
$company_name = $company->company_name;
$description = $company->description;
$status = $company->status;
}
@endphp
<div class="section_wrapper mcb-section-inner">
  <!---- Form Section ---->
  <section class="content">
    <div class="container-fluid">
      <div class="card card-default" style="box-shadow:none;">
        <div class="row">
          <div class="col-md-3">&nbsp;</div>
          <div class="col-md-5">
            <h2>{{ Request::segment(3) ? 'Update Registered Company' : 'Create New Company'}}</h2>
          </div>
          <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary"
              href="{{ route('companies.index') }}">
              Back</a>
          </div>
          <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">
          <div class="col-md-3"> &nbsp;</div>
          <div class="col-md-6">
            @include('layouts.error')
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">{{ Request::segment(3) ? 'Update Company' : 'Register Company'}}</h3>
              </div>
              <div class="card-body">
                {!! form($form) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!---- Form Section ---->
</div>
@endsection