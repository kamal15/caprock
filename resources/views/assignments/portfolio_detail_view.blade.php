@extends('layouts.master')
@section('content')
<!---- Form Section ---->
<style>
    .redStar {
        color: red;
        font-size: 18px;
        font-family: 'Caladea', ;
    }
</style>
<link href="https://fonts.googleapis.com/css?family=Caladea&display=swap" rel="stylesheet">
<section class="content">
    <div class="container-fluid">
        <div class="card card-default" style="box-shadow:none;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Vehicle Detail Page</h3>
                </div>
            </div>
            <?php //echo '<pre>';print_r($assignments_by_client_dt);exit;
$count = 0;
if($assignments_by_client_dt){
    $assignments_by_client_dt = $assignments_by_client_dt->toArray();
    // echo '<pre>';print_r($assignments_by_client_dt);exit;
    //echo $assignments_by_client_dt['contract_date'];exit;
    /*$date_assigned_to_repo_agent = only_date_format($assignments_by_client_dt['date_assigned_to_repo_agent'],1);
    $date_assigned_to_repo_agent = only_date_format($assignments_by_client_dt['date_assigned_to_repo_agent'],1);
    $date_assigned_to_repo_agent = only_date_format($assignments_by_client_dt['date_assigned_to_repo_agent'],1);*/
     $date_assigned_to_repo_agent = only_date_format(trim($assignments_by_client_dt['date_assigned_to_repo_agent']),1);
     $contract_date               = only_date_format($assignments_by_client_dt['contract_date'],1) ;
    $recovery_date               = only_date_format($assignments_by_client_dt['recovery_date'],1) ;
    $secured_date                = only_date_format($assignments_by_client_dt['secured_date'],1) ;
    $hold_date                   = only_date_format($assignments_by_client_dt['hold_date'],1);
    $scheduled_sale_date         = only_date_format($assignments_by_client_dt['scheduled_sale_date'],1);
    $sold_date                   = only_date_format($assignments_by_client_dt['sold_date'],1);
    $cure_expired                = only_date_format($assignments_by_client_dt['cure_expired'],1);
    $cure_sent                   = only_date_format($assignments_by_client_dt['cure_sent'],1);


}

 //echo '<pre>';print_r($assignments_by_client_dt['client_account_no']);exit;
                $count = count($assignments_by_client_dt);
                $make = '';$model = '';$year = '';$vin = '';$licencePlate = '';
                if($count>0){
                    $loan_no        = $assignments_by_client_dt['client_account_no'];
                    $make           = $assignments_by_client_dt['make'];
                    $model          = $assignments_by_client_dt['model'];
                    $year           = $assignments_by_client_dt['year'];
                    $vin            = $assignments_by_client_dt['vin'];
                    $license_plate  = $assignments_by_client_dt['license_plate'];
                    $NOI_mail_date  = only_date_format($assignments_by_client_dt['NOI_mail_date'],'');
                    $NOI_expire_date= only_date_format($assignments_by_client_dt['NOI_expire_date'],'');
                    $close_date     = only_date_format($assignments_by_client_dt['close_date'],'');


                ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">

                                <div class="col-md-2"><span class=" alert bg-caprock-col" style="color:#fff;">Make:
                                        {{$make}}</span></div>

                                <div class="col-md-2"><span class=" alert bg-caprock-col" style="color:#fff;">Model:
                                        {{$model}}</span></div>
                                <div class="col-md-2"><span class=" alert bg-caprock-col" style="color:#fff;">Year:
                                        {{$year}}</span></div>
                                <div class="col-md-3"><span class=" alert bg-caprock-col" style="color:#fff;">VIN:
                                        {{$vin}}</span></div>
                                <div class="col-md-3"><span class="alert bg-caprock-col" style="color:#fff;">License
                                        Plate: {{$license_plate}}</span></div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-group-last">
                @if ($errors->any())
                <div class="alert alert-danger " role="alert">
                    <div class="alert-icon"><i class="flaticon-warning k-font-brand"></i></div>
                    <div class="alert-text">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif

                @if (session()->has('message_notes'))
                <div class="alert alert-success">
                    <div class="alert-icon"><i class="ace-icon fa fa-check bigger-110"
                            style="font-size: 15px;"></i>&nbsp; {{ session('message_notes') }}</div>

                </div>
                @endif
            </div>

            <form action="{{ route('portfolio-detail.storeportfolio') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="assignment_id" value="{{$assignments_by_client_dt['id']}}">
                <input type="hidden" name="loan_no" value="{{$loan_no}}">
                <input type="hidden" name="vin_n" value="{{$vin}}">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <!-- <div class="col-md-2">
                                        Status <span class="redStar">*</span>
                                        <select class="form-control" name="statusOpt" id="statusOpt">
                                            <option value="">-Status-</option>
                                            <option value="Active">Active</option>
                                            <option value="InActive">In-Active</option>
                                        </select>
                                    </div> -->
                                    <!--  <div class="col-md-3">
                                        Date Of Original Document Received  <span class="redStar">*</span>:
                                        <input style="background: #fff;color: #797575;border-radius: 5px;" type="date" placeholder="Select Date" autocomplete="off" class="form-control" name="dateOrgDocRecvd" id="dateOrgDocRecvd">
                                    </div>
 -->
                                    <div class="col-md-2">
                                        Type:
                                        <select class="form-control" name="note_type" id="note_type">
                                            <!-- <option value="">Select type</option> -->
                                            <option value="Update Request">Update Request</option>
                                            <option value="Close Request">Close Request</option>
                                        </select></div>
                                    <div class="col-md-4">
                                        Notes <span class="redStar">*</span>:<textarea rows="3" name="note_text"
                                            id="note_text" placeholder="Add Notes" class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-1">
                                        <br><input type="submit" class="bg-caprock-col" name="submit" value="Submit"
                                            id="submit"></div>
                                    <div class="col-md-5">
                                        Notes:
                                        <div
                                            style="padding: .375rem .75rem;font-size: 1rem;line-height: 1.5;color: #495057;background-color: #fff;background-clip: padding-box;border: 1px solid #ced4da;border-radius: .25rem;transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out; height: 90px; overflow-y: auto;">
                                            <ul style="padding-left: 20px; list-style-type: disc;">
                                                @foreach ($notes as $note)
                                                <li style="display: list-item;text-align: -webkit-match-parent;">
                                                    {{$note->noteText}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!--------------- File upload start ---------- -->

            <div class="form-group form-group-last">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    <div class="alert-icon"><i class="ace-icon fa fa-check bigger-110"
                            style="font-size: 15px;"></i>&nbsp; {{ session('message') }}</div>
                </div>
                @endif
                @if (session()->has('message_er'))
                <div class="alert alert-danger">
                    <div class="alert-icon"><i class="ace-icon fa fa-check bigger-110"
                            style="font-size: 15px;"></i>&nbsp; {{ session('message_er') }}</div>
                </div>
                @endif
            </div>
            <form action="{{ route('portfolio-detail.uploadDocument') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label class="col-2 col-form-label"></label>
                    <label class="col-2 col-form-label">Upload Document</label>
                    <div class="col-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" multiple=""
                                    name="files_input[]" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group">
                            <div class="input-group-prepend"> </div>
                            <button type="submit" id="submit" name="submit" class="btn btn-primary btn-wide"> Save
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <!--------------- File upload End ---------- -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class=" ">
                            <div class="form-group row" style="margin-top: 10px;">
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                    <div class="alert bg-caprock-col" style="cursor:pointer;color:white;"
                                        onclick="return openChild('vehinfo');">
                                        Vehicle Information
                                    </div>
                                    <div id="vehinfo" style="display: none;">
                                        <div class="form-group row">
                                            <!-------------------- Segment start -------------- -->
                                            <!-- =========================Accordian ====================-->
                                            <div class="col-md-3" id="vehicle_dd1">
                                                <div class="alert bg-caprock-col">Assignment type
                                                </div>
                                                <?php ## ----------For each loop for first Accordian start ---------##?>
                                                @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                                1,'name'=>'One','title'=>'Assignment
                                                type','count'=>0,'no_of_inputs'=>3,'city_state_zip'=>''])
                                                <?php  ## ---------For each loop for first Accordian start -------------##?>
                                            </div>
                                            <div class="col-md-2" id="vehicle_dd1">
                                                <div class="alert bg-caprock-col">Vehicle
                                                </div>
                                                <?php ## ----------For each loop for first Accordian start ---------##?>
                                                @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                                2,'no_of_inputs'=>5,'name'=>'One','title'=>'Vehicle','count'=>0,'city_state_zip'=>'',
                                                'fld_arr' =>
                                                ['make'=>$make,'model'=>$model,'year'=>$year,'vin'=>$vin,'Licence
                                                plate'=>$license_plate],'add'=>[]])
                                                <?php  ## ---------For each loop for first Accordian start -------------##?>
                                            </div>
                                            <div class="col-md-2" id="vehicle_dd1">
                                                <div class="alert bg-caprock-col">Account
                                                </div>
                                                <?php ## ----------For each loop for first Accordian start ---------##?>
                                                @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                                3,'no_of_inputs'=>6,'name'=>'One','title'=>'Account','count'=>0,'city_state_zip'=>'',
                                                'fld_arr' => ['Client
                                                Name'=>$assignments_by_client_dt['client_name'],'Client
                                                Account#'=>$assignments_by_client_dt['client_account_no'],
                                                'client Queue#' => $assignments_by_client_dt['client_queue_no'],'Date
                                                Assigned'=>$date_assigned_to_repo_agent,'Contract Date'=>$contract_date,
                                                'Original Loan Amt'=> $assignments_by_client_dt['original_loan_amount']
                                                ],'add'=>[]])
                                                <?php  ## ---------For each loop for first Accordian start -------------##?>
                                            </div>

                                            <div class="col-md-2" id="vehicle_dd1">
                                                <div class="alert bg-caprock-col">Customer
                                                </div>
                                                <?php ## ----------For each loop for first Accordian start ---------##?>
                                                @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                                1,'no_of_inputs'=>5,'name'=>'One','title'=>'Assigned to
                                                Rep','count'=>0,'city_state_zip'=>'yes','fld_arr' => ['First
                                                name'=>$assignments_by_client_dt['first_name'],'last
                                                name'=>$assignments_by_client_dt['last_name'],'ssn'=>$assignments_by_client_dt['ssn'],'customer
                                                address'=>$assignments_by_client_dt['customer_address'], 'Apt #'=>
                                                $assignments_by_client_dt['customer_apt_no']],'add'=>['city'=>$assignments_by_client_dt['customer_city'],'state'=>$assignments_by_client_dt['customer_state'],'zip'=>$assignments_by_client_dt['customer_zip']
                                                ]])
                                                <?php  ## ---------For each loop for first Accordian start -------------##?>
                                            </div>
                                            <div class="col-md-3" id="vehicle_dd1">
                                                <div class="alert bg-caprock-col">Place Of Employment
                                                </div>
                                                <?php ## ----------For each loop for first Accordian start ---------##?>
                                                @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                                1,'no_of_inputs'=>3,'name'=>'One','title'=>'Place of
                                                imployment','count'=>0,'city_state_zip'=>'yes','fld_arr' => ['Company
                                                name'=>$assignments_by_client_dt['cust_poe_company_name'],'Address'=>$assignments_by_client_dt['cust_poe_company_address'],'Suit#'=>$assignments_by_client_dt['cust_poe_suite_no']],'add'=>['city'=>$assignments_by_client_dt['cust_poe_company_city'],'state'=>$assignments_by_client_dt['cust_poe_company_state'],'zip'=>$assignments_by_client_dt['cust_poe_company_zip']
                                                ]])
                                                <?php  ## ---------For each loop for first Accordian start -------------##?>
                                            </div>

                                            <!-- =====================Accordian =======================-->
                                        </div>

                                        <!-------------------- Segment start -------------- -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-1">&nbsp;</div>
                            <div class="col-md-10">
                                <div class="alert bg-caprock-col" style="cursor:pointer;color:white;"
                                    onclick="return openChild('vehinfo2');">
                                    Recovery Information
                                </div>
                                <div id="vehinfo2" style="display: none;">
                                    <div class="form-group row">
                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!--  <div class="alert bg-caprock-col">Repo Status
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>4,'name'=>'One','title'=>'Repo Status',
                                            'city_state_zip'=>'','fld_arr' => ['Repo
                                            Status'=>$assignments_by_client_dt['repo_status'],'Repo
                                            Type'=>$assignments_by_client_dt['repo_type'],'Cure Sent'=>$cure_sent,'cure
                                            expired'=>$cure_expired],'add'=>[]])
                                        </div>
                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!--  <div class="alert bg-caprock-col">Date ass. to repo status
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>4,'name'=>'One','title'=>'Date ass. to repo status',
                                            'city_state_zip'=>'','fld_arr' => ['Date assigned to Repo
                                            Agent'=>$date_assigned_to_repo_agent,'Recovery Date'=>$recovery_date,'NOI
                                            mail date'=>$NOI_mail_date,'NOI expired date'=>$NOI_expire_date],'add'=>[]])
                                        </div>
                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!--   <div class="alert bg-caprock-col">Repo Hold Reason
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>3,'name'=>'One','title'=>'Hold
                                            Reason','count'=>0,'city_state_zip'=>'','fld_arr' => ['Repo Hold
                                            Reason'=>$assignments_by_client_dt['repo_hold_reason'],'Close
                                            reason'=>$assignments_by_client_dt['close_reason'],'Close
                                            date'=>$close_date],'add'=>[]])
                                        </div>
                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!--  <div class="alert bg-caprock-col">Storage Address
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>3,'name'=>'One','title'=>'Storage
                                            Address','count'=>0,'city_state_zip'=>'','fld_arr' => ['Storage
                                            Address'=>$assignments_by_client_dt['storage_address'],'Storage apt
                                            no'=>$assignments_by_client_dt['storage_apt_no'],'Keys'=>$assignments_by_client_dt['keys'],'Drivable'=>$assignments_by_client_dt['vehicle_driveable']],'add'=>['city'=>$assignments_by_client_dt['cust_poe_company_city'],'state'=>$assignments_by_client_dt['cust_poe_company_state'],'zip'=>$assignments_by_client_dt['cust_poe_company_zip']
                                            ]])
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-1">&nbsp;</div>
                            <div class="col-md-10">
                                <div class="alert bg-caprock-col" style="cursor:pointer;color:white;"
                                    onclick="return openChild('vehinfo3');">
                                    Remarketing Information
                                </div>
                                <div id="vehinfo3" style="display: none;">
                                    <div class="form-group row">
                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!-- <div class="alert bg-caprock-col">Aucton status
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>4,'name'=>'One','title'=>'Auction
                                            status','count'=>0,'city_state_zip'=>'','fld_arr' => ['Auction
                                            status'=>$assignments_by_client_dt['auction_status'],'Auction'=>$assignments_by_client_dt['auction'],'Auction
                                            address'=>$assignments_by_client_dt['auction_address'],'Auction title
                                            status'=>$assignments_by_client_dt['auction_title_status']],'add'=>[]])
                                        </div>

                                        <div class="col-md-3" id="vehicle_dd2">
                                            <!--   <div class="alert bg-caprock-col">Secured date
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>4,'name'=>'One','title'=>'Secured
                                            date','count'=>0,'city_state_zip'=>'','fld_arr' => ['Secured
                                            date'=>$secured_date,'Vehicle Condition
                                            Report'=>$assignments_by_client_dt['condition_report_link'],'Mileage'=>$assignments_by_client_dt['Mileage'],'grade'=>$assignments_by_client_dt['Grade']],'add'=>[]])
                                        </div>
                                        <div class="col-md-2" id="vehicle_dd2">
                                            <!-- <div class="alert bg-caprock-col">MMR
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>3,'name'=>'One','title'=>'MMR','count'=>0,'city_state_zip'=>'','fld_arr'
                                            => ['MMR'=>$assignments_by_client_dt['MMR'],'Floor
                                            Price'=>$assignments_by_client_dt['floor_price'],'Sales
                                            price'=>$assignments_by_client_dt['sales_price']],'add'=>[]])
                                        </div>
                                        <div class="col-md-2" id="vehicle_dd2">
                                            <!--   <div class="alert bg-caprock-col">Days run
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>3,'name'=>'One','title'=>'Days
                                            run','count'=>0,'city_state_zip'=>'','fld_arr' => ['Days
                                            run'=>$assignments_by_client_dt['days_run'],'Hold date'=>$hold_date,'Sale
                                            hold reason'=>$assignments_by_client_dt['sale_hold_reason']],'add'=>[]])
                                        </div>
                                        <div class="col-md-2" id="vehicle_dd2">
                                            <!--  <div class="alert bg-caprock-col">Scheduled sale date
                                                </div> -->
                                            @include('templates.accordian_dt_tpl', ['parent_acc'=>1,'key' =>
                                            1,'no_of_inputs'=>3,'name'=>'One','title'=>'Scheduled Sale
                                            Date','count'=>0,'city_state_zip'=>'','fld_arr' => ['Scheduled
                                            Sale Date'=>$scheduled_sale_date,'Total
                                            charges'=>$assignments_by_client_dt['total_charges'],'Sold
                                            Date'=>$sold_date],'add'=>[]])
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php //===================== Next Part =====================================//?>
        <?php }else{
                echo 'No data';
                }?>

    </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script>
        function openChild(id) {
            $("#" + id).slideToggle('slow');
        }
    </script>


</section>
@endsection