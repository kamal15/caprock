@extends('layouts.master')
@section('content')
<?php
  $session_data = get_session_data();
  $client_name  =  get_company_name(@$session_data['fk_company_id']);
  $user_name    =  '';
  //dd($client_name);
?>
<!---- Form Section ---->
<section class="content">
    <div class="container-fluid">
        <div class="card card-default" style="box-shadow:none;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Assignment Screen</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">&nbsp;</div>
                <div class="col-md-6">
                    @include('layouts.error')
                </div>
            </div>
            <form action="{{ route('assignments.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-2 p-1">

                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">Assignment Type</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 16px;">
                            <div class="col-sm-12">
                                <label class="k-checkbox">
                                    <input type="checkbox" class="asgn_type" onclick="return selectOne('recovery');"
                                        name="assignment_type" value="recovery" id="recovery" checked="checked">
                                    Recovery
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="k-checkbox">
                                    <input type="checkbox" class="asgn_type" onclick="return selectOne('remarketing');"
                                        name="assignment_type" value="remarketing" id="remarketing">
                                    Remarketing
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="k-checkbox">
                                    <input type="checkbox" onclick="return selectOne('both');" class="asgn_type"
                                        name="assignment_type" value="both" id="both">
                                    Both
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <select name="repo_type" id="repo_type" class="form-control">
                                    <option value="">--Select Repo Type--</option>
                                    <option value="Voluntary" {{old('repo_type') == 'Voluntary' ? 'selected' : ''}}>
                                        Voluntary</option>
                                    <option value="Involuntary" {{old('repo_type') == 'Involuntary' ? 'selected' : ''}}>
                                        Involuntary</option>
                                    <option value="Impound" {{old('repo_type') == 'Impound' ? 'selected' : ''}}>Impound
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">Vehicle</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="make" id="make"
                                    class="form-control" placeholder="Make" value="{{ old('make') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="model" id="model"
                                    class="form-control" placeholder="Model" value="{{ old('model') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="4" name="year" id="year"
                                    class="form-control" placeholder="year" value="{{ old('year') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="vin" id="vin"
                                    class="form-control" placeholder="VIN" value="{{ old('vin') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="license_plate"
                                    id="license_plate" class="form-control" placeholder="License Plate"
                                    value="{{ old('license_plate') }}">
                            </div>

                        </div>

                    </div>
                    <div class="col-md-2 p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">If
                                    Already Recovered</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="vehicle_address"
                                    id="vehicle_address" placeholder="Vehicle Address" class="form-control"
                                    value="{{ old('vehicle_address') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="vehicle_apt_no"
                                    id="vehicle_apt_no" placeholder="Apt #" class="form-control"
                                    value="{{ old('vehicle_apt_no') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row p-0">
                                    <div class="col-md-4 pr-0">
                                        <input type="text" autocomplete="off" maxlength="255" name="vehicle_city"
                                            id="vehicle_city" placeholder="City" class="form-control"
                                            value="{{ old('vehicle_city') }}">
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <select class="form-control" name="vehicle_state" id="vehicle_state">
                                            @foreach ($states as $state)
                                            <option {{old('vehicle_state') == $state ? 'selected' : ''}}
                                                value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                        </select>
                                        {{-- <input type="text" autocomplete="off" maxlength="255" name="vehicle_state"
                                            id="vehicle_state" placeholder="State" class="form-control"
                                            value="{{ old('vehicle_state') }}"> --}}
                                    </div>
                                    <div class="col-md-4 pl-0">
                                        <input type="text" autocomplete="off" maxlength="255" name="vehicle_zip"
                                            id="vehicle_zip" placeholder="Zip" class="form-control"
                                            value="{{ old('vehicle_zip') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="legal_sale_date"
                                    id="legal_sale_date" placeholder="Legal Sale Date" class="form-control"
                                    value="{{ old('legal_sale_date') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="k-checkbox">
                                    <input type="checkbox" name="keys" value="1" id="keys">
                                    Keys
                                    <span></span>
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-2 p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">Account</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="client_name"
                                    id="client_name" class="form-control" placeholder="Client Name"
                                    value="{{ old('client_name', $client_name) }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="client_account_no"
                                    id="client_account_no" class="form-control" placeholder="Customer Account#"
                                    value="{{ old('client_account_no') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="contract_date"
                                    id="contract_date" class="form-control" placeholder="Contract Date"
                                    value="{{ old('contract_date') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="original_loan_amount"
                                    id="original_loan_amount" class="form-control" placeholder="Original Loan Amount"
                                    value="{{ old('original_loan_amount') }}"
                                    onchange="return format_original_loan_amount(this.id, this.value);">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="client_queue_no"
                                    id="client_queue_no" class="form-control" placeholder="Client Queue #"
                                    value="{{ old('client_queue_no') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">Customer</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="first_name" id="first_name"
                                    class="form-control" placeholder="First Name" value="{{ old('first_name') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="last_name" id="last_name"
                                    class="form-control" placeholder="Last Name" value="{{ old('last_name') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <input type="password" autocomplete="off" name="ssn" id="ssn" class="form-control"
                                    placeholder="SSN" value="{{ old('ssn') }}" onkeyup="return encrypt_ssn();"
                                    maxlength="9" onchange="return format_ssn(this.id, this.value); ">
                            </div>
                            <div class="col-sm-5" id="ssn_divDisplay"
                                style="padding-top:10px; padding-left:0px;font-size:13px; color:grey;display:none;">
                                XXX-XX-<span id="ssn_div"></span></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="customer_address"
                                    id="customer_address" class="form-control" placeholder="Address"
                                    value="{{ old('customer_address') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="customer_apt_no"
                                    id="customer_apt_no" class="form-control" placeholder="Apt #"
                                    value="{{ old('customer_apt_no') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row p-0">
                                    <div class="col-md-4 pr-0">
                                        <input type="text" autocomplete="off" maxlength="255" name="customer_city"
                                            id="customer_city" placeholder="City" class="form-control"
                                            value="{{ old('customer_city') }}">
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <select class="form-control" name="customer_state" id="customer_state">
                                            @foreach ($states as $state)
                                            <option {{old('customer_state') == $state ? 'selected' : ''}}
                                                value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                        </select>
                                        {{-- <input type="text" autocomplete="off" maxlength="255" name="company_state"
                                            id="customer_state" placeholder="State" class="form-control"
                                            value="{{ old('customer_state') }}"> --}}
                                    </div>
                                    <div class="col-md-4 pl-0">
                                        <input type="text" autocomplete="off" maxlength="255" name="customer_zip"
                                            id="customer_zip" placeholder="Zip" class="form-control"
                                            value="{{ old('customer_zip') }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-2 p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" style="width: 100% !important;" class="btn btn-danger">Place
                                    of Employment</a>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="cust_poe_company_name"
                                    id="cust_poe_company_name" class="form-control" placeholder="Company Name"
                                    value="{{ old('cust_poe_company_name') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="cust_poe_company_address"
                                    id="cust_poe_company_address" class="form-control" placeholder="Address"
                                    value="{{ old('cust_poe_company_address') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" autocomplete="off" maxlength="255" name="cust_poe_suite_no"
                                    id="cust_poe_suite_no" class="form-control" placeholder="Suite #"
                                    value="{{ old('cust_poe_suite_no') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row p-0">
                                    <div class="col-md-4 pr-0">
                                        <input type="text" autocomplete="off" maxlength="255"
                                            name="cust_poe_company_city" id="cust_poe_company_city" placeholder="City"
                                            class="form-control" value="{{ old('cust_poe_company_city') }}">
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <select class="form-control" name="cust_poe_company_state"
                                            id="cust_poe_company_state">
                                            @foreach ($states as $state)
                                            <option {{old('cust_poe_company_state') == $state ? 'selected' : ''}}
                                                value="{{$state}}">{{$state}}</option>
                                            @endforeach
                                        </select>
                                        {{-- <input type="text" autocomplete="off" maxlength="255"
                                            name="cust_poe_company_state" id="cust_poe_company_state"
                                            placeholder="State" class="form-control"
                                            value="{{ old('cust_poe_company_state') }}"> --}}
                                    </div>
                                    <div class="col-md-4 pl-0">
                                        <input type="text" autocomplete="off" maxlength="255"
                                            name="cust_poe_company_zip" id="cust_poe_company_zip" placeholder="Zip"
                                            class="form-control" value="{{ old('cust_poe_company_zip') }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="javascript::void(0);" style="width: 100%;" class="btn btn-danger ">Upload
                                    Documents</a>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="custom-file">
                                    <input type="file" name="documents[]" class="custom-file-input" id="documents">
                                    <label class="custom-file-label" for="files_input">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <textarea rows="2" name="assignment_notes" id="assignment_notes" class="form-control"
                                    placeholder="Notes-Text Only">{{old('assignment_notes')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" style="height: 60px;" name="submit" id="submit"
                                    class="form-control btn btn-danger btn-lg" value="Submit">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </form>
            <br><br>
            <!--   <div class="row">
        <div class="col-md-2">
          <div class="row">
            <div class="col-md-12">
              <input type="text" autocomplete="off" class="form-control" name="created_by" id="created_by"
                placeholder="Created By" value="{{old('created_by')}}">
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="row">
            <div class="col-md-12">
              <input type="text" autocomplete="off" class="form-control" name="last_modified_by" id="last_modified_by"
                placeholder="Last Modified By" value="{{ old('last_modified_by') }}">
            </div>
          </div>
        </div>
      </div> -->
        </div>
    </div>
</section>
<!---- Form Section ---->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    function selectOne(val){
     $('.asgn_type').prop('checked',false);
     $('#'+val).prop('checked',true);
  }
  var assType = "{{old('assignment_type')}}";
  if(assType){
      selectOne(assType);
  }
  function format_ssn(id, ssn) {
    var cleaned = ('' + ssn).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{2})(\d{4})$/)
    if (match){
      var ssn = match[1] +'-'+ match[2]+ '-' +match[3];
      return $('#'+id).val(ssn);
    }
    return null;
  }

  function encrypt_ssn(){
    var str = $("#ssn").val();
    if(str.length>4){
      $("#ssn_divDisplay").show();
      $("#ssn_div").html(str.substring(5, 10));
    }
    if(str.length<4){
      $("#ssn_divDisplay").hide();
    }
    //alert(str);
  }
  $( function() {
    $("#legal_sale_date").datepicker({
      format: "yy-mm-dd",
      todayHighlight: true,
      autoclose: true
    });
    $("#contract_date" ).datepicker({
      format: "yy-mm-dd",
      todayHighlight: true,
      autoclose: true
    });
  });



  function format_original_loan_amount(id, amount) {
    Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    };
    var currency = parseFloat(amount).format();
    return $('#'+id).val('$'+currency);
  }


  $('#documents').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      fileName =fileName.replace("C:\\fakepath\\", "");
      //replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
  })
</script>


@endsection