<?php
 
$dataPoints = array(
    array("x"=> 29, "y"=> 41),
    array("x"=> 21, "y"=> 9, "indexLabel"=> "Lowest"),
    array("x"=> 14, "y"=> 44),
    array("x"=> 15, "y"=> 45),
    array("x"=> 9, "y"=> 41),
    array("x"=> 12, "y"=> 22),
    array("x"=> 11, "y"=> 38),
    array("x"=> 50, "y"=> 50, "indexLabel"=> "Highest"),
   
);
    
?>
<!DOCTYPE HTML>
<html>
<head>  
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    exportEnabled: true,
    theme: "dark2", // "light1", "light2", "dark1", "dark2"
    title:{
        text: "Simple Column Chart with Index Labels"
    },
    data: [{
        type: "column", //change type to bar, line, area, pie, etc
        //indexLabel: "{y}", //Shows y value on all Data Points
        indexLabelFontColor: "#5A5757",
        indexLabelPlacement: "outside",   
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
});
chart.render();
 
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 150px; width: 20%;"></div>
<div id="chartContainer2" style="height: 150px; width: 20%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>