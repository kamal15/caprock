@extends('layouts.master')
@section('content')
<style>
  #document_upload_list_div {
    height: 250px;
  }

  #image-upload {
    border: 2px dashed #1dc9b7;
    border-radius: 10px;
  }

  .btn {
    background: #fff !important;
    color: #000;
  }

  .swal-button {
    background-color: #560b07 !important;
  }
</style>
<?php $session_data = get_session_data();?>
<div class="section_wrapper mcb-section-inner">
  <!-- ============================Section start =============================== --->
  <div class="card card-default" style="box-shadow:none;">

    <div class="row">
      <div class="col-lg-1">&nbsp;</div>
      <div class="col-lg-10">
        <section class="content">
          <div class="container-fluid" style="padding-left: 0px;">
            <div class="card card-danger panel-default">
              <div class="card-header bg-caprock-col">
                <h3 class="card-title">Upload Document</h3>
              </div>
              <div class="card-body">
                <div class="form-group row">
                  <div class="col-lg-12 col-md-9 col-sm-12">
                    @include('layouts.error')
                  </div>
                </div>

                
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="company">Company</label>
                    <div class="col-md-6">
                      <select name="company" id="company" class="form-control"
                        onchange="return list_comp_docs(this.value);">
                        @if(@$session_data['role_name']=='Caprock Administrator') 
                        <option value="">--Select Company--</option>
                        @endif
                        @foreach ($companies as $company)
                        <option @if($session_data['fk_company_id']== $company['id'] && @$session_data['role_name']!='Caprock Administrator') selected  readonly="readonly" @endif{{ $company['id'] == request()->segment(2) ? 'selected' : '' }} value={{$company['id']}}>{{$company['company_name']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="form-group row">
                  <div class="col-md-12 " id="dropzone_div">
                    <!-------------- Upload Doc Start --------------------- -->

                    {!! Form::open([ 'route' => [ 'upload-document-file' ], 'files' => true, 'enctype' =>
                    'multipart/form-data', 'class' =>
                    'dropzone', 'id' => 'image-upload' ]) !!}
                    <div class="dz-message" data-dz-message><span>
                        <h4>Upload Multiple Files By Drag & Drop or Clicking On
                          the Box</h4>
                        <h6>(Only jpeg, jpg, png, pdf, txt, xlsx, xls, doc files are allowed)</h6>
                      </span></div>
                    <input type="hidden" id="company_id" name="company_id" value="{{@Request()->segment(2)}}">
                    {!! Form::close() !!}
                    <!-- -----------------Upload Doc End ----------------- --->
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12 ">
                    <!--begin::Portlet-->
                    <div id="main_listing_div" class="k-portlet__body k-portlet__body--fit k-portlet__body--fluid"
                      style="display:none;height:auto; overflow: auto;border: 1px solid #f1eded;border-radius: 10px;">
                      <div class="k-widget-7" style="padding: 7px;margin: 12px;">
                        <div class="k-widget-7__items" id="document_upload_list_div">
                          <button
                            class="spinner_cls btn btn-accent btn-icon btn-circle k-spinner k-spinner--v2 k-spinner--center k-spinner--sm k-spinner--danger"></button>
                        </div>
                      </div>
                    </div>
                    <!--end::Portlet-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

    </div>
  </div>
</div>

<!-- =================================Section start =============================== --->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  var company_id = 0;
  function list_comp_docs(compId){  
  $("#company_id").val(compId);
  company_id = compId;
  load_uploaded_doc_list('document_upload_list_div',company_id);
  }
  function get_selected_company_id(){
      var id =  $( "#company option:selected" ).val();
      if($.isNumeric(id)){
        return id;
      } else {
        return NaN;
      }
  }

 
  
  
  
  Dropzone.options.imageUpload = {
  maxFilesize : 1,
  acceptedFiles: ".jpeg,.jpg, .JPG, .png,.pdf,.txt,.xlsx,.xls,.doc",
  error: function (file, response){
    swal({
    title: "Oops...,",
    text: "Some error occured during upload!",
    icon: "error",
    });
  },
  success: function (file, response) {
    if(response.err_msg){
      swal({
      title: "Oops...,",
      text: "You forgot to select a company!",
      icon: "error",
      });
    }
    if(response.suc_msg){
    swal({
    title: "Your file is uploaded successfully!",
    text: "",
    icon: "success",
    });
    }
  $('.dz-error-mark').hide();
  $('.dz-success-mark').hide();
  this.removeFile(file);
  if(!response.err_msg){
  load_uploaded_doc_list('document_upload_list_div', response.company_id);
  }
  }
  };
   
  function deleting_file(row, userID){
    var companyID = $( "#company option:selected" ).val();
    var r = confirm("Are you sure want to delete file?");
    if (r == true) { 
      window.location.href="{{url('deleting-file')}}/"+row+'/'+userID+'/'+companyID;
      return true
    }return false;
  
  }
  function load_uploaded_doc_list(display_div_id,company_id='all'){
    $("#image-upload").hide(); 
  $.ajax({
  url:"{{ url('uploaded-files-listing') }}",
  type:"POST",
  data:{'_token':'{{ csrf_token() }}', 'uploaded_by': '{{ auth()->id()}}', 'company_id': company_id },
  success:function (res){
  if(res==0 || res==''){
  $("#main_listing_div").hide();
  }else{
  $("#main_listing_div").show();
  $("#"+display_div_id).html(res);

  
  }$("#image-upload").show();
  }
  })
  }
  //load_uploaded_doc_list('document_upload_list_div');

  if('{{request()->segment(2)}}'!=''){
load_uploaded_doc_list('document_upload_list_div', '{{request()->segment(2)}}');
  }
  

  $("#image-upload").hide();
</script>
@if(@$session_data['role_name']!='Caprock Administrator') 
<script> 
  list_comp_docs('<?php echo $session_data['fk_company_id'];?>');
</script>
@endif
<style>
.btn {
background: #8f130c !important;
color: white !important;
} 
</style>

@endsection