@extends('layouts/master')
@section('content')

<div class="section_wrapper mcb-section-inner"> 
  <div class="form-group row">
    <div class="col-lg-12 margin-tb alert alert-danger bg-caprock-col">      
      <div class="col-md-4 pull-left">
        <h3>User Management</h3>
      </div>
      <div class="col-md-5 pull-left">
        <form action="{{ route('users.index') }}" method="get" id="user_frm_srch">
        
          @csrf
          <div class="form-group row">
            <div class="input-group col-md-8">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></i></span>
                </div>
               <input type="text" placeholder="Search Name or Company" value="<?php echo @$_REQUEST['srchFld']?>" class="form-control" name="srchFld" id="srchFld"> 
            </div> 
             
          </div>
        </form>
      </div>
      <div class="col-md-2 pull-left">
        @if(session('impersonated_by') )
        <a class="link_btn pull-left" style="text-decoration:none;" href="<?php echo url('impersonate_leave') ;?>">Back to my account</a>
        @else
        <!-- <li> <a class="btn btn-info" href="{{ route('logout') }}">Logout</a></li>  --> 
        @endif</div>

      <div class="col-md-1 pull-right">
        @can('user-create')
        <div class=" link_btn"> 
        <a href="{{ route('users.create') }}"> Create User</a>
        </div>
        @endcan
      </div>
    </div>
  </div>
<?php 
$session_data = get_session_data();

//echo '<pre>';print_r($session_data['role_name']);exit;
?>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    {{ $message }} 
  </div>
  @endif

  <table class="table table-bordered text-left">
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Associated Company</th>
      <th>Role</th> 
      <th width="20%">Login as </th>
      <th width="20%">Action</th>
    </tr>
    @if(count($data)>0)
      @foreach ($data as $key => $user)
      <?php  
      $userRoleName =  App\User::find($user->id)->getRoleNames() ; 

       //$user = json_decode(json_encode($user),true);
      // echo '<pre>';print_r($user);exit;
       $company_name = '';
      if(@$userRoleName[0]!='Caprock Administrator'){ 
        $fk_company_id =$user->fk_company_id; 
        $compaData = company_listing($fk_company_id); 
      
        if($compaData){
          @$compaDataArr = $compaData->toArray(); 
          @$company_name  = array_values( @$compaDataArr )[0];  
        }
      }
     
      ?>
      @if($user->id!=$session_data['user_id'])
      <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $company_name }}</td>
        <td>
          <?php  $userRoleName2 =  App\User::find($user->id)->getRoleNames() ; ?>
          @if(@$userRoleName[0]!='')
          @foreach($userRoleName2 as $v)
          <span class="client_name">{{ $v }}</span> 
          @endforeach
          @endif
        </td>
        <td> 
          @if(!empty($user->id))
          <a class="btn btn-sm btn-success" href="<?php echo url('impersonate/'.$user->id) ;?>" data-toggle='tooltip'
            data-placement='top' title='Impersonate'>login as {{ $v }}</a>
          @endif
         
        </td>
        <td>
          <div class="row">
            <div class="col-md-3">
              @can('user-show')
              <a class="btn btn-sm btn-info pull-left" href="{{ route('users.show',$user->id) }}"><i class="fas fa-eye"></i></a>
              @endcan
            </div>
            <div class="col-md-3">
              @can('user-edit')
              <a class="btn btn-sm btn-primary" href="{{ route('users.edit',$user->id) }}"><i class="fas fa-edit"></i></a>
              @endcan
            </div>
            <div class="col-md-3">
              @can('user-delete')
            <form action="{{ route('users.destroy', $user->id) }}" method="post" id="frm_{{$user->id}}">
                @csrf
                @method('delete')
                <button class="btn btn-sm btn-primary"  onclick="return deleteUser('<?php echo $user->id ?>');" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
              </form>
              {{-- {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
              {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger pull-right']) !!}
              {!! Form::close() !!} --}}
              @endcan
            </div>
          </div>
        </td>
      </tr>
      @endif
      @endforeach
    @else
    <tr>
        <td colspan="8" style="color:red;text-align:center !important;">No Data Found </td>
         
  </tr>
    @endif
  </table>
<script>
function deleteUser(id){ 
    var r = confirm("Are you sure to delete company?");
    if (r == true) {
      $("#frm_"+id).submit(); 
    } else {
      return false;
    }
  }
  </script>
 {{ $data->appends(Request::except('page'))->links() }}
</div>
@endsection