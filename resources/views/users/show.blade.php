@extends('layouts/master')
@section('content')
<div class="section_wrapper mcb-section-inner">
 
 

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<?php //echo '<pre>';print_r($roles);exit;?>

<div class="row">
      <div class="col-md-2"> &nbsp;</div>
      <div class="col-md-8">
        <div class="card card-danger">
           


            <div class="form-group row">
                <div class="col-lg-12 margin-tb alert alert-danger bg-caprock-col">
                  
                  <div class="col-md-4 pull-left">
                    <h3>Show User Detail</h3>
                  </div>
                  <div class="col-md-2 pull-left ">
                    @if(session('impersonated_by') )
                    <a class=" pull-left link_btn" style="text-decoration:none;" href="<?php echo url('impersonate_leave') ;?>">Back to my  account</a>  
                   @endif
                    </div>
                   &nbsp;
                  <div class="col-md-3 pull-left link_btn" style="text-align:center;"> 
                    @can('user-create')
                    <a class=" " style="text-decoration:none;" href="{{ route('users.create') }}">Create New User</a>
                    @endcan
                  </div>
                  &nbsp; <div class="col-md-1 pull-right link_btn" style="text-align:center;"> 
                    @can('user-create')
                    <a class=" " style="text-decoration:none;" href="{{ route('users.index') }}">Back</a>
                    @endcan
                  </div>
                 
                         
                </div>
              </div>


              
           
          <div class="card-body">
            
              <label>Name:</label> {{ $user->name }}</div>             
          

          <div class="card-body">
           
              <label>Email:</label> {{ $user->email }}         
          </div>

           


        </div>
      </div>
    </div>

</div></div>
   <style>.card-body{padding:0.5rem;}</style>
    @endsection