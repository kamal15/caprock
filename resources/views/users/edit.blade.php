@extends('layouts.master')
@section('content')
<!---- Form Section ---->
<section class="content">
  <div class="container-fluid">

    <div class="card card-default" style="box-shadow:none;">

     <?php $session_data = get_session_data();
      $userRole = Request()->segment(2); 
      
     $userRole =  $user->getRoleNames()->first();

     
      // / $userRole = $session_data['role_name'];
      //echo '<pre>';print_r($session_data );exit;?>
     


      <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
        </div>
      </div>
      {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
      <div class="row">
        <div class="col-md-3"> &nbsp;</div>
        <div class="col-md-6">
          <div class="card card-danger">
            <div class="card-header row bg-caprock-col">
              <div class="col-md-6"><h3 class="card-title">Update User</h3></div>
              <div class="col-md-5"></div>
              <div class="col-md-1 "><a class="link_btn" href="{{ route('users.index') }}">
            Back</a></div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Name:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</i></span>
                  </div>
                  {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}

                </div>
              </div>
              <div class="form-group">
                <label>Email:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">#</span>
                  </div>
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
              </div>
               
              <div class="form-group">
                <label>Confirm Password:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">##</span>
                  </div>
                  {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' =>
                  'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Role:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-laptop"></i></span>
                  </div>

                  <?php //echo '<pre>';print_r($roles);?>
                  {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control')) !!}
                </div>
              </div>
              @if($userRole!='Caprock Administrator')
              <div class="form-group">
                <label>Company:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-laptop"></i></span>
                  </div>
                  {!! Form::select('fk_company_id', company_listing(), null, ['class' => 'form-control'])
                  !!}
                </div>
              </div>
              @endif
             
              <div class="">

                <button type="submit" class="btn btn-info">Submit</button> 
                <a href=""  class="btn btn-default float-right" style="color:#fff">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
</section>
<!---- Form Section ---->

<script>
  function change_company_opt(val){ 
    $("#company_div").show();
   if(val=='Caprock Administrator'){
      $("#company_div").hide();
   }

  }
  change_company_opt( "<?php echo @old('roles')[0];?>")
</script>
@endsection