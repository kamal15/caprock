@extends('layouts.master')
@section('content')
<!---- Form Section ---->
<section class="content">
  <div class="container-fluid">

    <div class="card card-default" style="box-shadow:none;">


      <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-5">
          <h2>Create New User</h2>
        </div>
        <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary" href="{{ route('users.index') }}">
            Back</a></div>
      </div>


      <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
        </div>
      </div>
      {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
      <div class="row">
        <div class="col-md-3"> &nbsp;</div>
        <div class="col-md-6">
          <div class="card card-danger">
            <div class="card-header bg-caprock-col" >
              <h3 class="card-title">Register User</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Name:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</i></span>
                  </div>
                  {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}

                </div>
              </div>
              <div class="form-group">
                <label>Email:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">#</span>
                  </div>
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Confirm Password:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">##</span>
                  </div>
                  {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' =>
                  'form-control')) !!}
                </div>
              </div>
              @php $listComp = company_listing();
              $session_data = get_session_data();## Helper functionfetching all session data
              $user_id = $session_data['user_id'];
              $fk_company_id =$session_data['fk_company_id'];
              $user_role='';               
              $currentuser = Auth::user() ->getRoleNames();
              if(count($currentuser)>0){
                $user_role = $currentuser[0];
              }
             // echo '<pre>';print_r( $currentuser);exit;
              //echo '****'.$currentuser ->getRoleNames();
              @endphp

              <div class="form-group">
                              
                <label>Role:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-laptop"></i></span>
                  </div> 
                  @if($user_role=='Caprock Administrator')
                  {!! Form::select('roles[]', array_merge(['' => 'Please Select'], $roles),[], array('class' => 'form-control','onchange'=>"return change_company_opt(this.value);")) !!}
                  @else
                  {!! Form::select('roles[]', $roles,[], array('class' => 'form-control')) !!}
                  @endif
                </div>
              </div>

              @if($user_role=='Caprock Administrator')
              <div class="form-group" id="company_div">
                <label>Company:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-laptop"></i></span>
                  </div>
                  {!! Form::select('fk_company_id', $listComp, '--', ['class' => 'form-control']) !!}
                </div>
              </div>
              @else
              {!! Form::hidden('fk_company_id', "$fk_company_id", array()) !!}

              @php
              $comp_dt = company_listing($fk_company_id);
              if($comp_dt){
              $compArr = array_values($comp_dt->toArray());
              $comp_name = $compArr;
              }
              @endphp
              <div class="form-control"><b>{{ ucwords(@$comp_name[0]) }}</b> </div>
              @endif

              
              <div class="">
                {!! Form::hidden('created_by', $user_id, array('placeholder' => 'created_by','class' => 'form-control'))
                !!}
                <button type="submit" class="btn btn-info">Submit</button> 
                <a href="" class="btn btn-info float-right">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
</section>
 
<script>
  function change_company_opt(val){ 
    $("#company_div").show();
   if(val=='Caprock Administrator'){
      $("#company_div").hide();
   }

  }
  change_company_opt( "<?php echo @old('roles')[0];?>")
</script>
<!---- Form Section ---->


@endsection