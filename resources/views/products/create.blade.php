@extends('layouts.master')
@section('content')
<!---- Form Section ---->
<section class="content">
  <div class="container-fluid">

    <div class="card card-default" style="box-shadow:none;">

      <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-5">
          <h2>Create New Product</h2>
        </div>
        <div class="col-md-1"> <a class="btn btn-primary" href="{{ route('products.index') }}">
            Back</a></div>
      </div>

      <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
        </div>
      </div>
      {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
      <div class="row">
        <div class="col-md-3"> &nbsp;</div>
        <div class="col-md-6">
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">Register User</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Name:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</i></span>
                  </div>
                  {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}

                </div>
              </div>
              <div class="form-group">
                <label>Email:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">#</span>
                  </div>
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="form-group">
                <label>Confirm Password:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">##</span>
                  </div>
                  {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' =>
                  'form-control')) !!}
                </div>
              </div>


              <div class="form-group">
                <label>Role:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-laptop"></i></span>
                  </div>
                  {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                </div>
              </div>
              <div class="card-footer">
                {!! Form::text('created_by', $user_id, array('placeholder' => 'created_by','class' => 'form-control'))
                !!}
                <button type="submit" class="btn btn-info">Submit</button>
                <button type="submit" class="btn btn-default float-right">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
</section>
<!---- Form Section ---->


@endsection