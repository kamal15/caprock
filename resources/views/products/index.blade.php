@extends('layouts.master')
@section('content') 
<div class="section_wrapper mcb-section-inner">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="col-md-4">&nbsp;</div>
        <div class="col-md-8">
        @include('layouts/auth_links')
        </div>
         
    </div>
</div>

<div class="section_wrapper mcb-section-inner"> 
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="col-md-4">&nbsp;</div>
      <div class="col-md-4 pull-left">
        <h2>Products</h2>
      </div>
      <div class="col-md-3 pull-left">
        @if(session('impersonated_by') )
        <a class="alert alert-info pull-left" href="<?php echo url('impersonate_leave') ;?>">Back to my account</a>
        @else
        <!-- <li> <a class="btn btn-info" href="{{ route('logout') }}">Logout</a></li>  -->
        @endif</div>


      <div class="pull-right">
        @can('product-create')
        <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
        @endcan
      </div>
    </div>
  </div>


  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif


  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Details</th>
      <th width="280px">Action</th>
    </tr>
    @foreach ($products as $product)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $product->name }}</td>
      <td>{{ $product->detail }}</td>
      <td>
        <form action="{{ route('products.destroy',$product->id) }}" method="POST">
          <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
          @can('product-edit')
          <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
          @endcan


          @csrf
          @method('DELETE')
          @can('product-delete')
          <button type="submit" class="btn btn-danger">Delete</button>
          @endcan
        </form>
      </td>
    </tr>
    @endforeach
  </table>


  {!! $products->links() !!}





</div>
@endsection