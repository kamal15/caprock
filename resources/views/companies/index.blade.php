@extends('layouts.master')
@section('content')
@php
$status_arr = array('Inactive', 'Active', 'Deleted');
$srchComp = @$_REQUEST['srchComp'];
@endphp
<div class="section_wrapper mcb-section-inner">
  <div class="row">
    <div class="col-lg-12 margin-tb alert alert-danger bg-caprock-col" >
      <div class="col-md-2 pull-left" style="top:8px;">
        <h2>Companies</h2>
      </div>
      
      <div class="col-md-6 pull-left" style="top:8px;">
        <form name="frm_srch" id="frm_srch" action="{{ route('companies.index') }}" method="POST">
          @csrf
          <div class="form-group row">
            <div class="input-group col-md-6"> 
              <input  style="background-color: white;" type="text" value="{{old('srchComp',$srchComp)}}"class="form-control" name="srchComp" id="srchComp" placeholder="Search Company name"> 
            </div>
            <div class="input-group col-md-4">
              <select class="form-control" name="status" id="status">
                <option value="">--Select Status --</option>
                <option @if (isset($_REQUEST['status'])) @if ($_REQUEST['status']=='0' ) selected="selected" @endif @endif
                  value="0">Inactive</option>
                <option @if (isset($_REQUEST['status'])) @if ($_REQUEST['status']=='1' ) selected="selected" @endif @else selected @endif
                  value="1">Active</option>
                <option @if (isset($_REQUEST['status'])) @if ($_REQUEST['status']=='2' ) selected="selected" @endif @endif
                  value="2">Deleted</option>
              </select>
            </div>
            <div class="input-group col-md-2"><input type="button" class="link_btn" value="Search" name="search" onclick="return submit_form();" style="background-color: #fec62a;"></div>
          </div>
        </form>
      </div>
      <div class="pull-right " style="margin-top:10px;">
        @if(session('impersonated_by') )
        <!-- <a class="alert link_btn pull-right" style="text-decoration:none;" href="<?php echo url('impersonate_leave') ;?>">Back to my account</a> -->
        @else
        {{-- <li> <a class="btn btn-info" href="{{ route('logout') }}">Logout</a></li> --}}
        @endif
        
        @can('company-create')
        <a class="acls link_btn" href="{{ route('companies.create') }}" style="text-decoration: none;margin-top:8px;">Create Company</a>
        @endcan
      </div>
    </div>
  </div>
  @include('layouts.error')
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th class="text-justify">Company Name</th>
      <th class="text-justify">Description</th>
      <th width="280px">Action</th>
    </tr>
    @php $i = 0; @endphp
    @foreach ($companies as $company)
    <tr>
      <td>{{ ++$i }}</td>
      <td class="text-justify">{{ $company->company_name }}</td>
      <td class="text-justify">{{ substr($company->description, 0, 100) }}</td>
      <td>
        <div class="row">
          <div class="col-md-3">
            @can('company-show')
            <a class="btn btn-sm btn-info" href="{{ route('companies.show',$company->id) }}"><i class="fas fa-eye"></i> </a>
            @endcan
          </div>
          <div class="col-md-3">
            @can('company-edit')
            <a class="btn btn-sm btn-primary" href="{{ route('companies.create',$company->id) }}"><i class="fas fa-edit"></i></a>
            @endcan
          </div>
          <div class="col-md-3">
          <form action="{{ route('companies.destroy',$company->id) }}" method="POST" id="frm_{{$company->id}}">
              @csrf
              @method('DELETE')
              @can('company-delete')
              <button type="button" onclick="return deleteComp('<?php echo $company->id ?>');" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
              @endcan
            </form>
          </div>
        </div>
      </td>
    </tr>
    @endforeach
  </table>
  
  {{ $companies->appends(Request::except('page'))->links() }}

</div>
<script>
  function submit_form(){ 
    if($("#srchComp").val()=='' && $( "#status option:selected" ).val()==''){
      return false;
    }
    document.getElementById('frm_srch').submit();
  }


  function deleteComp(id){ 
    var r = confirm("Are you sure to delete company?");
    if (r == true) {
      $("#frm_"+id).submit(); 
    } else {
      return false;
    }
  }
</script>
@endsection