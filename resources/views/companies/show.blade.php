@extends('layouts.master')
@section('content')
@php
$id = '';
$company_name = '';
$description = '';
$status = '';
$status_arr = array('Inactive', 'Active', 'Deleted');
if( !empty($company) ){
$id = $company->id;
$company_name = $company->company_name;
$description = $company->description;
$status = $company->status;
$loan_group = $company->loan_group;
$loan_class = $company->loan_class;
}
@endphp

<?php 
$clientLogoExists = '';
$session_data       = get_session_data();
$com_logo_name = '';
$com_logo_name = company_logo($id); 
$caprock_Cl_logo = '';
if(file_exists('./documents/company_logo/'.$com_logo_name) && !empty($com_logo_name)){
  $caprock_Cl_logo = '/documents/company_logo/'.$com_logo_name; 
} 
 
//echo '***'.$caprock_Cl_logo;exit;
?>
<div class="section_wrapper mcb-section-inner">
  <!---- Form Section ---->
  <section class="content">
    <div class="container-fluid">
      <div class="card card-default" style="box-shadow:none;">
        <div class="row">
          <div class="col-md-3">&nbsp;</div>
          <div class="col-md-5">
            <h2>Company Detail</h2>
          </div>
          <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary"
              href="{{ route('companies.index') }}">
              Back</a>
          </div>
          <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">
          <div class="col-md-3"> &nbsp;</div>
          <div class="col-md-6">
            <div class="card card-danger">
              <div class="card-header bg-caprock-col">
                <h3 class="card-title">Company Detail</h3>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Company Name:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-building"></i></span>
                    </div>
                    {!! Form::text('company_name', old('company_name', $company_name), array(
                    'placeholder' => 'Company Name',
                    'disabled' => '',
                    'class' => 'form-control')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label>Description:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-book-open"></i></span>
                    </div>
                    {!! Form::textarea('description', old('description', $description), array('placeholder' =>
                    'Description', 'disabled' => '', 'class' =>
                    'form-control')) !!}
                  </div>
                </div>

                <div class="form-group">
                  <label>Logo:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                    @if($caprock_Cl_logo)
                    <img src="{{@url($caprock_Cl_logo)}}" style="width:250px;">
                    @else
                    <img src="{{@url('assets/default/no-logo-image.png')}}" style="width:250px;">
                    @endif
                    </div>
                     
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label>Loan Group:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-book-open"></i></span>
                    </div>
                    {!! Form::text('loan_group', old('loan_group', $loan_group), array('placeholder' =>
                    'Loan Group', 'disabled' => '', 'class' =>
                    'form-control')) !!}
                  </div>
                </div>

                <div class="form-group">
                  <label>Loan Class:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-book-open"></i></span>
                    </div>
                    {!! Form::text('description', old('loan_class', $loan_class), array('placeholder' =>
                    'Loann Class', 'disabled' => '', 'class' =>
                    'form-control')) !!}
                  </div>
                </div>
 -->


                <div class="form-group">
                  <label>Status:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-users-cog"></i></span>
                    </div>
                    {!! Form::select('status', $status_arr, $status, array('disabled' => '', 'class' => 'form-control'))
                    !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!---- Form Section ---->
</div>
@endsection