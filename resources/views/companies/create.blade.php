@extends('layouts.master')
@section('content')
<style type="text/css">
  .hidden {
    display: none;
  }
</style>
@php
  $id = '';
  $company_name = '';
  $description = '';
  $status = '';
  $status_arr = array('Inactive', 'Active');
  if( !empty($company) ){
    $id = $company->id;
    $company_name = $company->company_name;
    $description = $company->description;
    $status = $company->status;
    $loan_class = $company->loan_class;
    $loan_group = $company->loan_group;
  }
@endphp
<div class="section_wrapper mcb-section-inner">
  <!---- Form Section ---->
  <section class="content">
    <div class="container-fluid">
      <div class="card card-default" style="box-shadow:none;">
        <div class="row">
          <div class="col-md-3">&nbsp;</div>
          <div class="col-md-5">
            <h2>{{ Request::segment(3) ? 'Update Registered Company' : 'Create New Company'}}</h2>
          </div>
          <div class="col-md-1" style="padding-left:4%"> <a class="btn btn-primary"
              href="{{ route('companies.index') }}">
              Back</a>
          </div>
          <div class="col-md-3">&nbsp;</div>
        </div>
        <div class="row">
          <div class="col-md-3"> &nbsp;</div>
          <div class="col-md-6">
            @include('layouts.error')
            <div class="card card-danger ">
              <div class="card-header bg-caprock-col" style="color:white;">
                <h3 class="card-title">{{ Request::segment(3) ? 'Update Company' : 'Register Company'}}</h3>
              </div>
              <div class="card-body">
                {!! Form::open(array('route' => array('companies.store'), 'method'=>'POST', 'enctype' =>
                "multipart/form-data")) !!}
                {!! Form::hidden('id', $id) !!}
                <div class="form-group">
                  <label>Company Name:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-building"></i></span>
                    </div>
                    {!! Form::text('company_name', old('company_name', $company_name),
                    array('placeholder' => 'Company Name',
                    'class' => 'form-control')) !!}
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Description:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-book-open"></i></span>
                    </div>
                    {!! Form::textarea('description', old('description', $description), array('placeholder' =>
                    'Description','class' =>
                    'form-control')) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label>Logo:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-file-image"></i></span>
                    </div>
                    <div class="custom-file">
                          <input type="file" name="logo[]" class="custom-file-input" id="documents">
                          <label class="custom-file-label" for="files_input">Choose file</label>
                        </div>
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label>Loan Class:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-building"></i></span>
                    </div>
                    {!! Form::text('loan_class', old('loan_class', $loan_class),
                    array('placeholder' => 'Loan Class',
                    'class' => 'form-control')) !!}
                  </div>
                </div>

                <div class="form-group">
                  <label>Loan Group:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-building"></i></span>
                    </div>
                    {!! Form::text('loan_group', old('loan_group', $loan_group),
                    array('placeholder' => 'Loan Group',
                    'class' => 'form-control')) !!}
                  </div>
                </div> -->

                <div class="form-group {{ Request::segment(3) ? '' : 'hidden' }}">
                  <label>Status:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-users-cog"></i></span>
                    </div>
                    {!!Form::select('status', $status_arr, $status, array('class' =>
                    'form-control')) !!}
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!---- Form Section ---->
  
</div>

<script>
  $('#documents').on('change',function(){
      //get the file name
      var fileName = $(this).val(); 
      fileName =fileName.replace("C:\\fakepath\\", "");
      //replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
  })
</script>
@endsection