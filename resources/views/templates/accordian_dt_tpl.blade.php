<div class="card" style="margin-bottom:3px;height: 226px;
overflow-y: scroll;">
<style type="text/css">
  .amountCls{text-align: right;}
</style>
<?php $count_inputs = 0;
  $amount_arr = array('Original Loan Amt','Floor Price','Sales price','Total charges');
  //echo '<pre>';print_r(@$add); 
        if(!empty(@$no_of_inputs)){
            $count_inputs = $no_of_inputs;
        }?>
     <div class="row_1" id="heading<?php echo $name;?>_<?php echo $key;?>"> 
        <a class="btn-link"
            style="cursor:pointer;background-color: #fff;width: 100%;color:#8f130c;"
                data-toggle="collapse" data-target="#collapse<?php echo $name;?>_<?php echo $key;?>"
            aria-expanded="true" aria-controls="collapse<?php echo $name;?>">
            <div class="card-body" style="width: 100%;padding:0px;">
            <div>
                <?php 
                if($title=='Assignment type'){?>
                <div class="col-md-12">
                    <p><label class="k-checkbox" style="color:#7f7f7f;font-weight: 500px;">
                      <input type="checkbox" name="assignment_type[]" value="recovery" id="assignment_type1" checked="checked">
                      Recovery
                      <span></span>
                    </label></p>
                  </div>
                <div class="col-md-12">
                    <p><label class="k-checkbox" style="color:#7f7f7f;font-weight: 500px;">
                      <input type="checkbox" name="assignment_type[]" value="remarketing" id="assignment_type2"  >
                      Remarketing
                      <span></span>
                    </label></p>
                  </div>
                <div class="col-md-12">
                    <p><label class="k-checkbox" style="color:#7f7f7f;font-weight: 500px;">
                      <input type="checkbox" name="assignment_type[]" value="both" id="assignment_type3" >
                      Both
                      <span></span>
                    </label></p>
                  </div>
                <?php }else if($title=='Storage Address'){
                      if(@count($fld_arr)>0){ 
                          foreach($fld_arr as $k=>$v){
                            if($k=='Storage Address'){?>
                              <div class="col-md-12" style="padding: 0px; padding-left:5px;background: #e2dede;">
                                  <p><div style="font-size:12px;">{{@$k}}<b>:</b></div> <div style="font-size:12px;">{{@$v}}</div></p> 
                              </div>
                          <?php }
                           }  
                      }?>
                      <div class="col-md-12">
                          <label class="k-checkbox" style="color:#7f7f7f;font-weight:100px !important;">
                            <input type="checkbox" name="keys" value="1" id="keys" 
                            <?php if(!empty(@$fld_arr['Keys']) && @$fld_arr['Keys']=='Y'){?>checked="checked"<?php }?>>
                            Keys
                            <span></span>
                          </label> <br>
                           <label class="k-checkbox" style="color:#7f7f7f;font-weight:100px !important;">
                            <input type="checkbox" name="drivables" value="1" id="drivables" <?php if(!empty(@$fld_arr['Drivable']) && @$fld_arr['Drivable'] =='Y'){?>checked="checked"<?php }?>>
                            Vehicle Drivable
                            <span></span>
                          </label> 
                      </div>
                <?php }else{
                        if(@count($fld_arr)>0){ 
                            foreach($fld_arr as $k=>$v){
                              $addCls=  '';
                              if($k=='ssn' && !empty($v)){ 
                                $v = 'XXX-XXX'.substr($v,6,10);;
                              }
                              if(!empty($v) && in_array($k, $amount_arr)){ 
                                $addCls="amountCls";
                                $v = '$'.$v;
                              }
                              if($k=='Vehicle Condition Report'){

                              ?>
                            <div class="col-md-12" style="padding: 0px; padding-left:5px;background: #e2dede;">
                                <p><div style="font-size:12px;"> </div><div style="font-size:12px;" class="<?php echo $addCls;?>"> <a target="_blank" href="http://{{@$v}}">{{ucfirst($k)}}</a></div></p> 
                            </div>
                            <?php 
                             }else{?>

                              <div class="col-md-12" style="padding: 0px; padding-left:5px;background: #e2dede;">
                                <p><div style="font-size:12px;">{{ucfirst($k)}}<b>:</b></div><div style="font-size:12px;" class="<?php echo $addCls;?>"> {{@$v}}</div></p> 
                            </div>
                             <?php }
                           }
                             ?>
                            <?php if(@$city_state_zip=='yes') {?>
                                <div style="font-size:12px;"> {{@$add['city']}} - {{@$add['state']}} - {{@$add['zip']}}  </div>
                            <?php }
                        }
                }?>
            </div>
        </div>
        </a> 
    </div>

   
</div>