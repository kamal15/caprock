<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Request New Account</title>
</head>

<body>
  <p>Hi there!<br>
    You have a new request to create an account from {{$data['name']}} <br>
    Please find the information provided by the user<br><br>
    Name: {{$data['name']}}<br>
    Email Address: {{$data['email']}}<br>
    Phone: {{$data['phone']}}
  </p>

</body>

</html>