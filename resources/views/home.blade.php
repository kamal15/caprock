@extends('layouts.master')


@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-bottom: 100px;">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body" style="padding-bottom:100px;"> 
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="alert alert-success" role="alert">
                    You are logged in!
                    </div>
                    @include('layouts/auth_links')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
