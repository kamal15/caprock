<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('logout', function () {
    Auth::logout();
    return Redirect::to("/login")
      ->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
});

Route::get('/', function () {
    Auth::logout();
    return Redirect::to("/login")
      ->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
});
 
Route::get('/home', 'UserController@index')->name('/');


Auth::routes();


Route::get('/roles', 'RoleController@index')->name('roles');


Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('products', 'ProductController');
});

Route::get('impersonate/{user_id}', 'RoleController@impersonate');
Route::get('impersonate_leave', 'RoleController@impersonate_leave');

##=================== Company Routes Start ==========================##

Route::any('companies', 'CompanyController@index')->name('companies.index');
Route::get('companies/create/{company?}', 'CompanyController@create')->name('companies.create');
Route::post('companies/store', 'CompanyController@store')->name('companies.store');
Route::get('companies/show/{company}', 'CompanyController@show')->name('companies.show');
Route::get('companies/edit/{company}', 'CompanyController@edit')->name('companies.edit');
Route::post('companies/update/{company}', 'CompanyController@update')->name('companies.update');
Route::delete('companies/destroy/{company}', 'CompanyController@destroy')->name('companies.destroy');

##=================== Company Routes End ==========================##

##=================== Document Routes Start ==========================##

Route::get('all-documents', 'DocumentController@index')->name('documents.index');
Route::post('upload-document-file', 'DocumentController@file_upload')->name('upload-document-file');
Route::post('uploaded-files-listing', 'DocumentController@list_uploaded_file')->name('uploaded-files-listing');
Route::get('download-file/{file}', 'DocumentController@download_file')->name('download-file');

Route::get('deleting-file/{id}/{userID}', 'DocumentController@deleting_file')->name('deleting-file');

##=================== Document Routes End ==========================##

##=================== Vehicle Routes Start ==========================##

Route::get('vehicles/create', 'VehicleController@create')->name('vehicles.create');
Route::post('vehicles', 'VehicleController@store')->name('vehicles.store');
Route::get('vehicles/{vehicle}/edit', 'VehicleController@edit')->name('vehicles.edit');
Route::post('update-vehicle', 'VehicleController@update')->name('vehicles.update');

##=================== Vehicle Routes End ==========================##

##=================== File Upload Routes Start ==========================##

Route::get('files-upload', 'FileUploadController@create');
Route::post('save-files', 'FileUploadController@store')->name('files.store');

##=================== File Upload Routes End ==========================##

##=================== File Upload Routes End ==========================##

Route::any('/assignments/create', 'AssignmentController@create')->name('assignments.create');
Route::post('/assignments', 'AssignmentController@store')->name('assignments.store');

##=================== File Upload Routes End ==========================##

##================================ RequestAccount Routes Start =============================##

Route::get('account/create', 'RequestAccountController@create')->name('request-account');
Route::post('account/create', 'RequestAccountController@store');

##================================ RequestAccount Routes End =============================##


Route::get('dropzone', 'FileUploadController@dropzone');
Route::post('store', 'FileUploadController@dropzoneStore')->name('store');

##================================ Portfolio Screen Start =============================## 
Route::get('portfolio', 'AssignmentController@portfolio')->name('portfolio'); 
Route::get('portfolio-detail', 'AssignmentController@portfolio_vehicle_dt')->name('portfolio-detail');

##================================ Portfolio Screen Routes End =============================##

Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');