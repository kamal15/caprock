<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentsByState extends Model
{
    protected $table = 'assignmentsByState';
    public $timestamps = false;
}
