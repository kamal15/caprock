<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblNote extends Model
{
    protected $table = 'tbl_notes'; 
  
    public $timestamps = false;
}
