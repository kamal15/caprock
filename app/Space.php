<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//use OwenIt\Auditing\Contracts\Auditable;

class Space extends Model
{
    //use \OwenIt\Auditing\Auditable;

    protected $table = 'spaces';
    protected $fillable = ['*'];
    public $timestamps = false;
}
