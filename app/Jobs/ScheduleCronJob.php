<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\FileUpload;
use App\Mail\failedImportReminder;

use Mail;
use Exception;
use App\Assignment_Client;
use App\Title_Status;
use App\Scheduled_cron_jobs_error_log;
use App\Average_vehicle_grade;
use App\Average_sales_price;
use App\AssignmentsByState;
use App\Average_days_to_reposses;
use App\Average_days_to_sell;
use App\Last_two_days_agent_report;
use App\Last_two_days_auction_report;
use App\Title_not_received;
use App\Mail\WelcomeEmail;

class ScheduleCronJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $file_data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file_data)
    {
        $this->file_data = $file_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            //  /home/caprockapp/webapps/innovate/cron_jobs_docs/
            $file_path = '/home/caprockapp/webapps/innovate/cron_jobs_docs/'; //str_replace('public','cron_jobs_docs',$_SERVER['DOCUMENT_ROOT'] );
            $file = $this->file_data;

            // echo '**'.$file_path;exit;
            $file_arr = explode($file_path, $file);
            //echo '<pre>';print_r($file_arr);exit;
            $base_folder = './cron_jobs_docs/';
            if (@count($file_arr) > 0) {
                $explode_more = explode('/', $file_arr[1]);
                //echo '<pre>';print_r($explode_more);exit;
                $folder_name = $explode_more[0];
                switch ($folder_name) {
                    case "Title Status": //echo $file_arr[1];exit;
                        $this->title_status($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Caprock to App Assignments":
                        $this->caprock_to_app_assignments($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Average Vehicle Grade":
                        $this->average_vehicle_grade($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Average Sales Price":
                        $this->average_sales_price($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;

                    case "Average Days to Sell":
                        $this->average_days_to_sell($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "AssignmentsByState":
                        $this->assignmentsbystate($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Average Days to Reposses":
                        $this->average_days_to_reposses($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Last Two Days Auction Report":
                        $this->last_two_days_auction_report($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Last Two Days Agent Report":
                        $this->last_two_days_agent_report($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    case "Title Not Received Report":
                        $this->title_not_received($base_folder . $file_arr[1]);
                        unlink('cron_jobs_docs/' . $file_arr[1]);
                        break;
                    default:
                        echo "";
                }
                DB::table('import_history_log')->where('name', $folder_name)->update(['time' => date('Y-m-d H:i:s')]);
            }
        } catch (Exception $exception) {
            // if exception found
            $this->failed($exception);
        }
    }

    public function failed(Exception $exception)
    {
        $error = $exception->getMessage();
        //send email
        $this->mail_sent($error);
    }


    public function mail_sent($error)
    {
        $data = array();
        $data['error'] = $error;
        Mail::to(config('constants.REPO_EMAIL'))
            ->cc(config('constants.REMARKETING_EMAIL'))
            ->cc(config('constants.DEV_EMAIL'))
            ->send(new WelcomeEmail($data));
    }

    ## Date format
    function date_formated($value)
    {
        return $value = date('Y-m-d H:i:s', $value);
    }

    function clean_data($string)
    {
        $string =  preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $string); //($string);
        if (empty($string) || $string === null || !($string)) {
            return $string = 0;
        } else {
            $string = str_replace(' ', ' ', $string);
            return $string = preg_replace('/-+/', '-', ($string));
        }
        return $string = ($string);
    }

    function log_error($line, $error)
    {
        $table_data             =  array();
        $table_data             =  new Scheduled_cron_jobs_error_log;

        $table_data->line_no            = $line;
        $table_data->error_description  = $error;
        if ($table_data->save()) {
        }
    }

    public function caprock_to_app_assignments($file)
    {

        Assignment_Client::truncate();
        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                //echo '<pre>';print_r($column);exit;
                if ($i != 1) {

                    //---------------- Insert Data start ------------------------------//
                    $table_data                             = new Assignment_Client;
                    $table_data->assignment_type            = @!empty($column[0]) ? $column[0] : null;
                    $table_data->repo_type                  = @!empty($column[1]) ? $column[1] : null;
                    $table_data->make                       = $this->clean_data(@$column[2]);
                    $table_data->model                      = $this->clean_data(@$column[3]);
                    $table_data->year                       = $this->clean_data(@$column[4]);
                    $table_data->vin                        = $this->clean_data(@$column[5]);
                    $table_data->license_plate              = $this->clean_data(@$column[6]);
                    $table_data->vehicle_address            = $this->clean_data(@$column[7]);
                    $table_data->vehicle_apt_no             = $this->clean_data(@$column[8]);
                    $table_data->vehicle_city               = @$column[9];
                    $table_data->vehicle_state              = @$column[10];
                    $table_data->vehicle_zip                = @$column[11];
                    $table_data->legal_sale_date            = @$column[12];
                    $table_data->keys                       = addslashes(@$column[13]);
                    $table_data->client_name                = @$column[14];
                    $table_data->client_account_no          = @$column[15];
                    $table_data->loan_class                 = @$column[16];
                    $table_data->caprock_account            = @$column[17];
                    $table_data->contract_date              = @$column[18];
                    $table_data->original_loan_amount       = @$column[19];
                    $table_data->first_name                 = @$column[20];
                    $table_data->last_name                  = @$column[21];
                    $table_data->ssn                        = @$column[22];
                    $table_data->customer_address           = @$this->clean_data(@$column[23]);;
                    $table_data->customer_apt_no            = @$column[24];
                    $table_data->customer_city              = @$column[25];
                    $table_data->customer_state             = @$column[26];
                    $table_data->customer_zip               = @$column[27];
                    $table_data->cust_poe_company_name      = !empty($column[28]) ? $this->clean_data($column[28]) : '';
                    $table_data->cust_poe_company_address   = !empty($column[29]) ? $this->clean_data(@$column[29]) : '';
                    $table_data->cust_poe_suite_no          = @$column[30];
                    $table_data->cust_poe_company_city      = @$column[31];
                    $table_data->cust_poe_company_state     = @$column[32];
                    $table_data->cust_poe_company_zip       = @$column[33];
                    $table_data->assignment_notes           = @$column[34];
                    $table_data->vehicle_notes_type         = @$column[35];
                    $table_data->vehicle_notes              = $this->clean_data(@$column[36]);
                    $table_data->date_initial_assignment_received = @$column[37];
                    $table_data->status                     = @$this->clean_data($column[38]);
                    $table_data->repo_status                = @$column[39];
                    $table_data->cure_sent                  = @$column[40];
                    $table_data->cure_expired               = @$column[41];
                    $table_data->date_assigned_to_repo_agent = @$column[42];
                    $table_data->recovery_date              = @$column[43];
                    $table_data->NOI_mail_date              = @$column[44];
                    $table_data->NOI_expire_date            = @$column[45];
                    $table_data->repo_hold_reason           = $this->clean_data(@$column[46]);
                    $table_data->close_reason               = addslashes(@$column[47]);
                    $table_data->close_date                 = @$column[48];
                    $table_data->vehicle_driveable          = $this->clean_data(@$column[49]);
                    $table_data->storage_address            = @$column[50];
                    $table_data->storage_apt_no             = null;
                    $table_data->storage_city               = null;
                    $table_data->storage_state              = null;
                    $table_data->storage_zip                = null;
                    $table_data->auction_status             = $this->clean_data(@$column[51]);
                    $table_data->auction                    = $this->clean_data(@$column[52]);
                    $table_data->auction_address            = $this->clean_data(@$column[53]);
                    $table_data->auction_title_status       = $this->clean_data(@$column[54]);
                    $table_data->secured_date               = $this->clean_data(@$column[55]);
                    $table_data->condition_report_link      = $this->clean_data(@$column[56]);
                    $table_data->Mileage                    = $this->clean_data(@$column[57]);
                    $table_data->Grade                      = @$column[58];
                    $table_data->MMR                        = @$column[59];
                    $table_data->floor_price                = @$column[60];
                    $table_data->sales_price                = @$column[61];
                    $table_data->auction_age                   = @!empty($column[62]) ? $column[62] : 0;
                    $table_data->hold_date                  = @$this->date_formated(@$column[63]);
                    $table_data->sale_hold_reason           = addslashes(@$column[64]);
                    $table_data->scheduled_sale_date        = @$column[65];
                    $table_data->total_charges              = @$column[66];
                    $table_data->sold_date                  = @$this->date_formated(@$column[67]);
                    $table_data->report_information         = NULL;
                    $table_data->row_status                 = 0;
                    $table_data->created_by                 = 0;
                    $table_data->days_run                   = @!empty($column[68]) ? $column[68] : 0;
                    $table_data->client_queue_no            = @$column[69];
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        
                        
                        $error = $ex->getMessage();
                        print $error;
                        ## Send mail notification
                        //$this->mail_sent($error);
                        ####
                        $q = DB::getQueryLog();
                       // $this->log_error($i, $error);
                        throw $ex;
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }

    public function title_not_received($file)
    {
        Title_not_received::truncate();
        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column    = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //----------------------- Insert Data start -----------------------//
                    $table_data                     = new Title_not_received;
                    $table_data->Vin                = $this->clean_data(@$column[0]);
                    $table_data->Year               = $this->clean_data(@$column[1]);
                    $table_data->Make               = $this->clean_data(@$column[2]);
                    $table_data->Model              = $this->clean_data(@$column[3]);
                    $table_data->title_status       = $this->clean_data(@$column[4]);
                    $table_data->innovate_account   = $this->clean_data(@$column[5]);
                    $table_data->client_name        = $this->clean_data(@$column[6]);
                    $table_data->Loan_Class         = $this->clean_data(@$column[7]);
                    $table_data->Purchase_account_no = $this->clean_data(@$column[8]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        ## Send mail notification
                        $this->mail_sent($error);
                        ####
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }

    public function last_two_days_agent_report($file)
    {
        Last_two_days_agent_report::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data                 = new Last_two_days_agent_report;
                    $table_data->VendorName     = $this->clean_data(@$column[0]);
                    $table_data->Make           = $this->clean_data(@$column[1]);
                    $table_data->Model          = $this->clean_data(@$column[2]);
                    $table_data->Year          = $this->clean_data(@$column[3]);
                    $table_data->Vin            = $this->clean_data(@$column[4]);
                    $table_data->CnAccNo        = $this->clean_data(@$column[5]);
                    $table_data->clientName     = $this->clean_data(@$column[6]);
                    $table_data->LoanClass      = $this->clean_data(@$column[7]);
                    $table_data->StatusCode    = $this->clean_data(@$column[8]);
                    $table_data->SecuredDate    = $this->date_formated($column[9]);
                    $table_data->RecoveryDate   = $this->date_formated($column[10]);
                    $table_data->status         =  1;
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        ## Send mail notification
                        $this->mail_sent($error);
                        ####
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }


    public function last_two_days_auction_report($file)
    {
        Last_two_days_auction_report::truncate();
        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data                     = new Last_two_days_auction_report;
                    $table_data->auction            = $this->clean_data(@$column[0]);
                    $table_data->year               = $this->clean_data(@$column[1]);
                    $table_data->make               = $this->clean_data(@$column[2]);
                    $table_data->model              = $this->clean_data(@$column[3]);
                    $table_data->Vin                = $this->clean_data(@$column[4]);
                    $table_data->CnAccNo            = $this->clean_data(@$column[5]);
                    $table_data->client_name        = $this->clean_data(@$column[6]);
                    $table_data->loan_class        = $this->clean_data(@$column[7]);
                    $table_data->mileage           = $this->clean_data(@$column[8]);
                    $table_data->days_to_secure    = $this->clean_data(@$column[9]);

                    $table_data->secured_date      = date('Y-m-d H:i:s', $column[10]);
                    $table_data->status            = $this->clean_data(@$column[11]);
                    $table_data->title_status      = $this->clean_data(@$column[12]);
                    $table_data->title_received    = $this->clean_data(@$column[13]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        ## Send mail notification
                        $this->mail_sent($error);
                        ####
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }



    public function title_status($file)
    {
        Title_Status::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new Title_Status;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        ## Send mail notification
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
            exit;
        }
    }

    public function average_vehicle_grade($file)
    {
        Average_vehicle_grade::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new Average_vehicle_grade;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }

    public function average_sales_price($file)
    {
        Average_sales_price::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new Average_sales_price;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }

    public function average_days_to_sell($file)
    {
        Average_days_to_sell::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new Average_days_to_sell;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }


    public function assignmentsbystate($file)
    {
        AssignmentsByState::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new AssignmentsByState;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }

    public function average_days_to_reposses($file)
    {
        Average_days_to_reposses::truncate();

        if (!empty($file) && file_exists($file)) {
            $i = 1;
            $file              = fopen($file, "r");
            DB::enableQueryLog();
            while (($column = fgetcsv($file, 100000, ",")) !== false) {
                if ($i != 1) {
                    //---------------- Insert Data start ------------------------------//
                    $table_data              = new Average_days_to_reposses;
                    $table_data->Client  = $this->clean_data(@$column[0]);
                    $table_data->Identifier  = $this->clean_data(@$column[1]);
                    $table_data->BulkDate     = $this->clean_data(@$column[2]);
                    //----------------------- Insert data end ------------------------//
                    try {
                        $table_data->save();
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $error = $ex->getMessage();
                        $this->mail_sent($error);
                        $this->log_error($i, $error);
                    }
                }
                $i++;
            }
        } else {
            echo 'error';
        }
    }
}
