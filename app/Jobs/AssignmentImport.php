<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\FileUpload;
use App\TblAccount;
use App\Space;
use App\TblCustomerSpace;
use App\TblNote;
use App\Contact;
use App\TblVehicle;
use App\Cron_logs;
use App\Cron_last_count;
use App\Assignment_Client1;
use App\Mail\failedImportReminder;
Use Mail;
Use App\Schedule_for_sale;
Use App\Secured_last_day;
 

class AssignmentImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FileUpload $file_data)
    {
        $this->file_data = $file_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         

        try { 
            
            $file = $this->file_data;
            $fileArr = json_decode(json_encode($this->file_data),true);
            $file_org_name = $fileArr['file_org_name'];
            $base_file_path = public_path('/files');
            $file_path      = $base_file_path . '/' . $file->file_name;
            // $csv_files_array = array(
            //                     'tbl_accounts'           =>'tblaccount.csv',
            //                     'contact'                =>'tblcontact.csv',
            //                     'tbl_notes'             => 'tblnotes.csv' ,
            //                     'tbl_unitactiveaccount' => 'tblunitactiveaccount.csv',
            //                     'spaces_covered'         => 'spacescovered.csv',
            //                     'tbl_address'            => 'tbladdress.csv',
            //                     'tbl_outside_accounts'   => 'tbloutsideaccount.csv',
            //                     'get_activities'         => 'gateactivit.csv',
            //                     'tbl_audits'             => 'tblaudit.csv',
            //                 );

            $limit              = 20000;
            $table_unique_id    = array(
            'assignments_by_client' => 'id',
            'schedule_for_sale'     => 'id',
            'secured_last_day'      => 'id'
             
        );
            //$table_name = strstr($file->file_org_name, '.', true);
            //$unique_id  = $table_unique_id[$table_name];
            
            if($file_org_name=='Scheduled_for_sale.csv'){
                $this->import_data($file_path, $table_name = 'schedule_for_sale', $limit, $unique_id = 'id');
            }
            else if($file_org_name=='Secured_last_2_days.csv'){
                $this->import_data($file_path, $table_name = 'secured_last_day', $limit, $unique_id = 'id');
            }else if('Test_File_DHX_Software.csv'){
                
                $this->import_data($file_path, $table_name = 'assignments_by_client', $limit, $unique_id = 'id');
            }

            $data = [];
        
            
        } catch (\Exception $e) {
            // if exception found
            
            $this->failed($e);
        }
    }

    public function failed(\Exception $e = null)
    {
        $messageBody="test kamals";
        Mail::raw($messageBody, function ($message) {
            $message->from('join2kamal@gmail.com', 'Learning Laravel');
            $message->to('join2kamal@gmail.com');
            $message->subject('Learning Laravel test email');
        });echo 'kkkk';exit;
       // Send a notification of failure
        

       // Mail::to('join2kamal@gmail.com')->send(new failedImportReminder($data));
    }

    public function import_data($file_path, $table_name, $limit, $unique_id)
    {
        $message            = 'Cron Executed';
        $db                 = DB::connection()->getDatabaseName();
        //echo '***'.$db;exit;
        $file               = fopen($file_path, "r");
        $start_position_arr = $this->get_cron_last_count($table_name, $unique_id);
        $start_position     = 0;
        //echo '***'.$start_position_arr;exit;
        if (@count($start_position_arr)>0) { //echo 'kamal';exit;
            $start_position = $start_position_arr[0]['last_count'];
        }
         
        $i                  = 0;
        $count              = 0;
        $limit_count        = 0;
        $command            = 'insert_data';
        $row_id             = 0;
        ## get the tables column so that data need to be inserted correct
        $table_column_count = array(
            'assignments_by_client' => 67,
            'schedule_for_sale'=>18,
            'secured_last_day'=>17 
        );
         
        while (($column         = fgetcsv($file, 100000, ","))!== false) {
            //echo '<pre>';print_r($column); exit;
            ## This is becouse first line containing column name
            if ($i==0) {
                $i++;
                continue;
            }## This is becouse first line containing column name
            if ($count < $start_position) {
                $count++;
                continue;
            }
            
            $count++;
            $limit_count++;
            if ($limit_count > $limit) {
                break;
            }
 
            $check_columns = $this->check_columns($table_name, $db);
            //echo '<pre>';print_r($check_columns);exit;
            ## Check Command whether records would be updated or inserted, checked by ID

            //echo '***'.$column[0];exit;

            if (is_numeric($column[0]) && $column[0]) {
                $row_exists = $this->check_row_exists($column[0], $table_name, $db, $unique_id);
                if (!empty($row_exists)) {
                    $command    = 'update_data';
                    $row_id     = $column[0];
                }
            }
            ## Check Command whether records would be updated or inserted, checked by ID


            //echo '***'.$check_columns;exit;
            //echo is_numeric($column[0]).'=='.$table_column_count[$table_name];exit; echo '***=='.$count;
            //---------------- Insert Data start ------------------------------//
            if (is_numeric($column[0]) && $check_columns==$table_column_count[$table_name]) {
                ## Calling function to execute
                $this->$table_name($column, $table_name, $db, $limit_count, $command, $row_id, $start_position);
            } else {
                try {
                    $this->$table_name($column, $table_name, $db, $limit_count, $command, $row_id, $start_position);
                } catch (\Illuminate\Database\QueryException $ex) {
                    // echo '***'.($ex->getMessage()); exit;
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
            }
            //----------------------- Insert data end ------------------------//
        }
 
        //reached the end, reset to 0 // Then Set its last count to zero so that it runs from start
        if ($count  == $start_position) {
            $count =  0;
            $start_position=0;
            $table_data['last_count']  = 0;
            $res    = DB::table('cron_last_counts')->where(array('table_name'=>$table_name))->update($table_data);
        }
 
        ## Insert last count in the table
        // echo $limit_count.'***>>='       .$count .'== >>'.$start_position.'==='.$limit;exit;
        if ($count == ($start_position+$limit+1)) {
            $last_count = $start_position+$limit;
            $this->set_cron_last_count($table_name, $unique_id, $last_count);
        }
        ## Insert last count in the table
        return $message;
    }
    
    public function check_columns($table_name, $db)
    {
        $data = 0;
        $count = DB::select(DB::raw("select COUNT(*) as cnt FROM INFORMATION_SCHEMA.COLUMNS where table_schema ='{$db}' and table_name = '{$table_name}'"));
        $res = json_decode(json_encode($count), true);

        if (count($res)>0) {
            $data = $res[0]['cnt'];
        }
        return $data;
    }

     
    public function check_row_exists($columnID, $table_name, $db, $unique_id)
    {
        $data = 0;
        $field = $unique_id;
        $count = DB::select(DB::raw("select COUNT(1) as cnt FROM {$table_name} 
            where {$field} = {$columnID}"));
         
        
        if ($count) {
            $rs = json_decode(json_encode($count), true);
            $data = @$rs[0]['cnt'];
        }
        return $data;
    }


    public function get_data_type($column_name, $table_name, $db)
    {
        $data_var = '';
        return $data_var =  DB::select(DB::raw(
            "SELECT DATA_TYPE as dtype , column_default FROM INFORMATION_SCHEMA.COLUMNS 
                             WHERE TABLE_SCHEMA = '{$db}' AND TABLE_NAME = '{$table_name}' 
                             and COLUMN_name= '{$column_name}'"
        ));
    }

    public function clean_data($column_name, $value, $table_name, $db)
    {
        $data_type = $this->get_data_type($column_name, $table_name, $db);

        $res = json_decode(json_encode($data_type), true);
        if (count($res)>0) {
            $dtype      = $res[0]['dtype'];
            $default_val    = $res[0]['column_default'];
        }
        return $this->process_data($column_name, $dtype, $value, $default_val);
    }

    public function process_data($column_name, $data_type, $value, $default_val)
    {
        $data_array = array('int', 'bigint',  'varchar','text' );
        /*if($column_name=='CreateDate'){
               echo '***'.var_dump($value);exit;
           } */
        if (in_array($data_type, $data_array)) {
            if ((empty($value) || $value===null || $value=='NULL')) {
                $value = null;
            } else {
                $value = addslashes($value);
            }
        }

        if ($data_type =='tinyint') {
            if (empty($value) || $value===null || !isset($value) || $value=='NULL'  || is_null($value)) {
                $value = 0;
            } elseif (!empty($default_val)) {
                $value = $default_val;
            } else {
                $value = intval($value);
            }
        }

        if ($data_type =='datetime') {
            if ((empty($value) || $value===null || $value==='NULL' || $value === '00:00:00')) {
                $value = null;
            } else {
                $value = date('Y-m-d H:i:s', strtotime($value));
            }
        }
        

        if ($data_type =='date') {
            if ((empty($value) || $value===null || $value=='NULL')) {
                $value = null;//$value = '1000-01-01';
            } else {
                $value = date('Y-m-d H:i:s', strtotime($value));
            }
        }

        if ($data_type =='text') {
            $value = addslashes($value);
        }
                
        return $value;
    }

    ## Function to create log if any error comes
    public function create_logs($table_name, $error, $line_no, $start_position)
    {
        $table_data             =  array();
        $table_data             =  new Cron_logs;
        $table_data->table_name = $table_name;
        $table_data->line_no    = $start_position+$line_no+1;
        $table_data->error      = $error;
        if ($table_data->save()) {
        }
    }

    ## Function to set last count no of cron
    public function set_cron_last_count($table_name, $unique_id, $last_count)
    {
        $last_count_arr         = array();
         
        $last_count_arr         = $this->get_cron_last_count($table_name, $unique_id);
        $last_countNo           = @count($last_count_arr[0]);
        if (!$last_countNo) {
            $table_data             =  array();
            $table_data             =  new Cron_last_count;
            $table_data->table_name = $table_name;
            $table_data->unique_id  = $unique_id;
            $table_data->last_count = $last_count;
            $table_data->save();
        } else {
            $table_data['table_name']  = $table_name;
            $table_data['unique_id']   = $unique_id;
            $table_data['last_count']  = $last_count;
            $res    = DB::table('cron_last_counts')->where(array('table_name'=>$table_name))->update($table_data);
        }
    }

    ## Function to get the last line inserted in Table
    public function get_cron_last_count($table_name, $unique_id)
    {
        $res = array();
        $res = Cron_last_count::where(array('table_name'=>$table_name,'unique_id'=>$unique_id))
                                -> orderBy('id', 'DESC')->take(1)->get()->toarray();
        /*
         if(@count($res)>0){
            $msg = $res;
        } */
        //echo '***'.$msg;exit;
        return  $res;
    }

    ## Function to get the last line inserted in Table
    public function check_id_exists_in_table($table_name, $field_name, $id, $model_name='')
    {
        $res = array();
        $data_res  = array();
        $res = DB::table($table_name)->select(DB::raw('count(1) as cnt'))
                    ->where(array($field_name=>$id))
                                -> get();
        if ($res) {
            $result=$res->toArray();
            $data = json_decode(json_encode($result), true);
            $data_res = $data[0]['cnt'];
        }
        return $data_res;
    }

    public function assignments_by_client($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data                          = array();
        
        if (!empty($column[0])) {
            DB::enableQueryLog();
            
            if ($command=='insert_data') {
                $table_data                             = new Assignment_Client;
                $table_data->assignment_type            = @!empty($column[0])?$column[0]:null;
                $table_data->repo_type                  = @!empty($column[1])?$column[1]:null;
                $table_data->make                       = @$column[2];
                $table_data->model                      = @$column[3] ;
                $table_data->year                       = @$column[4];
                $table_data->vin                        = @$column[5];
                $table_data->license_plate              = @$column[6];
                $table_data->vehicle_address            = addslashes(@$column[7]);
                $table_data->vehicle_apt_no             = @$column[8];
                $table_data->vehicle_city               = @$column[9];
                $table_data->vehicle_state              = @$column[10];
                $table_data->vehicle_zip                = @$column[11];
                $table_data->legal_sale_date            = @$column[12];
                $table_data->keys                       = @$column[13];
                $table_data->client_name                = @$column[14];
                $table_data->client_account_no          = @$column[15];
                $table_data->caprock_account            = @$column[16];
                $table_data->contract_date              = @$column[17];
                $table_data->original_loan_amount       = @$column[18];
                $table_data->first_name                 = @$column[19];
                $table_data->last_name                  = @$column[20];
                $table_data->ssn                        = @$column[21];
                $table_data->customer_address           = @$column[22];
                $table_data->customer_apt_no            = @$column[23];
                $table_data->customer_city              = @$column[24];
                $table_data->customer_state             = @$column[25];
                $table_data->customer_zip               = @$column[26];
                $table_data->cust_poe_company_name      = @$column[27];
                $table_data->cust_poe_company_address   = @$column[28];
                $table_data->cust_poe_suite_no          = @$column[29];
                $table_data->cust_poe_company_city      = @$column[30];
                $table_data->cust_poe_company_state     = @$column[31];
                $table_data->cust_poe_company_zip       = @$column[32];
                $table_data->assignment_notes           = @$column[33];
                $table_data->vehicle_notes_type         = @$column[34];
                $table_data->vehicle_notes              = @$column[35];
                $table_data->date_initial_assignment_received = @$column[36];
                $table_data->repo_status                = @$column[38];
                $table_data->cure_sent                  = @$column[39];
                $table_data->cure_expired               = @$column[40];
                $table_data->date_assigned_to_repo_agent = @$column[41];
                $table_data->recovery_date              = @$column[42];
                $table_data->NOI_mail_date              = @$column[42];
                $table_data->NOI_expire_date            = @$column[44];
                $table_data->repo_hold_reason           = @$column[45];
                $table_data->close_reason               = @$column[46];
                $table_data->close_date                 = @$column[47];
                $table_data->vehicle_driveable          = @$column[48];
                $table_data->storage_address            = @$column[49];
                $table_data->storage_apt_no             = null;
                $table_data->storage_city               = null;
                $table_data->storage_state              = null;
                $table_data->storage_zip                = null;
                $table_data->auction_status             = @$column[50];
                $table_data->auction                    = @$column[51];
                $table_data->auction_address            = @$column[52];
                $table_data->auction_title_status       = @$column[53];
                $table_data->secured_date               = @$column[54];
                $table_data->condition_report_link      = @$column[55];
                $table_data->Mileage                    = @$column[56];
                $table_data->Grade                      = @$column[57];
                $table_data->MMR                        = @$column[58];
                $table_data->floor_price                = @$column[59];
                $table_data->sales_price                = @$column[60];
                $table_data->days_run                   = @$column[61];
                $table_data->hold_date                  = @$column[62];
                $table_data->sale_hold_reason           = @$column[63];
                $table_data->scheduled_sale_date        = @$column[64];
                $table_data->total_charges              = @$column[65];
                $table_data->sold_date                  = @$column[66];
                $table_data->report_information         = null;
                
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $q = DB::getQueryLog() ;
                    //echo '<pre>';print_r($q);exit;
                    $error = $ex->getMessage();
                    //echo '<pre>';print_r($error);exit;
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } 
        }
    }

    ## Schedule for sale start
    public function schedule_for_sale($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data                          = array();
        
        if (!empty($column[0])) {
            DB::enableQueryLog();
            
            if ($command=='insert_data') {
                $table_data                             = new Schedule_for_sale;
                $table_data->Auction                    = @!empty($column[0])?$column[0]:null;
                $table_data->Run                        = @!empty($column[1])?$column[1]:null;
                $table_data->Photo                      = @$column[2];
                $table_data->Cn_Acct_no                 = @$column[3] ;
                $table_data->Description                = @$column[4];
                $table_data->Mileage                    = @$column[5];
                $table_data->Vehicle_Grade              = @$column[6];
                $table_data->Floor_Price_Requested      = addslashes(@$column[7]);
                $table_data->Floor_Price_Accepted       = @$column[8];
                $table_data->Auction_Age                = @$column[9];
                $table_data->Scheduled_Sale_Date        = @$column[10];
                $table_data->On_Block                   = @$column[11];
                $table_data->Title_Received             = @$column[12];
                $table_data->Pricing_Info               = @$column[13];
                $table_data->Legal_Sale_Date            = @$column[14];
                $table_data->Secondary_Client           = @$column[15];
                $table_data->Summary                    = @$column[16]; 
                
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $q = DB::getQueryLog() ;
                    //echo '<pre>';print_r($q);exit;
                    $error = $ex->getMessage();
                    //echo '<pre>';print_r($error);exit;
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            }  
        }
    }
    ## Schedule for sale end


    ## Secured last day start

    public function secured_last_day($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data                          = array();
        
        if (!empty($column[0])) {
            DB::enableQueryLog();
            
            if ($command=='insert_data') {
                $table_data                             = new Secured_last_day;
                $table_data->Auction                    = @!empty($column[0])?$column[0]:null;
                $table_data->Year                       = @!empty($column[1])?$column[1]:null;
                $table_data->Make                       = @$column[2];
                $table_data->Model                      = @$column[3] ;
                $table_data->VIN                        = @$column[4];
                $table_data->Cn_Acc_no                  = @$column[5];
                $table_data->Mileage                    = @$column[6];
                $table_data->Assign_Accept_to_Secured   = addslashes(@$column[7]);
                $table_data->Secured_Date               = @$column[8];
                $table_data->Status                     = @$column[9];
                $table_data->Title_Status               = @$column[10];
                $table_data->Title_Received             = @$column[11];
                $table_data->Chronology                 = @$column[12];
                $table_data->Add_Note                   = @$column[13];
                $table_data->Summary                    = @$column[14];
                $table_data->Place_Hold                 = @$column[15]; 
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $q = DB::getQueryLog() ;
                    //echo '<pre>';print_r($q);exit;
                    $error = $ex->getMessage();
                    //echo '<pre>';print_r($error);exit;
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            }  
        }
    }
    ## Secured last day end

}
