<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use App\Mail\WelcomeEmail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $error;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $error)
    {
        $this->error = $error;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $data = array();
        $data['error'] = $this->error;
        Mail::to("bhuvan@restolabs.com")
     //   Mail::to(config('constants.REPO_EMAIL'))
         //   ->cc(config('constants.REMARKETING_EMAIL'))
         //   ->cc(config('constants.DEV_EMAIL'))
            ->send(new WelcomeEmail($data));
    }
}
