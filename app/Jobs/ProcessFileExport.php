<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\FileUpload;
use App\TblAccount;
use App\Space;
use App\TblCustomerSpace;
use App\TblNote;
use App\Contact;
use App\TblVehicle;
use App\Cron_logs;
use App\Cron_last_count;
use App\Assignment_Client;

class ProcessFileExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FileUpload $file_data)
    {
        $this->file_data = $file_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->file_data;
        $base_file_path = public_path('/files');
        $file_path 		= $base_file_path . '/' . $file->file_name;
        // $csv_files_array = array(
        //                     'tbl_accounts'			=>'tblaccount.csv',
        //                     'contact'			    =>'tblcontact.csv',
        //                     'tbl_notes'             => 'tblnotes.csv' ,
        //                     'tbl_unitactiveaccount' => 'tblunitactiveaccount.csv',
        //                     'spaces_covered' 		=> 'spacescovered.csv',
        //                     'tbl_address' 		    => 'tbladdress.csv',
        //                     'tbl_outside_accounts'	=> 'tbloutsideaccount.csv',
        //                     'get_activities' 		=> 'gateactivit.csv',
        //                     'tbl_audits' 			=> 'tblaudit.csv',
        //                 );

        $limit			 	= 20000;
        $table_unique_id 	= array(
            'tbl_notes'             => 'noteID',
            'tbl_accounts'          => 'AcctID',
            'contacts'              => 'ConID',
            'tbl_vehicles'          => 'VehID',
            'tbl_unitactiveaccount' => 'id',
            'spaces_covered' 		=> 'CoveredSpaceID',
            'tbl_address' 			=> 'AcctID',
            'tbl_outside_accounts' 	=> 'MailingListID',
            'get_activities' 		=> 'GateEntryID',
            'tbl_audits' 			=> 'AuditID',
            'spaces'                => 'id',
            'tbl_customer_spaces'     => 'id'
        );
        $table_name = strstr($file->file_org_name, '.', true);
        $unique_id  = $table_unique_id[$table_name];

        $this->import_data($file_path, $table_name, $limit, $unique_id);
    }
    public function import_data($file_path, $table_name, $limit, $unique_id)
    {
        $message 			= 'Cron Executed';
        $db 				= DB::connection()->getDatabaseName();
        //echo '***'.$db;exit;
        $file 				= fopen($file_path, "r");
        $start_position_arr = $this->get_cron_last_count($table_name, $unique_id);
        $start_position 	= 0;
        //echo '***'.$start_position_arr;exit;
        if (@count($start_position_arr)>0) { //echo 'kamal';exit;
            $start_position = $start_position_arr[0]['last_count'];
        }
         
        $i 					= 0;
        $count 				= 0;
        $limit_count 		= 0;
        $command			= 'insert_data';
        $row_id				= 0;
        ## get the tables column so that data need to be inserted correct
        $table_column_count = array(
            'tbl_accounts'=>43,
            'contacts'=>28,
            'tbl_vehicles'=>40
        );
         
        while (($column 		= fgetcsv($file, 100000, ","))!== false) {
            //echo '<pre>';print_r($column); exit;
            ## This is becouse first line containing column name
            if ($i==0) {
                $i++;
                continue;
            }## This is becouse first line containing column name
            if ($count < $start_position) {
                $count++;
                continue;
            }
            
            $count++;
            $limit_count++;
            if ($limit_count > $limit) {
                break;
            }
 
            $check_columns = $this->check_columns($table_name, $db);
            //echo '<pre>';print_r($check_columns);exit;
            ## Check Command whether records would be updated or inserted, checked by ID

            //echo '***'.$column[0];exit;

            if (is_numeric($column[0]) && $column[0]) {
                $row_exists = $this->check_row_exists($column[0], $table_name, $db, $unique_id);
                if (!empty($row_exists)) {
                    $command 	= 'update_data';
                    $row_id 	= $column[0];
                }
            }
            ## Check Command whether records would be updated or inserted, checked by ID


            //echo '***'.$check_columns;exit;
            //echo is_numeric($column[0]).'=='.$table_column_count[$table_name];exit; echo '***=='.$count;
            //---------------- Insert Data start ------------------------------//
            if (is_numeric($column[0]) && $check_columns==$table_column_count[$table_name]) {
                ## Calling function to execute
                $this->$table_name($column, $table_name, $db, $limit_count, $command, $row_id, $start_position);
            } else {
                try {
                    $this->$table_name($column, $table_name, $db, $limit_count, $command, $row_id, $start_position);
                } catch (\Illuminate\Database\QueryException $ex) {
                    // echo '***'.($ex->getMessage()); exit;
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
            }
            //----------------------- Insert data end ------------------------//
        }
 
        //reached the end, reset to 0 // Then Set its last count to zero so that it runs from start
        if ($count  == $start_position) {
            $count =  0;
            $start_position=0;
            $table_data['last_count']  = 0;
            $res 	= DB::table('cron_last_counts')->where(array('table_name'=>$table_name))->update($table_data);
        }
 
        ## Insert last count in the table
        // echo $limit_count.'***>>='		.$count .'== >>'.$start_position.'==='.$limit;exit;
        if ($count == ($start_position+$limit+1)) {
            $last_count = $start_position+$limit;
            $this->set_cron_last_count($table_name, $unique_id, $last_count);
        }
        ## Insert last count in the table
        return $message;
    }
    
    public function check_columns($table_name, $db)
    {
        $data = 0;
        $count = DB::select(DB::raw("select COUNT(*) as cnt FROM INFORMATION_SCHEMA.COLUMNS where table_schema ='{$db}' and table_name = '{$table_name}'"));
        $res = json_decode(json_encode($count), true);

        if (count($res)>0) {
            $data = $res[0]['cnt'];
        }
        return $data;
    }

     
    public function check_row_exists($columnID, $table_name, $db, $unique_id)
    {
        $data = 0;
        $field = $unique_id;
        $count = DB::select(DB::raw("select COUNT(1) as cnt FROM {$table_name} 
		 	where {$field} = {$columnID}"));
         
        
        if ($count) {
            $rs = json_decode(json_encode($count), true);
            $data = @$rs[0]['cnt'];
        }
        return $data;
    }


    public function get_data_type($column_name, $table_name, $db)
    {
        $data_var = '';
        return $data_var = 	DB::select(DB::raw(
            "SELECT DATA_TYPE as dtype , column_default FROM INFORMATION_SCHEMA.COLUMNS 
							 WHERE TABLE_SCHEMA = '{$db}' AND TABLE_NAME = '{$table_name}' 
							 and COLUMN_name= '{$column_name}'"
        ));
    }

    public function clean_data($column_name, $value, $table_name, $db)
    {
        $data_type = $this->get_data_type($column_name, $table_name, $db);

        $res = json_decode(json_encode($data_type), true);
        if (count($res)>0) {
            $dtype		= $res[0]['dtype'];
            $default_val	= $res[0]['column_default'];
        }
        return $this->process_data($column_name, $dtype, $value, $default_val);
    }

    public function process_data($column_name, $data_type, $value, $default_val)
    {
        $data_array = array('int', 'bigint',  'varchar','text' );
        /*if($column_name=='CreateDate'){
               echo '***'.var_dump($value);exit;
           } */
        if (in_array($data_type, $data_array)) {
            if ((empty($value) || $value===null || $value=='NULL')) {
                $value = null;
            } else {
                $value = addslashes($value);
            }
        }

        if ($data_type =='tinyint') {
            if (empty($value) || $value===null || !isset($value) || $value=='NULL'  || is_null($value)) {
                $value = 0;
            } elseif (!empty($default_val)) {
                $value = $default_val;
            } else {
                $value = intval($value);
            }
        }

        if ($data_type =='datetime') {
            if ((empty($value) || $value===null || $value==='NULL' || $value === '00:00:00')) {
                $value = null;
            } else {
                $value = date('Y-m-d H:i:s', strtotime($value));
            }
        }
        

        if ($data_type =='date') {
            if ((empty($value) || $value===null || $value=='NULL')) {
                $value = null;//$value = '1000-01-01';
            } else {
                $value = date('Y-m-d H:i:s', strtotime($value));
            }
        }

        if ($data_type =='text') {
            $value = addslashes($value);
        }
                
        return $value;
    }

    ## Function to create log if any error comes
    public function create_logs($table_name, $error, $line_no, $start_position)
    {
        $table_data 			=  array();
        $table_data 			=  new Cron_logs;
        $table_data->table_name = $table_name;
        $table_data->line_no 	= $start_position+$line_no+1;
        $table_data->error 		= $error;
        if ($table_data->save()) {
        }
    }

    ## Function to set last count no of cron
    public function set_cron_last_count($table_name, $unique_id, $last_count)
    {
        $last_count_arr 		= array();
         
        $last_count_arr 		= $this->get_cron_last_count($table_name, $unique_id);
        $last_countNo 			= @count($last_count_arr[0]);
        if (!$last_countNo) {
            $table_data 			=  array();
            $table_data 			=  new Cron_last_count;
            $table_data->table_name = $table_name;
            $table_data->unique_id 	= $unique_id;
            $table_data->last_count = $last_count;
            $table_data->save();
        } else {
            $table_data['table_name']  = $table_name;
            $table_data['unique_id']   = $unique_id;
            $table_data['last_count']  = $last_count;
            $res 	= DB::table('cron_last_counts')->where(array('table_name'=>$table_name))->update($table_data);
        }
    }

    ## Function to get the last line inserted in Table
    public function get_cron_last_count($table_name, $unique_id)
    {
        $res = array();
        $res = Cron_last_count::where(array('table_name'=>$table_name,'unique_id'=>$unique_id))
                                -> orderBy('id', 'DESC')->take(1)->get()->toarray();
        /*
         if(@count($res)>0){
            $msg = $res;
        } */
        //echo '***'.$msg;exit;
        return  $res;
    }

    ## Function to get the last line inserted in Table
    public function check_id_exists_in_table($table_name, $field_name, $id, $model_name='')
    {
        $res = array();
        $data_res  = array();
        $res = DB::table($table_name)->select(DB::raw('count(1) as cnt'))
                    ->where(array($field_name=>$id))
                                -> get();
        if ($res) {
            $result=$res->toArray();
            $data = json_decode(json_encode($result), true);
            $data_res = $data[0]['cnt'];
        }
        return $data_res;
    }

    public function tbl_accounts($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 = array();
        //$SignedContract = !empty($column[24])?1:0;
        
        //$NonCompliance = !empty($column[29])?1:0;
        //$Summons = !empty($column[22])?1:0;
        //$BillTo = !empty($column[15])?1:0;
         
     
        if (!empty($column[0])) {
            DB::enableQueryLog();
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 					 = new TblAccount;
                $table_data->AcctID 			 = $column[0];
                $table_data->CurrentAcct 		 = @!empty($column[1])?$column[1]:null ;
                $table_data->scAddress 			 = addslashes(@$column[2]);
                $table_data->scCity 			 = @$column[3] ;
                $table_data->scState 		     = @$column[4];
                $table_data->scZip 				 = @$column[5];
                $table_data->community_name 	 = '';
                $table_data->scCountry 			 = @$column[7];
                $table_data->scPhone 			 = @$column[8];
                $table_data->oAddress 			 = @$column[9];
                $table_data->oCity 				 = @$column[10];
                $table_data->oState 			 = @$column[11];
                $table_data->oZip 				 = @$column[12];
                $table_data->oCountry 			 = @$column[13];
                $table_data->oPhone 			 = @$column[14];
                $table_data->BillTo 			 = @$column[15];
                $table_data->ClubContact 		 = @$column[16];
                $table_data->Notes 				 = $this->clean_data('Notes', @$column[17], $table_name, $db);
                $table_data->CreateDate 		 = $this->clean_data('CreateDate', @$column[18], $table_name, $db);
                $table_data->CreatedBy 			 = $this->clean_data('CreatedBy', @$column[19], $table_name, $db);
                $table_data->LastUpdate 		 = $this->clean_data('LastUpdate', @$column[20], $table_name, $db);
                $table_data->LastUpdatedBy 		 = $this->clean_data('LastUpdatedBy', @$column[21], $table_name, $db);
                $table_data->Summons 			 = @$column[22];
                $table_data->AuthLtr 			 = $this->clean_data('authltr', @$column[23], $table_name, $db);
                $table_data->SignedContract 	 = @$column[24];
                $table_data->ConvertDate 		 = $this->clean_data('ConvertDate', @$column[25], $table_name, $db);
                $table_data->AccountWaitListed 	 = @$column[26];
                $table_data->WaitLtr 			 = $this->clean_data('WaitLtr', @$column[27], $table_name, $db);
                $table_data->Annotated 			 = @$column[28];
                $table_data->NonCompliance 		 = @$column[29];
                $table_data->CellPhone 			 = $this->clean_data('CellPhone', @$column[30], $table_name, $db);
                $table_data->EMail 				 = $this->clean_data('EMail', @$column[31], $table_name, $db);
                $table_data->RenewalStatus 		 = @$column[32];
                $table_data->care_of 			 = '';
                $table_data->emergency_contact_1 = '';
                $table_data->emergency_contact_2 = '';
                $table_data->insurance_current 	 = 0;
                $table_data->bill_paid 			 = 0;
                $table_data->registration_current= 0;
                $table_data->contact_name1 		 = '';
                $table_data->contact_name2 		 = '';
                $table_data->relation1 			 = '';
                $table_data->relation2 			 = '';
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $q = DB::getQueryLog() ;
                    //echo '<pre>';print_r($q);exit;
                    $error = $ex->getMessage();
                    //echo '<pre>';print_r($error);exit;
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                
                try {
                    $table_data['CurrentAcct']   	= @$column[1];
                    $table_data['scAddress']        = @$column[2];
                    $table_data['scCity']           = @$column[3];
                    $table_data['scState']          = @$column[4];
                    $table_data['scZip']            = @$column[5];
                    $table_data['scCountry']        = @$column[6];
                    $table_data['scPhone']          = @$column[7];
                    $table_data['oAddress']         = @$column[8];
                    $table_data['oCity']            = @$column[9];

                    $table_data['oState']           = @$column[10];
                    $table_data['oZip']             = @$column[11];
                    $table_data['oCountry']         = @$column[12];
                    $table_data['oPhone']           = @$column[13];
                    $table_data['BillTo']           = $BillTo;

                    $table_data['ClubContact']      = $this->clean_data('ClubContact', '', $table_name, $db);
                    $table_data['Notes']            = $this->clean_data('Notes', @$column[16], $table_name, $db);
                    $table_data['CreateDate']       = $this->clean_data('CreateDate', @$column[17], $table_name, $db);
                    $table_data['CreatedBy']        = $this->clean_data('CreatedBy', @$column[18], $table_name, $db);
                    $table_data['LastUpdate']       = $this->clean_data('LastUpdate', @$column[19], $table_name, $db);


                    $table_data['LastUpdatedBy']    = $this->clean_data('LastUpdatedBy', @$column[20], $table_name, $db);
                    $table_data['Summons']          = $Summons;
                    $table_data['AuthLtr']          = $AuthLtr;
                    $table_data['SignedContract']   = $SignedContract;
                    $table_data['ConvertDate']      = null;
                    $table_data['AccountWaitListed']=$this->clean_data('AccountWaitListed', @$column[25], $table_name, $db);
     
                    $table_data['WaitLtr']      	= $this->clean_data('WaitLtr', @$column[26], $table_name, $db);
                    $table_data['Annotated']        = $this->clean_data('Annotated', '', $table_name, $db);
                    $table_data['NonCompliance']    = $NonCompliance;
                    $table_data['CellPhone']        = $this->clean_data('CellPhone', @$column[29], $table_name, $db);
                    $table_data['EMail']       		= $this->clean_data('EMail', @$column[30], $table_name, $db);
                    $table_data['RenewalStatus']    = $this->clean_data('RenewalStatus', @$column[31], $table_name, $db);


                    $table_data['insurance_current']=$this->clean_data('insurance_current', '', $table_name, $db);
                    $table_data['bill_paid']        = $this->clean_data('bill_paid', '', $table_name, $db);
                    $table_data['registration_current']= $this->clean_data('registration_current', '', $table_name, $db);
                    $table_data['care_of']        	= $this->clean_data('care_of', '', $table_name, $db);
                    $table_data['emergency_contact_1']= $this->clean_data('emergency_contact_1', '', $table_name, $db);
                    $table_data['emergency_contact_2']= $this->clean_data('emergency_contact_2', '', $table_name, $db);
                    $table_data['contact_name1']	= $this->clean_data('contact_name1', '', $table_name, $db);
                    $table_data['contact_name2']    = $this->clean_data('contact_name2', '', $table_name, $db);
                    $table_data['relation1']		= $this->clean_data('relation1', '', $table_name, $db);
                    $table_data['relation2']		= $this->clean_data('relation2', '', $table_name, $db);
                    $res 	= DB::table('tbl_accounts')->where(array('AcctID'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }

    
    public function tbl_notes($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 = array();

        if (!empty($column[0])) {
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 					= new TblNote;
                $table_data->noteID 			= @$column[0];
                $table_data->noteConID 		 	= @$column[1];
                $table_data->noteAcctID 		= @$column[2];
                $table_data->noteUser 			= @$column[3];
                $table_data->noteTime 			= @$column[4];
                $table_data->noteText 			= @$column[5];
                $table_data->noteVendorID		= @$column[6];
                $table_data->noteType 			= @$column[7];
                
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                try {
                    $table_data['noteID']			= @$column[0];
                    $table_data['noteConID'] 		= @$column[1];
                    $table_data['noteAcctID'] 		= @$column[2];
                    $table_data['noteUser'] 		= @$column[3];
                    $table_data['noteTime'] 		= @$column[4];
                    $table_data['noteText'] 		= @$column[5];
                    $table_data['noteVendorID']		= @$column[6];
                    $table_data['noteType'] 		= @$column[7];
                    $res 	= DB::table('tbl_notes')->where(array('noteID'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }
    
    public function contacts($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 	= array();

        if (!empty($column[0])) {
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 				= new Contact;
                $table_data->ConID 			= $column[0];
                $table_data->AcctID 		= @!empty($column[1])?$column[1]:null ;
                $table_data->RecID 			= @$column[2];
                $table_data->RecExpDate 	= $this->clean_data('RecExpDate', @$column[3], $table_name, $db);
                $table_data->HomeOwner 		= @$column[4] ;
                $table_data->Resident 		= @$column[5];
                $table_data->auth_user 		= 0;

                $table_data->is_primary_cont= 0;

                $table_data->billable_contact= 0;
                $table_data->Prefix 		= $this->clean_data('Prefix', @$column[9], $table_name, $db);
                $table_data->Fname 			= $this->clean_data('Fname', @$column[10], $table_name, $db);
                $table_data->Mname 			= $this->clean_data('Mname', @$column[11], $table_name, $db);
                $table_data->LName 			= $this->clean_data('LName', @$column[12], $table_name, $db);
                $table_data->address 		= '';
                $table_data->city 			= '';
                $table_data->state 			= '';
                $table_data->zip 			= '';
                $table_data->phone 		 	= '';
                $table_data->cell 			= '';
                $table_data->email 			= '';
                $table_data->notes 			= '';
                $table_data->Photo 			= $this->clean_data('Photo', @$column[21], $table_name, $db);
                $table_data->CreateDate 	= $this->clean_data('CreateDate', @$column[22], $table_name, $db);
                $table_data->CreatedBy 		= $this->clean_data('CreatedBy', @$column[23], $table_name, $db);
                $table_data->LastUpdate 	= $this->clean_data('LastUpdate', @$column[24], $table_name, $db);
                $table_data->LastUpdatedBy 	= $this->clean_data('LastUpdatedBy', @$column[25], $table_name, $db);
                $table_data->GuestCount 	=$this->clean_data('GuestCount', @$column[26], $table_name, $db);
                $table_data->status 		= 1;
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                
                try {
                    $table_data['AcctID']   	= @$column[1];
                    $table_data['RecID']    	= @$column[2];
                    $table_data['RecExpDate']   = @$column[3];
                    $table_data['HomeOwner']    = @$column[4];
                    $table_data['Resident']     = @$column[5];
                    $table_data['auth_user']    = @$column[6];
                    $table_data['is_primary_cont']= @$column[7];
                    $table_data['billable_contact']= @$column[8];
                    $table_data['Prefix']       =$this->clean_data('Prefix', @$column[9], $table_name, $db);
                    $table_data['Fname']        = $this->clean_data('Fname', @$column[10], $table_name, $db);
                    $table_data['Mname']        = $this->clean_data('Mname', @$column[11], $table_name, $db);
                    $table_data['LName']        = $this->clean_data('LName', @$column[12], $table_name, $db);
                    $table_data['address']      = $this->clean_data('address', @$column[13], $table_name, $db);
                    $table_data['city']    		= $this->clean_data('city', @$column[14], $table_name, $db);
                    $table_data['state']        = $this->clean_data('state', @$column[15], $table_name, $db);
                    $table_data['zip']      	= $this->clean_data('zip', @$column[16], $table_name, $db);
                    $table_data['phone']        = $this->clean_data('phone', @$column[17], $table_name, $db);
                    $table_data['cell']     	= $this->clean_data('cell', @$column[18], $table_name, $db);
                    $table_data['email']   		= $this->clean_data('email', @$column[19], $table_name, $db);
                    $table_data['notes']    	= $this->clean_data('notes', @$column[20], $table_name, $db);
                    $table_data['Photo']   		= $this->clean_data('Photo', @$column[21], $table_name, $db);
                    $table_data['CreateDate'] 	= $this->clean_data('CreateDate', @$column[22], $table_name, $db);
                    $table_data['CreatedBy']    = $this->clean_data('CreatedBy', @$column[23], $table_name, $db);
                    $table_data['LastUpdate']   =$this->clean_data('LastUpdate', @$column[25], $table_name, $db);
                    $table_data['LastUpdatedBy']= $this->clean_data('LastUpdatedBy', @$column[25], $table_name, $db);
                    $table_data['GuestCount']   = $this->clean_data('GuestCount', @$column[26], $table_name, $db);
                    $table_data['status']    	= $this->clean_data('status', @$column[27], $table_name, $db);
                 
                    $res 	= DB::table('contacts')->where(array('VehID'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }
        
    public function tbl_vehicles($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 	= array();

        if (!empty($column[0])) {
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 				= new TblVehicle;
                $table_data->VehID 			= $column[0];
                $table_data->AcctID 		= @!empty($column[1])?$column[1]:null ;
                $table_data->ClassCode 		=  (@$column[2]);
                $table_data->vMake 			= @$column[3] ;
                $table_data->vModel 		= @$column[4] ;
                $table_data->vYear 			= @$column[5];
                $table_data->vState 		= @$column[6];
                $table_data->VPlate 		= @$column[7];
                $table_data->VIN 			= $this->clean_data('VIN', @$column[8], $table_name, $db);
                $table_data->VLength 		= $this->clean_data('VLength', @$column[9], $table_name, $db);
                $table_data->LHName 		= $this->clean_data('LHName', @$column[10], $table_name, $db);
                $table_data->LHAddress 		= $this->clean_data('LHAddress', @$column[11], $table_name, $db);
                $table_data->LHCity 		= $this->clean_data('LHCity', @$column[12], $table_name, $db);
                $table_data->LHState 		= $this->clean_data('LHState', @$column[13], $table_name, $db);
                $table_data->LHCountry 		= $this->clean_data('LHCountry', @$column[14], $table_name, $db);
                $table_data->LHZIp 			= $this->clean_data('LHZIp', @$column[15], $table_name, $db);
                $table_data->LHPhone 		= $this->clean_data('LHPhone', @$column[16], $table_name, $db);
                $table_data->LHFAX 		 	= $this->clean_data('LHFAX', @$column[17], $table_name, $db);
                $table_data->AmorDate 		= $this->clean_data('AmorDate', @$column[18], $table_name, $db);
                $table_data->CreateDate 	= $this->clean_data('CreateDate', @$column[19], $table_name, $db);
                $table_data->CreatedBy 		= $this->clean_data('CreatedBy', @$column[20], $table_name, $db);
                $table_data->LastUpdate 	= $this->clean_data('LastUpdate', @$column[21], $table_name, $db);
                $table_data->LastUpdatedBy 	= $this->clean_data('LastUpdatedBy', @$column[22], $table_name, $db);
                $table_data->Leased 		= $this->clean_data('Leased', @$column[23], $table_name, $db);
                $table_data->VHullNo 		= $this->clean_data('VHullNo', @$column[24], $table_name, $db);
                $table_data->WaitingForSpace=$this->clean_data('WaitingForSpace', @$column[25], $table_name, $db);
                $table_data->WaitListDate 	= $this->clean_data('WaitListDate', @$column[26], $table_name, $db);
                $table_data->RequestedLength=$this->clean_data('RequestedLength', @$column[27], $table_name, $db);
                $table_data->NonComplianceDate=$this->clean_data('NonComplianceDate', @$column[28], $table_name, $db);
                $table_data->NonCompliance 	=$this->clean_data('NonCompliance', @$column[29], $table_name, $db);
                ;
                $table_data->BoLicNo 		= $this->clean_data('BoLicNo', @$column[30], $table_name, $db);
                $table_data->InsuranceCo 	= $this->clean_data('InsuranceCo', @$column[31], $table_name, $db);
                $table_data->PolicyNo 		= $this->clean_data('PolicyNo', @$column[32], $table_name, $db);
                $table_data->UnitNo 		= $this->clean_data('UnitNo', @$column[33], $table_name, $db);
                $table_data->registration_type= @!empty($column[34])?$this->clean_data('registration_type', @$column[34], $table_name, $db): 0;
                $table_data->registration_exp_at= @!empty($column[35])?$this->clean_data('registration_exp_at', @$column[35], $table_name, $db):null;
                $table_data->insurance_exp_at= @!empty($column[36])?$this->clean_data('insurance_exp_at', @$column[36], $table_name, $db):null;
                $table_data->report_me 		= @!empty($column[37])?$this->clean_data('report_me', @$column[37], $table_name, $db):0;
                $table_data->notes 			= @!empty($column[38])?$this->clean_data('notes', @$column[38], $table_name, $db):null;
                $table_data->status 		= @!empty($column[39])?$this->clean_data('status', @$column[39], $table_name, $db):1;
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                
                try {
                    $table_data['AcctID']   	= @$column[1];
                    $table_data['ClassCode']        = @$column[2];
                    $table_data['vMake']           = @$column[3];
                    $table_data['vModel']          = @$column[4];
                    $table_data['vYear']            = @$column[5];
                    $table_data['vState']        = @$column[6];
                    $table_data['VPlate']          = @$column[7];
                    $table_data['VIN']         = @$column[8];
                 

                    $table_data['VLength']           =$this->clean_data('VLength', @$column[9], $table_name, $db);
                    $table_data['LHName']             = $this->clean_data('LHName', @$column[10], $table_name, $db);
                    $table_data['LHAddress']         = $this->clean_data('LHAddress', @$column[11], $table_name, $db);
                    $table_data['LHCity']           = $this->clean_data('LHCity', @$column[12], $table_name, $db);
                    $table_data['LHState']           = $this->clean_data('LHState', @$column[13], $table_name, $db);

                    $table_data['LHCountry']      = $this->clean_data('LHCountry', @$column[14], $table_name, $db);
                    $table_data['LHZIp']            = $this->clean_data('LHZIp', @$column[15], $table_name, $db);
                    $table_data['LHPhone']       = $this->clean_data('LHPhone', @$column[16], $table_name, $db);
                    $table_data['LHFAX']        = $this->clean_data('LHFAX', @$column[17], $table_name, $db);
                    $table_data['AmorDate']       = $this->clean_data('AmorDate', @$column[18], $table_name, $db);
                    $table_data['CreateDate']    = $this->clean_data('CreateDate', @$column[19], $table_name, $db);
                    $table_data['CreatedBy']          = $this->clean_data('CreatedBy', @$column[20], $table_name, $db);
                    $table_data['LastUpdate']          = $this->clean_data('LastUpdate', @$column[21], $table_name, $db);
                    $table_data['LastUpdatedBy']   = $this->clean_data('LastUpdatedBy', @$column[22], $table_name, $db);
                    $table_data['Leased']      = $this->clean_data('Leased', @$column[23], $table_name, $db);
                    $table_data['VHullNo']=$this->clean_data('VHullNo', @$column[25], $table_name, $db);
     
                    $table_data['WaitingForSpace']      	= $this->clean_data('WaitingForSpace', @$column[25], $table_name, $db);
                    $table_data['WaitListDate']        = $this->clean_data('WaitListDate', @$column[26], $table_name, $db);
                    $table_data['RequestedLength']    = $this->clean_data('RequestedLength', @$column[27], $table_name, $db);
                    $table_data['NonComplianceDate']        = $this->clean_data('NonComplianceDate', @$column[28], $table_name, $db);
                    $table_data['NonCompliance']       		= $this->clean_data('NonCompliance', @$column[29], $table_name, $db);
                    $table_data['BoLicNo']    = $this->clean_data('BoLicNo', @$column[30], $table_name, $db);


                    $table_data['InsuranceCo']=$this->clean_data('InsuranceCo', @$column[31], $table_name, $db);
                    $table_data['PolicyNo']        = $this->clean_data('PolicyNo', @$column[32], $table_name, $db);
                    $table_data['UnitNo']= $this->clean_data('UnitNo', @$column[33], $table_name, $db);
                    $table_data['registration_type']        	= $this->clean_data('registration_type', @$column[34], $table_name, $db);
                    $table_data['registration_exp_at']= $this->clean_data('registration_exp_at', @$column[35], $table_name, $db);
                    $table_data['insurance_exp_at']= $this->clean_data('insurance_exp_at', @$column[36], $table_name, $db);
                    $table_data['report_me']	= $this->clean_data('report_me', @$column[37], $table_name, $db);
                    $table_data['notes']    = $this->clean_data('notes', @$column[38], $table_name, $db);
                    $table_data['status']		= $this->clean_data('status', @$column[39], $table_name, $db);
                 
                    $res 	= DB::table('tbl_vehicles')->where(array('VehID'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }

    public function spaces($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 = array();

        if (!empty($column[0])) {
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 					= new Space;
                $table_data->SpaceNum 			= @$column[0];
                $table_data->Length 		 	= @$column[1];
                $table_data->PullthruNum 		= @$column[2];
                $table_data->TempOnly 			= @$column[3];
                $table_data->Covered 			= @$column[4];
                $table_data->TimeStamp 			= @$column[5];
                $table_data->defaultPullthru	= !empty(@$column[2]) && @$column[2] != 'NULL' ? '1' : '0';
                $table_data->isPullthru 		= !empty(@$column[2]) && @$column[2] != 'NULL' ? '1' : '0';
                $table_data->status             = 'active';
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                try {
                    $table_data['SpaceNum']			= @$column[0];
                    $table_data['Length'] 		= @$column[1];
                    $table_data['PullthruNum'] 		= @$column[2];
                    $table_data['TempOnly'] 		= @$column[3];
                    $table_data['Covered'] 		= @$column[4];
                    $table_data['TimeStamp'] 		= @$column[5];
                    
                    $res 	= DB::table('spaces')->where(array('id'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }

    public function tbl_customer_spaces($column, $table_name, $db, $limit_count, $command='insert_data', $row_id='', $start_position=0)
    {
        $table_data 					 	 = array();

        if (!empty($column[0])) {
            //echo '<pre>';print_r($column);exit;
            if ($command=='insert_data') {
                $table_data 				= new TblCustomerSpace;
                $table_data->SpaceNum 		= @$column[0];
                $table_data->VehID 		 	= @$column[1];
                $table_data->AcctID 		= @$column[2];
                $table_data->Term 			= @$column[3];
                
                ###### Saving Error Log if error comes #########
                try {
                    $table_data->save();
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);
                }
                ###### Saving Error Log if error comes #########
            } else { //echo '<pre>';print_r($column);exit;
                try {
                    $table_data['SpaceNum']		= @$column[0];
                    $table_data['VehID'] 		= @$column[1];
                    $table_data['AcctID'] 		= @$column[2];
                    $table_data['Term'] 		= @$column[3];
                    
                    
                    $res 	= DB::table('tbl_customer_spaces')->where(array('id'=>$row_id))->update($table_data);
                } catch (\Illuminate\Database\QueryException $ex) {
                    $error = $ex->getMessage();
                    $this->create_logs($table_name, $error, $limit_count, $start_position);//exit;
                }
            }
        }
    }
}
