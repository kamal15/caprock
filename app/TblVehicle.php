<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TblVehicle extends Model
{
    protected $table = 'tbl_vehicles';
    public $timestamps = false;
    protected $fillable = ['*'];
    protected $primaryKey  = 'VehID';
}
