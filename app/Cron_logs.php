<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cron_logs extends Model
{
    protected $table = 'cron_logs';
    public $timestamps = false;
}
