<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = ['assignment_type', 'repo_type', 'make', 'model', 'year', 'vin', 'license_plate', 'vehicle_address', 'vehicle_apt_no', 'vehicle_city', 'vehicle_state', 'vehicle_zip', 'legal_sale_date', 'keys', 'client_name', 'client_account_no', 'contract_date', 'original_loan_amount', 'client_queue_no', 'first_name', 'last_name', 'ssn', 'customer_address', 'customer_apt_no', 'customer_city', 'customer_state', 'customer_zip', 'user_name', 'cust_poe_company_name', 'cust_poe_company_address', 'cust_poe_suite_no', 'cust_poe_company_city', 'cust_poe_company_state', 'cust_poe_company_zip', 'vehicle_notes_type', 'vehicle_notes', 'date_initial_assignment_received', 'status', 'created_by', 'last_modified_by'];
}
