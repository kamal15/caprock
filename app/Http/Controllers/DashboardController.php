<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Charts;
use App\User;
use DB;
use App\Assignment_Client;
use App\Company;
use Excel;
use App\Exports\DataExport;
use App\Last_two_days_auction_report;
use App\Last_two_days_agent_report;
use App\Title_not_received;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){  

      $session_data = get_session_data();
     // echo '<pre>';print_r($session_data);exit;
        $comp_name =  get_company_name(); 
        //$comp_name = 'Crayhill';
        $assignmentsRep_arr     = '';$avg_sell_arr= '';
        $avg_sales_price_arr  = '';$title_status_arr= '';
        $average_vehicle_grade = ''; $average_vehicle_grade_arr='';
        $assignmentsRep=array();

         ## Average days to repossess
        $assignmentsRep=DB::select(DB::raw('select * from average_days_to_reposses where Client="'.$comp_name.'"'));

        if($assignmentsRep){
           $assignmentsRep = json_decode(json_encode($assignmentsRep),true); 
           if(!empty(@$assignmentsRep[0]['BulkDate'])>0){
              $assignmentsRep_arr = $this->create_array2($assignmentsRep);
            }
        } 
       // echo '<pre>';print_r($assignmentsRep_arr);exit;
        ## Average days to sell
        $average_days_to_sell=DB::select(DB::raw('select * from average_days_to_sell where Client="'.$comp_name.'"'));
       
        if($average_days_to_sell){
        $average_days_to_sell = json_decode(json_encode($average_days_to_sell),true);
        // echo '<pre>';print_r($average_days_to_sell);exit;
        if(!empty(@$average_days_to_sell[0]['BulkDate'])>0){
          $avg_sell_arr = $this->create_array2($average_days_to_sell);
        }
      }
       
       

        // echo '<pre>';print_r($assignmentsRep_arr);exit;
         ## Average sales price 
        $average_sales_price=DB::select(DB::raw('select * from average_sales_price where Client="'.$comp_name.'"'));
        $average_sales_price = json_decode(json_encode($average_sales_price),true); 


        if(!empty(@$average_sales_price[0]['BulkDate'])>0){
          $avg_sales_price_arr= $this->create_array2($average_sales_price);
        }
        // echo '<pre>';print_r( $average_sales_price);exit;
        ## Title Chart
        $title_status=DB::select(DB::raw('select * from title_status where Client="'.$comp_name.'"'));
        $title_status = json_decode(json_encode($title_status),true); 
        if(!empty(@$title_status[0]['BulkDate'])>0){
          $title_status_arr =  $this->create_array3($title_status);
        } 

        ## Average vehicle grade 
        $average_vehicle_grade=DB::select(DB::raw('select * from average_vehicle_grade where Client="'.$comp_name.'"')); 
        $average_vehicle_grade = json_decode(json_encode($average_vehicle_grade),true); 
        if(!empty(@$average_vehicle_grade[0]['BulkDate'])>0){
          $average_vehicle_grade_arr = $this->create_array2($average_vehicle_grade);
        }
        
      // 

      
      $graph_data['average_days_to_repo']  = $assignmentsRep_arr;
      $graph_data['average_days_to_sell']  = $avg_sell_arr;
      $graph_data['avg_sales_price']       = $avg_sales_price_arr;
      $graph_data['title_status']          = $title_status_arr;
      $graph_data['VehicleGrade']          = $average_vehicle_grade_arr;
       return view('dashboard_front')->with('graph_data', $graph_data);;      
    }


   

    public function create_array2($arr){ 
     // echo '<pre>';print_r($arr[0]['BulkDate']);exit;
      if(is_array($arr) && !empty($arr[0]['BulkDate'])){
        $prepare_graph_data = '';
        $prepare_graph_data =$arr[0]['BulkDate']; 
        $data_exp=  explode(',',$prepare_graph_data);
       //  echo '<pre>';print_r($data_exp);exit;
        $j=1; $final_array = array();
        $dataRow = array();$dataRowDates=array();
        for($i=0;$i<count($data_exp);$i++){ 
           if(strlen($data_exp[$i])==6 && intval($data_exp[$i])){
            $date_str       =  substr($data_exp[$i],2,5) .'/'. substr($data_exp[$i],0,2) .'/'. 01;
            $dateExtract    = date('M y',strtotime($date_str));
            //$dataRow[$dateExtract][$i]['city']        = $data_exp[$j]; 
            $dataRow[$dateExtract][$i]['month']        = $dateExtract; 
            if(isset($data_exp[$j])){
                $dataRow[$dateExtract][$i]['employees_no'] = $data_exp[$j];
            }else{
              $dataRow[$dateExtract][$i]['employees_no'] = 0;
            }
             
            //echo '<pre>';print_r($data_exp);exit;
            $dateExtractY  = date('Y',strtotime($date_str));
            $dateExtractM  = date('m',strtotime($date_str));
            $dataRowDates[$dateExtractY][]= $dateExtractM; 
           }
           $j++; 
        }

        ## reset inner keys
        foreach($dataRow as $k=>$v){
            $dataRowFinal[$k] = array_values($v);
        }  
         //echo '<pre>'; print_r($dataRow);exit;

      // dates with month wise extract and  reset 
        //echo '<pre>'; print_r( ($dataRowDates));exit;
        foreach($dataRowDates as $k=>$v){
          $dates_year[$k] =  (array_unique($v));
        }
        @asort($dates_year);
        @ksort($dates_year);

        ##Final Array
        
//==========================================================//
        foreach ($dataRowFinal as $key => $value) {
          $YearWiseCountArr[$key] = $value[0];
        }
//==========================================================//
        $final_array['year'] = $dates_year;
        $final_array['data'] = $YearWiseCountArr;
        //  echo '<pre>'; print_r( $final_array);exit; 
        return @json_encode( @$final_array) ;
      }
   }

   public function create_array3($arr){ 
        ##---------------added-------------##
        $final_array['year'] = '';
        $final_array['data'] = '';
        return @json_encode( $final_array);
        ##---------------added-------------##
      if(is_array($arr) && !empty($arr[0]['BulkDate'])>0){
        $prepare_graph_data = '';
        $prepare_graph_data =$arr[0]['BulkDate'];  
        //echo '<pre>';print_r($prepare_graph_data);exit;
        // echo '<pre>';print_r(str_replace('Title Received ','',$prepare_graph_data));exit;
        $data_exp=  explode(',',str_replace('Title Received,','',$prepare_graph_data));
        
        $j=1; $final_array = array();
        $dataRow = array();$dataRowDates=array();
        for($i=0;$i<count($data_exp);$i++){ 
           if(strlen($data_exp[$i])==6 && intval($data_exp[$i])){
            $date_str       =  substr($data_exp[$i],2,5) .'/'. substr($data_exp[$i],0,2) .'/'. 01;
            $dateExtract    = date('M y',strtotime($date_str));
            //$dataRow[$dateExtract][$i]['city']        = $data_exp[$j]; 
            //$dataRow[$dateExtract][$i]['month']        = $dateExtract; 
            $dataRow[$dateExtract][$i]['employees_no'] = $data_exp[$j]; 
//echo '<pre>';print_r($data_exp);exit;
            $dateExtractY  = date('Y',strtotime($date_str));
            $dateExtractM  = date('m',strtotime($date_str));
            $dataRowDates[$dateExtractY][]= $dateExtractM; 
           }
           $j++; 
        }

        ## reset inner keys
        foreach($dataRow as $k=>$v){
            $dataRowFinal[$k] = array_values($v);
        }  
       // echo '<pre>'; print_r($dataRow);exit;

      // dates with month wise extract and  reset 
        //echo '<pre>'; print_r( ($dataRowDates));exit;
        foreach($dataRowDates as $k=>$v){
          $dates_year[$k] =  (array_unique($v));
        }
        @asort($dates_year);
        @ksort($dates_year);

        ##Final Array
        
//==========================================================//
        if(@count($dataRowFinal)>0 && @is_array($dataRowFinal)){
        foreach ($dataRowFinal as $key => $value) {
          $YearWiseCountArr[$key] = $value[0];
        }
      }else{
        $YearWiseCountArr[] = 0;
      }
//==========================================================//
        $final_array['year'] = $dates_year;
        $final_array['data'] = $YearWiseCountArr;
        //  echo '<pre>'; print_r( $YearWiseCountArr);exit; 
        return @json_encode( @$final_array) ;
      }
   }

    public function create_array($arr){ 
      if(is_array($arr) && !empty($arr[0]['BulkDate'])>0){
        $prepare_graph_data = '';
        $prepare_graph_data =$arr[0]['BulkDate']; 
        $data_exp=  explode(' ',$prepare_graph_data);
        // echo '<pre>';print_r($data_exp);exit;
        $j=1;$k=2;$final_array = array();
        $dataRow = array();$dataRowDates=array();
        for($i=0;$i<count($data_exp);$i++){ 
           if(strlen($data_exp[$i])> 5 && intval($data_exp[$i])){
            $date_str       =  substr($data_exp[$i],2,5) .'/'. substr($data_exp[$i],0,2) .'/'. 01;
            $dateExtract  = date('M y',strtotime($date_str));
            $dataRow[$dateExtract][$i]['city']        = $data_exp[$j]; 
            $dataRow[$dateExtract][$i]['month']        = $dateExtract; 
            $dataRow[$dateExtract][$i]['employees_no'] = $data_exp[$k]; 

            $dateExtractY  = date('Y',strtotime($date_str));
            $dateExtractM  = date('m',strtotime($date_str));
            $dataRowDates[$dateExtractY][]= $dateExtractM; 
           }
           $j++;$k++;
        }

        ## reset inner keys
        foreach($dataRow as $k=>$v){
            $dataRowFinal[$k] = array_values($v);
        }  
       // echo '<pre>'; print_r($dataRowFinal);exit;

      // dates with month wise extract and  reset 
        //echo '<pre>'; print_r( ($dataRowDates));exit;
        foreach($dataRowDates as $k=>$v){
          $dates_year[$k] =  (array_unique($v));
        }
        asort($dates_year);
        ksort($dates_year);

        ##Final Array
        $final_array['year'] = $dates_year;
        $final_array['data'] = $dataRowFinal;
       // echo '<pre>'; print_r( $final_array);exit;
 
       
        return @json_encode( @$final_array) ;
      }
   }

   ##-------------------------- dashboard Export functions start -----------------------------##
   public function dashboard_export_xls() { 
      $filename = "Scheduled_for_sale_".date("Ymd").'.csv'; 
      $session_data        = get_session_data();## Helper function fetching all session data  
      $company_name        = '';
      $company_name        = get_company_name(); 
      $Assignment_Client   = Assignment_Client::where(array('client_name'=>$company_name,'status'=>'CLEARED FOR SALE'))

       ->selectRaw(' Auction ,condition_report_link as `Condition Report` , days_run as `Run#`,client_account_no as `Cn.acc`,assignment_notes as `Description` ,Mileage, Grade as `Vehicle Grade`, 
       floor_price `Floor Price(Requested)`,sales_price as `Floor Price(Accepted)`,
       from_unixtime(scheduled_sale_date, "%Y / %m / %d")
        as `Scheduled Date`, from_unixtime(legal_sale_date, "%Y / %m / %d") as `Legal Sale Date`, client_name as `Secondary Client` ,report_information as `Summary` '
            );  
      $Assignment_Client = $Assignment_Client->latest()->get();
      if($Assignment_Client){
        $Assignment_Client = $Assignment_Client->toArray();  
      } 

      //echo '<pre>';print_r( $Assignment_Client  );exit;
 
          // file name for download
          /*Auction-Auction-Auction
          Run#-days_run varchar
          Photo-?
          Cn.Acc- client_account_no
          description- assignment_notes
          Mileage- Mileage
          Vehicle Grade=Grade
          floor price(requested)= floor_price
          floor price(accepted)= sales_price
          Auction Age=?
          scheduled date= scheduled_sale_date
          on block =?
          title received=?
          pricing_info==?
          legal_sale_date
          secondary_client=client
          summary=report_information text */
     header("Content-Type: text/csv");
      header("Content-Disposition: attach; filename=\"$filename\""); 
      $flag = false;
      $column_array = array();
     
      foreach($Assignment_Client as $k=>$row) { 
          if(!$flag) { 
            echo implode(",", array_keys($row)) . "\n";
            $flag = true;
          } 
          echo implode(",", array_values($row)) . "\n"; 
      } 
    }

    public function inventory_dashboard_export_xls() { 
      $filename = "inventory_".date("Ymd").'.csv'; 
      $session_data        = get_session_data();## Helper function fetching all session data  
      $company_name        = '';
      $company_name        = get_company_name(); 
      $Assignment_Client   = Assignment_Client::where(array('client_name'=>$company_name))

       ->selectRaw('client_account_no as `Cn.acc`, Auction, client_name  as `Secondary Client`, status as `Status`, from_unixtime(recovery_date, "%Y / %m / %d") as `Recovery date`, from_unixtime(secured_date, "%Y  / %m /   %d") as `Secured Date`, vin as `VIN`, assignment_notes as `Description` ,  auction_title_status as `Title Status`, from_unixtime(legal_sale_date, "%Y / %m / %d") as `Legal Sale Date`, from_unixtime(scheduled_sale_date, "%Y / %m / %d")
        as `Scheduled Date`, from_unixtime(sold_date, "%Y / %m / %d")
        as `Sold Date`, floor_price `Floor Price(Accepted)`,sales_price as `Sales Price`, total_charges as `Total Changes`,  Grade as `Vehicle Grade` '
            );  
      $Assignment_Client = $Assignment_Client->latest()->get();
      if($Assignment_Client){
        $Assignment_Client = $Assignment_Client->toArray();  
      } 
 
      header("Content-Type: text/csv");
      header("Content-Disposition: attach; filename=\"$filename\""); 
      $flag = false;
      $column_array = array();
     
      foreach($Assignment_Client as $k=>$row) { 
          if(!$flag) {
            echo implode(",", array_keys($row)) . "\n";
            $flag = true;
          } 
          echo implode(",", array_values($row)) . "\n";
      } 
    }


    public function export_xls_last_two_days_agent() { 
    $type = 'xlsx';
    $filename = "last_two_days_agent_".date("Ymd").'.csv';
    $hidden_val          = request('hid_val');
    $session_data        = get_session_data();## Helper function fetching all session data 
     
    $company_name        = '';
    $company_name        = get_company_name(); 
    if(!empty($company_name)){

      $Assignment_Client   = Last_two_days_agent_report::where(array('clientName'=>$company_name));  
      
      $Assignment_Client = $Assignment_Client->get();
      if($Assignment_Client){
        $Assignment_Client = $Assignment_Client->toArray();  
      } 
        // file name for download
        
        //header("Content-Disposition: attachment; filename=\"$filename\"");
       // header("Content-Type: application/vnd.ms-excel");
        
        header("Content-Type: text/csv");
        header("Content-Disposition: attach; filename=\"$filename\"");

        $flag = false;
        foreach($Assignment_Client as $row) {
          if(!$flag) {
            // display field/column names as first row
            echo implode(",", array_keys($row)) . "\n";
            $flag = true;
          }
         // array_walk($row, __NAMESPACE__ . '\cleanData');
          echo implode(",", array_values($row)) . "\n";//  \t replace with ,
        }  
      }else{
        return back();
      }

    }


    public function export_xls_last_two_days_auction() { 
    $type = 'xlsx';
    $filename = "last_two_days_auction_".date("Ymd").'.csv';
    $hidden_val          = request('hid_val');
    $session_data        = get_session_data();## Helper function fetching all session data 
     
    $company_name        = '';
    $company_name        = get_company_name(); 
    if(!empty($company_name)){
      $Assignment_Client   = Last_two_days_auction_report::where(array('client_name'=>$company_name));  
      
      $Assignment_Client = $Assignment_Client->get();
      if($Assignment_Client){
        $Assignment_Client = $Assignment_Client->toArray();  
      } 
        // file name for download
        //echo '<pre>';print_r( $Assignment_Client );exit;
       /* header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");*/
        header("Content-Type: text/csv");
        header("Content-Disposition: attach; filename=\"$filename\"");
        
        // echo '<pre>';print_r($Assignment_Client );exit;
        $flag = false;
        foreach($Assignment_Client as $row) {
          if(!$flag) {
            // display field/column names as first row
            echo implode(",", array_keys($row)) . "\n";
            $flag = true;
          }
         // array_walk($row, __NAMESPACE__ . '\cleanData');
          echo implode(",", array_values($row)) . "\n";
        }
      }
    }

    public function export_title_not_received() { 
      $filename = "Title_not_received".date("Ymd").'.csv'; 
      $session_data        = get_session_data();## Helper function fetching all session data 
      $company_name        = '';
      $company_name        = get_company_name(); 
      if(!empty($company_name)){ 
        $Title_not_received   = Title_not_received::where(array('client_name'=>$company_name)); 
        $Title_not_received = $Title_not_received->get();
        if($Title_not_received){
          $Title_not_received = $Title_not_received->toArray();  
        } 
        
       // echo '<pre>';print_r(  $Title_not_received );exit;
        header("Content-Type: text/csv");
        header("Content-Disposition: attach; filename=\"$filename\"");

          $flag = false;
          foreach($Title_not_received as $row) {
            if(!$flag) {
              // display field/column names as first row
              echo implode(",", array_keys($row)) . "\n";
              $flag = true;
            }
           // array_walk($row, __NAMESPACE__ . '\cleanData');
            echo implode(",", array_values($row)) . "\n";//  \t replace with ,
          }  
      }else{
      return back();
      } 
    }

   ##-------------------------- dashboard Export functions end -----------------------------##



    ##------------------download export data --------------------------------##
 public function download_client_assgnmt_xls() { 
    $type = 'xlsx';
    
    $session_data        = get_session_data();## Helper function fetching all session data 
     
    $company_name        = '';
    $company_name        = get_company_name(); 
    $filename  = $company_name.'_' . date('Ymd') . ".csv"; 

    $Assignment_Client   = Assignment_Client::where(array('client_name'=>$company_name))
     ->selectRaw('client_name  as `Client Name`, client_account_no as `Client Account`, caprock_account `Caprock Account` , 
      from_unixtime(contract_date, "%Y  / %m /   %d") as `Contract Date`,  
original_loan_amount as `Original Loan Amount`,
first_name as `First Name`,
last_name as `Last Name`,
ssn as `SSN`,
customer_address as `Customer Address`,
customer_apt_no as `Customer Apt No`,
customer_city as `Customer City`,
customer_state as `Customer State`,
customer_zip as `Customer Zip`,
cust_poe_company_name as `Cust Poe Company Name`,  
cust_poe_company_address as `Cust Poe Company Address`,
cust_poe_suite_no as `Cust Poe Suite No`,
cust_poe_company_city as `Cust Poe Company City`,
cust_poe_company_state as `Cust Poe Company State`,
cust_poe_company_zip as `Cust Poe Company Zip`,
assignment_notes as `Assignment Notes`,
vehicle_notes_type as `Vehicle Notes Type`,
vehicle_notes as `Vehicle Notes`, 
date_initial_assignment_received as `Date Initial Assignment Received`,
status as `Status`,
repo_status as `Repo Status`,
from_unixtime(cure_sent, "%Y  / %m /   %d")  as `Cure Sent`,
from_unixtime(cure_expired, "%Y  / %m /   %d")  as `Cure Expired`,
from_unixtime(date_assigned_to_repo_agent, "%Y  / %m /   %d")  as `Date Assigned To Repo Agent`,

from_unixtime(recovery_date, "%Y  / %m /   %d")  as `NOI Mail Date`,
from_unixtime(NOI_mail_date, "%Y  / %m /   %d")  as `NOI Mail Date`,
 from_unixtime(NOI_expire_date, "%Y  / %m /   %d")  as `NOI Expire Date`,
 
repo_hold_reason as `Repo Hold Reason`,
close_reason as `Close Reason`,
 from_unixtime(close_date, "%Y  / %m /   %d") as `Close Date`,
vehicle_driveable as `Vehicle Driveable`,
storage_address as `Storage Addresss`,
storage_apt_no as `Storage City`,
storage_city as `Loan Amount`,
storage_state as `Storage State`,
storage_zip as `Storage Zip`,
auction_status as `Auction Status`,
auction as `Auction`,
auction_address as `Auction Address`,
auction_title_status as `Account Title Status`, 
from_unixtime(secured_date, "%Y  / %m /   %d")  as `Secured Date`,
condition_report_link as `Condition Report Link`,
Mileage ,
Grade ,
MMR ,
floor_price as `Floor Price`,
sales_price as `Sales Price`,
days_run as `Days Run`,
from_unixtime(hold_date, "%Y  / %m /   %d")  as `Hold Date`,
from_unixtime(sale_hold_reason, "%Y  / %m /   %d")  as `Sale Hold Reason`,
from_unixtime(scheduled_sale_date, "%Y  / %m /   %d")  as `Scheduled Sale Date`,
total_charges as `Total Charges`,
from_unixtime(sold_date, "%Y  / %m /   %d")  as `Sold Date`,
report_information as `Report Information`,
row_status as `Row Status`,
created_by as `Created By`, 
updated_by as `Updated By`,
from_unixtime(created_at, "%Y  / %m /   %d") as `Created Date`, 
from_unixtime(updated_at, "%Y  / %m /   %d")  as `Updated Date`, 
loan_class as `Loan Class`'); 
     
    
    $Assignment_Client = $Assignment_Client->latest()->get();
    if($Assignment_Client){
      $Assignment_Client = $Assignment_Client->toArray();  
    } 
      // file name for download
      header("Content-Type: text/csv");
      header("Content-Disposition: attach; filename=\"$filename\"");
      
       
      $flag = false;
      foreach($Assignment_Client as $row) {
        if(!$flag) {
          // display field/column names as first row
          echo implode(",", array_keys($row)) . "\n";
          $flag = true;
        }
       // array_walk($row, __NAMESPACE__ . '\cleanData');
        echo implode(",", array_values($row)) . "\n";
      }  

    }
    ##---------------------Download export data all end -------------------##
}?>