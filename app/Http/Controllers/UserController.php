<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {  //  echo '<pre>ss';print_r($request->post('srchFld'));exit;
        $session_data       = get_session_data();## Helper function fetching all session data
        $current_role_id    = get_role_id();
        
         $srch = $request->post('srchFld') ; 
        $data = DB::table('users')
            ->leftJoin('companies', 'users.fk_company_id', '=', 'companies.id')
            ->select('users.*','companies.company_name');

        if(!empty($srch)){
            $data = $data->whereRaw(" (companies.company_name LIKE '%".$srch."%' OR users.name LIKE '%".$srch."%')");
        }
        if ($current_role_id>1) {
            $data = $data->where('users.created_by', $session_data['user_id']);
        }else{ 
            $data = $data->where('users.id', '<>', $session_data['user_id']);
        } 
        $data = $data->orderBy('users.id', 'DESC')->paginate(5);
 
        return view('users.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function index2(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
     
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $session_data       = get_session_data();## Helper functionfetching all session data
        $roles  = array();
        // echo '<pre>';print_r(  $session_data);exit;
        $rolesArr = DB::table('model_has_roles')->where('model_id', $session_data['user_id'])->get()->toArray();
      
        //echo '***'; dd(Auth::user() ->roles ->pluck('id') ->toArray() );;exit;
        
        $roles_arr = json_decode(json_encode($rolesArr), true);    //  echo '<pre>';print_r($rolesArr);exit;
        if ($roles_arr[0]['role_id']<='2') {
            $array = array(4);
            if ($roles_arr[0]['role_id']==1) {
                $array = array(1,2);//// FOr Display User place ",4"
            }
            $roles = Role::whereIn('id', $array);
            
            $roles = $roles->pluck('name', 'name')->all();
        }
        // echo '<pre>';print_r( $roles);exit;
        return view('users.create', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //echo '<pre>';print_r($_POST);exit;
        $this->validate($request, [
            'name' => 'required',
            'roles.*' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ], ['roles.*.required'=>"Role is mandatory"]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        //echo '<pre>';print_r($input);exit;
    
        $user = User::create($input);
        $rolesDt = $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
                        ->with('success', 'User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        //$roles = Role::where('id','<>',1)->pluck('name','name')->all();

        $session_data       = get_session_data();## Helper functionfetching all session data
 
         
        $roles = Role::pluck('name', 'name')->all();
        
        $userRole = $user->roles->pluck('name', 'name')->all();
        //dd($userRole);
        return view('users.edit', compact('user', 'roles', 'userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            //'roles' => 'required'
        ]);


        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }


        $user = User::find($id);
        $user->update($input);
        //DB::table('model_has_roles')->where('model_id', $id)->delete();


        //$user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success', 'User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session_data       = get_session_data();## Helper functionfetching all session data
        if ($id!=1 && $id!= $session_data['user_id']) {
            User::find($id)->delete();
        }
        return redirect()->route('users.index')
                        ->with('success', 'User deleted successfully');
    }

    public function impersonate($user_id)
    {
        if ($user_id != ' ') {
            $user = User::find($user_id);
            Auth::user()->impersonate($user);
            return redirect('/');
        }
        return redirect()->back();
    }

    public function impersonate_leave()
    {
        Auth::user()->leaveImpersonation();
        return redirect('/');
    }

    public function user_profile(User $user)
    {
        return view('users.my_profile', compact('user'));
    }
}
