<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\RequestAccount;

class RequestAccountController extends Controller
{
    public function create()
    {
        return view('requestaccount.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | min:3',
            'email' => 'required | email| min:4',
            'phone' => 'required | min:10',
        ]);
        $data = array();

        $data['name'] = request('name');
        $data['email'] = request('email');
        $data['phone'] = request('phone');

        $admin_email = 'jaysingh4797@gmail.com';
        Mail::to($admin_email)->send(new RequestAccount($data));

        session()->flash('message', 'Your request has been sent successfully!');
        return redirect()->back();
    }
}
