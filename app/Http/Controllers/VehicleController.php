<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\Field;
use App\Vehicle;

class VehicleController extends BaseController 
{
    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(\App\Forms\vehicleForm::class
            ,[
            'method' => 'POST',
            'url' => route('vehicles.store')
        ]);

        return view('vehicles.create', compact('form'));
    }

    public function store(FormBuilder $formBuilder, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lyrics' => 'required',
            //'publish' => 'required',
        ]);
        // $form = $formBuilder->create(\App\Forms\VehicleForm::class);
        // $form->redirectIfNotValid();
        //dd(request()->all());
        $id = request('id');
        if (!empty($id) && is_numeric($id)) {
            $vehicle = Vehicle::find($id);
        } else {
            $vehicle = new Vehicle();
        }
        
        $vehicle->name = request('name');
        $vehicle->lyrics = request('lyrics');
        $vehicle->publish = request('publish');
        $vehicle->save();

        // Do redirecting...
    }

    public function edit(Vehicle $vehicle, FormBuilder $formBuilder)
    {
        
        //$vehicle = Vehicle::findOrFail($id);
        $form = $formBuilder->create('App\Forms\vehicleForm', [
            'model' => $vehicle,
            'method' => 'POST',
            'url' => route('vehicles.store')
        ])->add('id', 'hidden', [ 'value' => $vehicle->id]);

        return view('vehicles.create', compact('form'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lyrics' => 'required',
            'publish' => 'required',
        ]);
        // $form = $formBuilder->create(\App\Forms\VehicleForm::class);
        // $form->redirectIfNotValid();
        
        $vehicle = new Vehicle();
        $vehicle->name = request('name');
        $vehicle->lyrics = request('lyrics');
        $vehicle->publish = request('publish');
        $vehicle->save();

        // Do redirecting...
    }
}
