<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use App\FileUpload;
use App\Jobs\ProcessFileExport;
use App\Jobs\AssignmentImport;

class FileUploadController extends Controller
{
    public function create()
    {
        // echo phpinfo();
        // exit;
        return view('files.create');
    }

    public function store(Request $request)
    {//dd($request->all());
        // request()->validate(
        //     [
        //     'files_input'     => 'required|array',
        //     'files_input.*'   => 'required|mimes:csv,txt|max:5120',
        // ],
        //     [
        //     'files_input.*.mimes' => 'Only csv files are allowed',
        //     'files_input.*.max' => 'Sorry! Maximum allowed size is 5MB',
        // ]
        // );
        //exit;


        $file = $request->file('files_input');
        // print_r($file[0]->getClientOriginalExtension());exit;
        $name = $file[0]->getClientOriginalName();
        $ext = 'csv'; 
        $ext =$file[0]->getClientOriginalExtension();
       // echo '<pre>';print_r( $ext_arr );exit;
        //echo'###'. end(explode('.',$name))  ; 
        $company_name = get_company_name();
        $get_session_data = get_session_data();
        $file_with_company_name=$company_name.'_'.time().'_'.$get_session_data['fk_company_id'].'_'.$get_session_data['user_id'].'.'.$ext;
        //echo $file_name;exit;
        $file_arr = upload_file($file, '/files', '/files/thumb/', 'csv', 300, 500,$file_with_company_name);
        foreach ($file_arr as $key => $value) {
            $file_data = new FileUpload;
            $file_data->file_name		= $value['generated_file_name'];
            $file_data->file_org_name   = $value['original_file_name'];
            $file_data->extension       = $value['extension'];
            $file_data->category        = 'Assignment';
            $file_data->status          = 1;
            $file_data->created_by      = auth()->user()->id;
            if (!$file_data->save()) {
                session()->flash('message_err', 'File could not be uploaded!');
                return redirect()->back();
            }
            AssignmentImport::dispatch($file_data);
        }
        
        session()->flash('message', 'Your file(s) are uploaded successfully!');
        return redirect()->back();
    }



    public function dropzone()
    {
        return view('dropzone_file');
    }
    
    /**
     * Image Upload Code
     *
     * @return void
     */
    public function dropzoneStore(Request $request)
    {
        $image = $request->file('file');
   
        $imageName = time().'.'.$image->extension();
        $image->move(public_path('images'), $imageName);
   
        return response()->json(['success'=>$imageName]);
    } 
}
