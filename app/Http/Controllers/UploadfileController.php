<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User; 
use App\Assignment;
use App\Assignment_Client;
use Carbon\Carbon;
use Mail;
use Auth; 
use Storage;
use Response;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use League\Flysystem\Adapter\Ftp;
use App\Jobs\ExportFileToCapJob; 

class UploadfileController extends Controller
{
    public function __construct()
    {
      // $this->middleware('auth');
    }  
    public function job_export_upl_file_to_caprock(Request $request){//echo 'kamal';exit;
        $local_folder = public_path('files');//App to Caprock Assignments
        $file_list = array_diff(scandir($local_folder), array('.', '..','thumb')); 
        foreach( $file_list as $k=>$v){
            $path_withFile  = $local_folder.'/'.$v; 
            if(ExportFileToCapJob::dispatch($v)){
                echo '<br>DONE--'.$path_withFile .' Job created';  
            }else{
                echo '<br>ERROR--'.$path_withFile .' Error Job not created';
            }  
        } 
    } 
}
