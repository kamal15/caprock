<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Company;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
         
    }

    public function index()
    {
         $session_data = get_session_data();
        // echo '<pre>';
        //  print_r($session_data);
        //   exit;
        if(!empty(@$session_data['fk_company_id']) && @$session_data['role_name']!='Caprock Administrator'){
            $companiesDT =  company_listing($session_data['fk_company_id'],1);
            $companies = array();
             $compArr= $companiesDT->toArray();
             if(count($compArr)>0){
                $companies[0]['id']= array_keys(@$compArr)[0];
                $companies[0]['company_name']= array_values(@$compArr)[0];
             }
             
        }else{
                //$companies = Company::all();
                $companies = Company::where(array('status'=>1))->get();
                
                
                if( $companies ){
                    $companies =  $companies->toArray();
                }
        }
       
        //echo '<pre>';print_r($companies);exit;

        
        return view('documents.index', compact('companies'));
    }

    public function file_upload(Request $request)
    {
        // echo '<pre>==';
        // print_r($_POST);
        // exit;
        //echo '<pre>==FILES===';print_r($_FILES['file']);exit;
        $msg = '';
        $suc_msg = '';
        $doc_file = '';

        if (!empty($_FILES['file']['name']) && !empty($request->company_id)) {
            $doc_file               = $_FILES['file']['name'];
            $get_extension = pathinfo($doc_file, PATHINFO_EXTENSION)   ;
            ## ------------- save img from image-data to .png ---------------##

            $allowed_extension = array( "png","jpg","jpeg","pdf","xls","xlsx","doc",'txt');
            
            // Validate file input to check if is not empty
            if (! file_exists($_FILES["file"]["tmp_name"]) || empty($doc_file)) {
                $msg =  "Sorry, file not found to upload!.";
            }    // Validate file input to check if is with valid extension
            elseif (! in_array($get_extension, $allowed_extension)) {
                $msg =  "Sorry, invalid file type!.";
            }    // Validate image file size
            elseif (($_FILES["file"]["size"] > 5000000)) {
                $msg =  "Sorry, your file is too large.";
            } else {
                $filename                = time().'.'.$get_extension;
                $target_dir         = public_path('./documents/');
                $target_file = $target_dir . $filename ;
              
                ## save original image ##
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    $suc_msg                            =   "The file ". basename($doc_file). " has been uploaded.";
                    
                    $ext                            = pathinfo($target_file, PATHINFO_EXTENSION);
                    ##save data
                    //$table                          = $_POST['tname'];
                    $tableData                      = new Document;
                    //$tableData->AcctID              = $_POST['AcctID'];
                    $tableData->document_name       = $filename;
                    $tableData->doc_org_name        = $doc_file;
                    $tableData->company_id           = $request->company_id;
                    $tableData->extension           = $ext;
                    //$tableData->type                = $table;
                    $tableData->uploaded_by         = auth()->id();
                    $tableData->status              = 1;
                    $tableData->save();
                } else {
                    $msg =   "Sorry, an error occured while uploading your file.";
                }
                ## ------------- save img from image-data to .png ---------------##
            }
        } else {
            $msg = "Either Company id is Missing Or File not Uploaded";
        }
        return response()->json([
        'suc_msg'          => $suc_msg,
        'err_msg'           => $msg,
        'company_id'        => $request->company_id,
        'original_name' => $doc_file,
    ]);
    }

    public function list_uploaded_file(Request $request, $id='')
    {
        $uploaded_by ="";
        $uploaded_by = request()->post('uploaded_by');
        $company_id = $request->company_id;
        $getdata = Document::where(array('category'=>'general','status'=>1));
        if (is_numeric($company_id)) {
            $getdata = $getdata->where('company_id', $company_id);
        }
        $getdata = $getdata->latest()->get();
        $data_display = '';
        if ($getdata) {
            $data_res  = $getdata->toArray();
            // echo '<pre>';print_r($data_res);exit;
        }
        if (@count($data_res)>0) {
            foreach ($data_res as $k=>$v) {
                $extenion = strtolower($v['extension']);
                if ($extenion=='jpg' || $extenion=='jpeg' || $extenion=='png' || $extenion=='gif') {
                    $extenion = 'jpg';
                }
                if ($extenion=='doc' || $extenion=='docx' || $extenion=='txt' || $extenion=='xls' || $extenion=='xlsx') {
                    $extenion = 'doc';
                }
                $id             = $v['id'];
                //$acctID         = $v['AcctID'];
                $filename       = $v['document_name'];
                $url            = public_path().'/documents/'.$filename ;
               
                $data_display  .= '<div class="form-group row text-justify">
                    <div class="col-lg-2 text-center"> 
                    <a title="Download File" href="/download-file/'.$filename.'" 
                        target="_blank"><img src="'.url('/assets/media/files/'.$extenion.'.svg').'" alt="" style="width: 50px;">
                        </a>
                    </div>
                    <div class="col-lg-6 text-justify">
                        <a class="btn btn-warning" href="/download-file/'.$filename.'" class="k-widget-7__item-title" title="Download File">'.$v['doc_org_name'].'
                        </a> 
                    </div>
                    <div class="col-lg-2 text-center">
                        <a class="btn btn-danger" href="javascript:void(0);" onclick="return deleting_file('.$id.','.$uploaded_by.');"> Delete</a>
                    </div>
                </div>
                 ';
            }
        } else {
            $data_display  = 0;
        }
        echo $data_display;
        exit;
    }

    public function download_file(Request $request)
    {
        $filepath =  'documents/'.$request->file;
        return response()->download($filepath, $request->file);
    }

    public function deleting_file(Request $request)
    {
        ######### ======================= For update process =====================##########
        $id = $request->id;
        $userID = $request->userID;
        $filestatus['status']        = 2; ## status 2 for delete, 0 for inactive and 1 for active
        // echo '<pre>';print_r(request()->segment(1));exit;
        $res= \DB::table('documents')->where(array('id'=>$id,'uploaded_by'=>$userID))->update($filestatus);
       
        if ($res) {
            session()->flash('message', 'File has been deleted successfully!!');
        } else {
            session()->flash('message_err', 'File Not deleted!!');
        }
        $redirect_url = 'all-documents/'.request()->segment(4);
        return   redirect($redirect_url);
    }
}
