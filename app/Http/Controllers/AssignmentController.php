<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Assignment;
use App\Assignment_Client;
use App\FileUpload;
use App\TblNote;
use App\Document;
use App\Company;
use Illuminate\Support\Facades\Mail;

class AssignmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        $states = DB::table('states')->pluck('code', 'name')->toArray();

        return view('assignments.create', compact('states'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'assignment_type'           => 'required',
            'repo_type'                 => 'required',
            'make'                      => 'required|alpha_num',
            'model'                     => 'required|alpha_num',
            'year'                      => 'required|numeric|digits:4',
            'vin'                       => 'required|alpha_num',
            'license_plate'             => 'nullable|alpha_num',
            'vehicle_address'           => 'required_if:assignment_type,remarketing|regex:/^[A-Za-z.\d\s]+$/',
            'vehicle_apt_no'            => 'nullable|alpha_num',
            'vehicle_city'              => 'required_if:assignment_type,remarketing|alpha',
            'vehicle_state'             => 'required_if:assignment_type,remarketing|alpha|size:2',
            'vehicle_zip'               => 'required_if:assignment_type,remarketing|regex:/^\d{5}(-\d{4})?$/',
            'legal_sale_date'           => 'required_if:assignment_type,remarketing|date',
            'client_name'               => 'required',
            'client_account_no'         => 'required|alpha_num',
            'contract_date'             => 'required|date',
            'original_loan_amount'      => 'required|regex:/^\$?[0-9]+(,\d{3})*(\.[0-9][0-9])?$/',
            'client_queue_no'           => 'nullable|numeric',
            'first_name'                => 'required|alpha',
            'last_name'                 => 'required|alpha',
            'ssn'                       => 'required|regex:/^[0-9]{3}(-)[0-9]{2}(-)[0-9]{4}/',
            'customer_address'          => 'required|regex:/^[A-Za-z\d\s]+$/',
            'customer_apt_no'           => 'nullable|alpha_num',
            'customer_city'             => 'required|alpha',
            'customer_state'            => 'required|alpha|size:2',
            'customer_zip'              => 'required|regex:/^\d{5}(-\d{4})?$/',
            'cust_poe_company_name'     => 'nullable|alpha',
            'cust_poe_company_address'  => 'nullable|regex:/^[A-Za-z\d\s]+$/',
            'cust_poe_suite_no'         => 'nullable|numeric',
            'cust_poe_company_city'     => 'nullable|alpha',
            'cust_poe_company_state'    => 'nullable|alpha|size:2',
            'cust_poe_company_zip'      => 'nullable|regex:/^\d{5}(-\d{4})?$/',
            //'documents'               => 'required',
            //'documents.*'             => 'mimes:csv',
        ]);
        DB::beginTransaction();
        $attributes = $request->all();
        $attributes['legal_sale_date']  = date('Y-m-d', strtotime(request('legal_sale_date')));
        $attributes['contract_date']    = date('Y-m-d', strtotime(request('contract_date')));

        $attributes['created_by'] = auth()->user()->id;
        $assignment = Assignment::create($attributes);
        $company_name          = get_company_name();
        $filename   = $company_name . '_' . $attributes['vin'] . '_' . date('m-d-Y') . ".csv";
        $documents  = $request->file('documents');

        // echo '<pre>';
        // print_r($documents[0]->getClientOriginalName());
        // exit;
        // echo 'kamal';exit;
        if (!empty(@$documents)) {
            // echo 'kamal';
            // exit;
            $file_arr = upload_file($documents, '/assignment_docs', '', 'csv', '', '', $filename);
            foreach ($file_arr as $key => $value) {
                $file_data = new FileUpload;
                $file_data->entityID        = $assignment->id;
                $file_data->file_name       = $filename;
                $file_data->file_org_name   = $value['original_file_name'];
                $file_data->extension       = $value['extension'];
                $file_data->category        = 'Assignment';
                $file_data->status          = 1;
                $file_data->created_by      = auth()->user()->id;
                if (!$file_data->save()) {

                    DB::rollBack();
                    session()->flash('message_err', 'Document(s) could not be uploaded!');
                    return redirect()->back()->withInput();
                } else {
                    // echo '***'.$assignment->id;exit;
                }
            }
        }
        DB::commit();
        session()->flash('message', 'Assignment created successfully!');
        return redirect()->back();
    }

    public function storeportfolio(Request $request)
    {
        $this->validate($request, [
            //'statusOpt'       => 'required',
            'note_text'          => 'required',
            //'dateOrgDocRecvd'   => 'required',
        ], [
            'statusOpt.required' => 'Note status required',
            'note_text.required' => 'Note text required',
            'dateOrgDocRecvd.required' => 'Date of original doc required'
        ]);

        $Notes = '';
        //  echo '<pre>';     print_r($_POST); exit;
        $TblNote = new TblNote;
        $TblNote->note_status       = 'active'; //$request->post('statusOpt') == 'Active' ? 'active' : 'inactive';
        $TblNote->noteType          = !empty($request->post('note_type')) ? $request->post('note_type') : '';
        $TblNote->noteText          = !empty($request->post('note_text')) ? $request->post('note_text') : '';
        $TblNote->FK_assignment_id  = $request->assignment_id;
        $TblNote->date_of_org_doc   = !empty($request->post('dateOrgDocRecvd')) ? date("Y-m-d", strtotime(str_replace('/', '-', $request->post('dateOrgDocRecvd')))) : ''; //
        $TblNote->created_by         = 1; //auth()->user()->id;
        // echo '<pre>';print_r($TblNote);exit;

        $Notes = $TblNote->save();
        if ($Notes) {
            $loan_no   = $request->post('loan_no');
            $vin       = $request->post('vin_n');
            $type      = $request->post('note_type');
            $notes     = $request->post('note_text');

            $this->profile_vehicle_notes_mail($notes, $type, $loan_no, $vin);
            session()->flash('message_notes', 'Notes added successfully!');
        } else {
            session()->flash('message_notes', 'Notes not added!');
        }

        return redirect()->back();
    }

    public function portfolio(Request $request)
    {   //echo '<pre>';print_r($_POST);exit;
        $company_name          = get_company_name();
        $assignments_by_client = Assignment_Client::select("*");
        if ($company_name) {
            $assignments_by_client = $assignments_by_client->where(array('client_name' => $company_name));
        }

        $search = $request->post('srch_str');
        if ($search) {
            $assignments_by_client = $assignments_by_client->whereRaw(
                "(vin LIKE '" . $search . "%')
                                        OR (client_account_no LIKE '" . $search . "%')
                                        OR (first_name LIKE '" . $search . "%')
                                        OR (last_name LIKE '" . $search . "%')
                                        "
            );
        }
        $assignments_by_client = $assignments_by_client->orderBy('status', 'asc')->paginate(1700);
        //echo '<pre>';print_r( $assignments_by_client );exit;
        return view('assignments.portfolio_view', compact('assignments_by_client'));
    }

    public function portfolio_vehicle_dt(Request $request)
    {
        $id = '';
        if (!empty(request()->segment(2))) {
            $id = request()->segment(2);
        }

        $assignments_by_client_dt = Assignment_Client::where(array('id' => $id))->first();
        $notes = TblNote::where('FK_assignment_id', $id)->get();

        return view('assignments.portfolio_detail_view', compact('assignments_by_client_dt', 'notes'));
    }


    public function update_document(Request $request)
    {
        //$allowed_extension   = array( "png","jpg","jpeg","xls","xlsx","doc",'txt');
        $this->validate($request, [
            'files_input.*' => 'required|mimes:doc,pdf,docx,png,jpg,jpeg,xls,xlsx,txt'
        ]);

        $target_dir          = public_path('./upload_doc/');
        ## User Session data
        $user_session_data = get_session_data();
        //print_r($user_session_data);exit;
        $user_id =  $user_session_data['user_id'];
        $fk_company_id =  $user_session_data['fk_company_id'];
        $files = $request->file('files_input');

        ## User Session data
        ## ------------- save img from image-data to .png ---------------##
        if ($files) {
            foreach ($files as $k => $file) {
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                $filename = '';
                $filename                = $k . '_' . time() . '.' . $extension;
                $name = $file->getClientOriginalName();
                $file->move($target_dir, $filename);

                $docs                       =  new Document;
                $docs->company_id             = $fk_company_id;
                $docs->document_name        = $filename;
                $docs->doc_org_name         = $name;
                $docs->extension            = $extension;
                $docs->uploaded_by          = $user_id;
                $docs->status               = 1;
                $docs->category             = 'veh_detail';
                $docs->save();
            }
            ##save data
            session()->flash('message', 'File uploaded sucessfully.');
        } else {
            session()->flash('message_er', 'Please select a file to uplaod.');
        }
        $qstr = $request->segment(2);
        return redirect(url('portfolio-detail/1'));
    }

    public function profile_vehicle_notes_mail($notes, $type, $loan_no, $vin)
    {
        $party_email = array();

        $data           = array();
        $data['notes']  = $notes;
        $data['type']   = $type;
        $data['loan_no'] = $loan_no;
        $data['vin']    = $vin;
        $toEmail        = array('repo@innovateauto.com', 'assetremarketing@innovateauto.com');

        //emails: repo@innovateauto.com and assetremarketing@innovateauto.com.
        if (!empty($notes) && !empty($type) && $loan_no && !empty($vin)) {
            Mail::to($toEmail)->cc($party_email)->send(new \App\Mail\NotesMail($data));
        } else {
            Mail::to($toEmail)->send(new \App\Mail\NotesMail($data));
        }
        return true;
    }
}
