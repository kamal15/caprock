<?php

namespace App\Http\Controllers;

use App\Company;
use File;
use App\Document;


use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:company-list|company-create|company-edit|company-delete', ['only' => ['index','show']]);
        $this->middleware('permission:company-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:company-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = request('status');
        $session_data       = get_session_data();## Helper function fetching all session data
       
        if ($status != "") { //dd($status);
            $companies = Company::where('status', $status);
        } else { //dd('here');
            $companies = Company::where('status', 1);
        }

        $current_role_id = get_role_id();
        if ($current_role_id>1) {
            $companies = $companies->where('created_by', $session_data['user_id']);
        }

        $companies = $companies->latest()->paginate(5);
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {
        return view('companies.create', compact('company'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required|min:3',
            'description' => 'required|min:3',
            'loan_class' => 'required|min:3',
            'loan_group' => 'numeric|required|min:3',
            'logo.*' => 'mimes:jpeg,jpg,png|max:10000' // max 10000kb
        ]);
        $id = request('id');
        $file = $request->file('logo');
        //dd($file);
        if (!empty($id) && is_numeric($id)) {
            $company = Company::find($id);
            $company->status = request('status');
            $company->updated_by = auth()->id();
        } else {
            $company = new Company;
            $company->status = 1;
            $company->created_by = auth()->id();
        }
        
        $company->company_name  = request('company_name');
        $company->description   = request('description');
        $company->loan_group    = request('loan_group');
        $company->loan_class    = request('loan_class');


        if ($company->save()) {
            if (!empty($file)) {
                $file_arr = upload_file($file, '/documents/company_logo/', '/documents/company_logo/thumb/', 'image');
                foreach ($file_arr as $key => $value) {
                    $doc_id = Document::where(array('company_id' => $id, 'category' => 'company_logo'))->value('id');
                    if (!empty($doc_id)) {
                        $file_data = Document::find($doc_id);
                        $db_file_name= $file_data->document_name;
                        
                        $s3_filename = public_path().'/documents/company_logo/'.$db_file_name;
                        \File::delete($s3_filename);
                        \File::delete(public_path().'/documents/company_logo/thumb/'.$db_file_name);
                    } else {
                        $file_data = new Document;
                    }
                    
                    $file_data->document_name       = $value['generated_file_name'];
                    $file_data->doc_org_name        = $value['original_file_name'];
                    $file_data->company_id          = $company->id;
                    $file_data->extension           = $value['extension'];
                    $file_data->category            = 'company_logo';
                    $file_data->uploaded_by         = auth()->id();
                    $file_data->status              = 1;
                    if (!$file_data->save()) {
                        session()->flash('message_err', 'File could not be uploaded!');
                        return redirect()->back();
                    }
                }
            }
            if (!empty($id) && is_numeric($id)) {
                session()->flash('message', 'Company is updated successfully!');
            } else {
                session()->flash('message', 'Company is created successfully!');
            }
        } else {
            session()->flash('message_err', 'Some error occurred, Please try later!');
        }
        
        return redirect(route('companies.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //dd($company);
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'company_name'  => 'required|min:3',
            'description'   => 'required|min:3',
            'status'        => 'required',
        ]);
        $company->company_name = request('company_name');
        $company->description = request('description');
        $company->status = request('status');
        $company->updated_by = auth()->id();
        $company->save();

        return redirect(route('companies.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->status = 2;
        $company->save();
        session()->flash('message', 'Company deleted successfully!!');
        return redirect(route('companies.index'));
    }
}
