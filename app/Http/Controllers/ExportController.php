<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use App\Assignment;
use App\Assignment_Client;
use App\Company;
use App\User;
use Excel;
use App\Exports\DataExport;


class ExportController extends Controller {

public function index(){
$emp = Assignment::paginate(5);
return view('export_assignment_list', ['emp' => $emp]);
}
public function export_to_caprock() {
    $type = 'xlsx';
    $employees = Assignment::all();
    //echo '<pre>';print_r($employees);exit;

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'assignment_type');
    $sheet->setCellValue('B1', 'repo_type');
    $sheet->setCellValue('C1', 'make');
    $sheet->setCellValue('D1', 'model');
    $sheet->setCellValue('E1', 'year');
    $sheet->setCellValue('F1', 'vin');
    $sheet->setCellValue('G1', 'license_plate');
    $sheet->setCellValue('H1', 'vehicle_address');
    $sheet->setCellValue('I1', 'vehicle_apt_no');
    $sheet->setCellValue('J1', 'vehicle_city');
    $sheet->setCellValue('K1', 'vehicle_state');
    $sheet->setCellValue('L1', 'vehicle_zip');
    $sheet->setCellValue('M1', 'legal_sale_date');
    $sheet->setCellValue('N1', 'keys');
    $sheet->setCellValue('O1', 'client_name');
    $sheet->setCellValue('P1', 'client_account_no');
    $sheet->setCellValue('Q1', 'contract_date');
    $sheet->setCellValue('R1', 'original_loan_amount');
    $sheet->setCellValue('S1', 'first_name');
    $sheet->setCellValue('T1', 'last_name');
    $sheet->setCellValue('U1', 'ssn');
    $sheet->setCellValue('V1', 'customer_address');
    $sheet->setCellValue('W1', 'customer_apt_no');
    $sheet->setCellValue('X1', 'customer_city');
    $sheet->setCellValue('Y1', 'customer_state');
    $sheet->setCellValue('Z1', 'customer_zip');
    $sheet->setCellValue('AA1', 'cust_poe_company_name');
    $sheet->setCellValue('AB1', 'cust_poe_company_address');
    $sheet->setCellValue('AC1', 'cust_poe_suite_no');
    $sheet->setCellValue('AD1', 'cust_poe_company_city');
    $sheet->setCellValue('AE1', 'cust_poe_company_state');
    $sheet->setCellValue('AF1', 'cust_poe_company_zip');
    $sheet->setCellValue('AG1', 'assignment_notes');
    $sheet->setCellValue('AH1', 'vehicle_notes_type');
    $sheet->setCellValue('AI1', 'vehicle_notes');
    $sheet->setCellValue('AJ1', 'date_initial_assignment_received');
    $sheet->setCellValue('AK1', 'status');
    $sheet->setCellValue('AL1', 'created_by');
    $sheet->setCellValue('AM1', 'created_at');
        $sheet->setCellValue('AN1', 'Client_Queue_#');
    $rows = 2;
    foreach($employees as $empDetails){
        $sheet->setCellValue('A' . $rows, $empDetails['assignment_type']);
        $sheet->setCellValue('B' . $rows, $empDetails['repo_type']);
        $sheet->setCellValue('C' . $rows, $empDetails['make']);
        $sheet->setCellValue('D' . $rows, $empDetails['model']);
        $sheet->setCellValue('E' . $rows, $empDetails['year']);
        $sheet->setCellValue('F' . $rows, $empDetails['vin']);
        $sheet->setCellValue('G' . $rows, $empDetails['license_plate']);
        $sheet->setCellValue('H' . $rows, $empDetails['vehicle_address']);
        $sheet->setCellValue('I' . $rows, $empDetails['vehicle_apt_no']);
        $sheet->setCellValue('J' . $rows, $empDetails['vehicle_city']);
        $sheet->setCellValue('K' . $rows, $empDetails['vehicle_state']);
        $sheet->setCellValue('L' . $rows, $empDetails['vehicle_zip']);
        $sheet->setCellValue('M' . $rows, $empDetails['legal_sale_date']);
        $sheet->setCellValue('N' . $rows, $empDetails['keys']);
        $sheet->setCellValue('O' . $rows, $empDetails['client_name']);
        $sheet->setCellValue('P' . $rows, $empDetails['client_account_no']);
        $sheet->setCellValue('Q' . $rows, $empDetails['contract_date']);
        $sheet->setCellValue('R' . $rows, $empDetails['original_loan_amount']);
        $sheet->setCellValue('S' . $rows, $empDetails['first_name']);
        $sheet->setCellValue('T' . $rows, $empDetails['last_name']);
        $sheet->setCellValue('U' . $rows, $empDetails['ssn']);
        $sheet->setCellValue('V' . $rows, $empDetails['customer_address']);
        $sheet->setCellValue('W' . $rows, $empDetails['customer_apt_no']);
        $sheet->setCellValue('X' . $rows, $empDetails['customer_city']);
        $sheet->setCellValue('Y' . $rows, $empDetails['customer_state']);
        $sheet->setCellValue('Z' . $rows, $empDetails['customer_zip']); 
 
        $sheet->setCellValue('AA' . $rows, $empDetails['cust_poe_company_name']);
        $sheet->setCellValue('AB' . $rows, $empDetails['cust_poe_company_address']);
        $sheet->setCellValue('AC' . $rows, $empDetails['cust_poe_suite_no']);
        $sheet->setCellValue('AD' . $rows, $empDetails['cust_poe_company_city']);
        $sheet->setCellValue('AE' . $rows, $empDetails['cust_poe_company_state']);
        $sheet->setCellValue('AF' . $rows, $empDetails['cust_poe_company_zip']);
        $sheet->setCellValue('AG' . $rows, $empDetails['assignment_notes']);
        $sheet->setCellValue('AH' . $rows, $empDetails['vehicle_notes_type']);
        $sheet->setCellValue('AI' . $rows, $empDetails['vehicle_notes']);
        $sheet->setCellValue('AJ' . $rows, $empDetails['date_initial_assignment_received']);
        $sheet->setCellValue('AK' . $rows, $empDetails['status']);
        $sheet->setCellValue('AL' . $rows, $empDetails['created_by']);
        $sheet->setCellValue('AM' . $rows, $empDetails['created_at']);
        $sheet->setCellValue('AN' . $rows, $empDetails['client_queue_no']);
        $rows++;
    }

    $fileName = "assigment_".date('m-d-Y').".".$type;
    if($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
    } else if($type == 'xls') {
        $writer = new Xls($spreadsheet);
    }

    $writer->save("../cron_jobs_docs/App to Caprock Assignments/".$fileName);
    header("Content-Type: application/vnd.ms-excel");//exit;
    //return redirect(url('/')."/export-to-caprock/".$fileName);
}


public function assignment_export(){ 
        //echo '<pre>';print_r($_POST);exit;
        $status             = request('status');
        $session_data       = get_session_data();## Helper function fetching all session data 
        $date_from          = strtotime(request()->post('date_from'));
        $date_to            = strtotime(request()->post('date_to'));
        $company_name       = '';
        $company_name       = get_company_name();
 
        $Assignment_Client  = Assignment_Client::where(array('status'=>0, 'client_name'=>$company_name)); 
        if (!empty($date_from)) {
            $Assignment_Client = $Assignment_Client->whereRaw("(DATE(recovery_date) >= '{$date_from}')");
        }

        if (!empty($date_to)) {
            $Assignment_Client = $Assignment_Client->whereRaw("(DATE(recovery_date) <= '{$date_to}')");
        }

/*
        $current_role_id = get_role_id();
        if ($current_role_id>1) {
            $companies = $companies->where('created_by', $session_data['user_id']);
        }*/

        $Assignment_Client = $Assignment_Client->latest()->paginate(20);
        return view('assignment_export.index', compact('Assignment_Client'));
    }

    public function export_xls() { 
    $type = 'xlsx';
    $status              = request('status');
    $session_data        = get_session_data();## Helper function fetching all session data 
    $date_from           = strtotime(request()->post('date_from'));
    $date_to             = strtotime(request()->post('date_to'));
    $company_name        = '';
    $company_name        = get_company_name(); 

    $Assignment_Client   = Assignment_Client::where(array('status'=>0, 'client_name'=>$company_name)); 
    if (!empty($date_from)) {
        $Assignment_Client = $Assignment_Client->whereRaw("(DATE(recovery_date) >= '{$date_from}')");
    }

    if (!empty($date_to)) {
        $Assignment_Client = $Assignment_Client->whereRaw("(DATE(recovery_date) <= '{$date_to}')");
    }
    $Assignment_Client = $Assignment_Client->latest()->get();
    if($Assignment_Client){
      $Assignment_Client = $Assignment_Client->toArray();  
    } 
      // file name for download
      $filename  = "assignment_" . date('Ymd') . ".xls"; 
      header("Content-Disposition: attachment; filename=\"$filename\"");
      header("Content-Type: application/vnd.ms-excel");
       
      $flag = false;
      foreach($Assignment_Client as $row) {
        if(!$flag) {
          // display field/column names as first row
          echo implode("\t", array_keys($row)) . "\n";
          $flag = true;
        }
       // array_walk($row, __NAMESPACE__ . '\cleanData');
        echo implode("\t", array_values($row)) . "\n";
      } 
        
       /* $fileName = "assigment_".date('m-d-Y').".".$type;
        if($type == 'xlsx') {
            $writer = new Xlsx($spreadsheet);
        } else if($type == 'xls') {
            $writer = new Xls($spreadsheet);
        }

        $writer->save("../cron_jobs_docs/App to Caprock Assignments/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        $writer->download("../cron_jobs_docs/App to Caprock Assignments/".$fileName);
        return redirect(url('/')."/export-to-caprock/".$fileName);*/ 

    }
}