<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Assignment;
use App\Assignment_Client;
use Carbon\Carbon;
use Mail;
use Auth;
use Storage;
use Response;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use League\Flysystem\Adapter\Ftp;
use App\Jobs\ScheduleCronJob;
use App\Jobs\SendEmail;

//use ZipArchive;

class CronJobsController extends Controller
{
    function fetchFile(Request $request)
    {
        $history = DB::table('import_history_log')->pluck('time', 'name')->toArray();
        $destination_folder     = 'cron_jobs_docs/';
        $executed_folder        = array('Caprock to App Assignments', 'Title Status', 'Average Vehicle Grade', 'Average Sales Price', 'Average Days to Sell', 'Average Days to Reposses', 'AssignmentsByState', 'Last Two Days Auction Report', 'Last Two Days Agent Report', 'Title Not Received Report');
        foreach ($executed_folder as $folder) {
            $comlete_folder         =  base_path($destination_folder . $folder);
            $scanned_directory      = array_diff(scandir($comlete_folder), array('.', '..', 'thumb'));
            $scanned_directoryVal   = array_values($scanned_directory);

            // print_r($scanned_directoryVal);exit;
            //$scanned_directory  =  scandir($comlete_folder,1); 
            if (!empty($scanned_directoryVal[0])) {
                $path_with_csv      = $comlete_folder . '/' . $scanned_directoryVal[0];

                if (file_exists($path_with_csv)) {
                    if (ScheduleCronJob::dispatch($path_with_csv)) {
                        echo '<br>==>>' . $path_with_csv . ' Job created';
                    } else {
                        echo '<br>==>>' . $path_with_csv . ' Error Job not created';
                    }
                }
            } else {
                if (empty($history[$folder]) || (!empty($history[$folder])) && (round((strtotime(date('Y-m-d H:i:s')) - strtotime($history[$folder])) / 3600, 1) > 24)) {
                    $error = 'No file found to import in ' . $folder . ' folder';
                    SendEmail::dispatch($error);
                }
            }
        }
    }
}
