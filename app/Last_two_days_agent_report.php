<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_two_days_agent_report extends Model
{
    protected $table = 'last_two_days_agent_report';
    public $timestamps = false;
}
