<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secured_last_day extends Model
{
    protected $table = 'secured_last_day';
    public $timestamps = false;
}
