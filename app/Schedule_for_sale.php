<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule_for_sale extends Model
{
    protected $table = 'schedule_for_sale';
    public $timestamps = false;
}
