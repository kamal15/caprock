<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class VehicleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text' )
            ->add('lyrics', 'textarea'  )
            ->add('publish', 'checkbox'  )
            ->add('submit', 'submit', ['label' => 'Save form']);
    }
}
