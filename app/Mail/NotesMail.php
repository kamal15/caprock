<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotesMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
       // dd($this->data);exit;
        $type = $this->data['type'];
        $loan_no = $this->data['loan_no'];
        $vin = $this->data['vin'];
        $fromEMail = 'no_reply@innovateauto.com';  //no_reply@innovateauto.com;
        return $this->view('emails.notes')
        ->from($fromEMail, 'no_reply')
        ->subject('Caprem:Type ('.$type.')/Loan Number ('.$loan_no .')/VIN ('. $vin .')!')
        ->replyTo('join2kamal@gmail.com', 'kamal Intellisoft');
        //return $this->view('view.name');
    }
}
