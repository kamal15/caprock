<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome')
        ->from(config('constants.REPO_EMAIL'), config('constants.SITE_NAME'))
        ->subject('Csv Import Error Notification!')
        ->replyTo(config('constants.REPO_EMAIL'), config('constants.SITE_NAME'));
        //return $this->view('view.name');
    }
}
