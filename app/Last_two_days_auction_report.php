<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_two_days_auction_report extends Model
{
    protected $table = 'last_two_days_auction_report';
    public $timestamps = false;
}
