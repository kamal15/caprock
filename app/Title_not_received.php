<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title_not_received extends Model
{
    protected $table = 'title_not_received';
    public $timestamps = false;
}
