<?php 
namespace App\Exports;
use App\Assignment_Client;


use Maatwebsite\Excel\Concerns\FromCollection;

class DataExport implements FromCollection
{
    public function collection()
    {
        return Assignment_client::latest()->paginate(20);
    }
}