<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblCustomerSpace extends Model
{
    protected $table = 'tbl_customer_spaces';
    public $timestamps = false;
}
