<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Scheduled_cron_jobs_error_log extends Model{
    protected $table = 'scheduled_cron_jobs_error_log';
    public $timestamps = false;
}
