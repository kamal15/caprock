<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Average_days_to_sell extends Model
{
    protected $table = 'average_days_to_sell';
    public $timestamps = false;
}
