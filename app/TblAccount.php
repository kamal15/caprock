<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

//use OwenIt\Auditing\Contracts\Auditable;
class TblAccount extends Model
{
    //protected $table = 'tbl_accounts';
    //use \OwenIt\Auditing\Auditable;
    public $timestamps = false;
    protected $fillable = ['*'];
    protected $primaryKey  = 'AcctID';
}
