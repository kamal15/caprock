<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title_Status extends Model
{
    protected $table = 'title_status'; 
    public $timestamps = false;
}
